package com.tracking.constantes;

public class ConstantesDAO {
	public static final String SCHEMA_NAME = "sch_trackingbox";
	public static final String SP_PEDIDO_CONSULTAR = "sp_pedido_consultar";
	public static final String SP_PEDIDO_REGISTRAR= "sp_pedido_registrar";
	public static final String SP_ADMIN_OBTENER_USUARIO= "sp_admin_obtener_usuario";
	public static final String SP_UBIGEO_LISTAR = "sp_ubigeo_listar";
	public static final String SP_SEGMENTO_LISTAR = "sp_segmento_listar";
	public static final String SP_TIPOPAGO_LISTAR = "sp_tipopago_listar";
	public static final String SP_PEDIDO_PREPARAR = "sp_pedido_preparar";
}
