package com.tracking.dao;

import java.util.List;

import com.tracking.bean.Filtro;
import com.tracking.bean.Segmento;
import com.tracking.bean.TipoPago;
import com.tracking.bean.Ubigeo;

public interface CommonDao {

	List<Ubigeo> listarUbigeos(Filtro filtro);

	List<TipoPago> listarTipoPago();

	List<Segmento> listarSegmento();
}