package com.tracking.dao;

import com.tracking.bean.Usuario;

public interface UsuarioDao {
	
	Usuario obtenerUsuario (Usuario usuario);
//	Integer actualizarClaveUsuario(Usuario usuario);
}