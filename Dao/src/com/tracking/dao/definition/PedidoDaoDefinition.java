package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.Pedido;
import com.tracking.dao.util.DaoDefinition;

@Repository("PedidoDaoDefinition")
public class PedidoDaoDefinition extends DaoDefinition<Pedido> {
	
	private AuditoriaDaoDefinition auditoriaDaoDefinition;
	private EstadoDaoDefinition estadoDaoDefinition;
	private SubEstadoDaoDefinition subEstadoDaoDefinition;
	private TipoDocPagoDaoDefinition tipoDocPagoDaoDefinition;
	private ModalidadDaoDefinition modalidadDaoDefinition;
	private TipoPagoDaoDefinition tipoPagoDaoDefinition;
	private SegmentoDaoDefinition segmentoDaoDefinition;
	private TipoDocDaoDefinition tipoDocDaoDefinition;
	
	public PedidoDaoDefinition() {
		super(Pedido.class);
		auditoriaDaoDefinition = new AuditoriaDaoDefinition();
		estadoDaoDefinition = new EstadoDaoDefinition();
		subEstadoDaoDefinition = new SubEstadoDaoDefinition();
		tipoDocPagoDaoDefinition = new TipoDocPagoDaoDefinition();
		modalidadDaoDefinition = new ModalidadDaoDefinition();
		tipoPagoDaoDefinition = new TipoPagoDaoDefinition();
		segmentoDaoDefinition = new SegmentoDaoDefinition();
		tipoDocDaoDefinition = new TipoDocDaoDefinition();
	}
	
	@Override
	public Pedido mapRow(ResultSet rs, int rowNumber) throws SQLException {
		
		Pedido pedido = super.mapRow(rs, rowNumber);
		pedido.setAuditoria(auditoriaDaoDefinition.mapRow(rs, rowNumber));

		pedido.setEstado(estadoDaoDefinition.mapRow(rs, rowNumber));
		pedido.setSubEstadoBean(subEstadoDaoDefinition.mapRow(rs, rowNumber));
		pedido.setTipoDocPagoBean(tipoDocPagoDaoDefinition.mapRow(rs, rowNumber));
		pedido.setModalidadBean(modalidadDaoDefinition.mapRow(rs, rowNumber));
		pedido.setTipoPagoBean(tipoPagoDaoDefinition.mapRow(rs, rowNumber));
		pedido.setSegmentoBean(segmentoDaoDefinition.mapRow(rs, rowNumber));
		pedido.setTipoDocBean(tipoDocDaoDefinition.mapRow(rs, rowNumber));
		return pedido;
	}

}
