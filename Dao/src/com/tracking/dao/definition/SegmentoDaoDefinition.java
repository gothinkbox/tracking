package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.Segmento;
import com.tracking.dao.util.DaoDefinition;

@Repository("SegmentoDaoDefinition")
public class SegmentoDaoDefinition extends DaoDefinition<Segmento> {
	
	
	public SegmentoDaoDefinition() {
		super(Segmento.class);
	}
	
	@Override
	public Segmento mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Segmento segmento = super.mapRow(rs, rowNumber);
		return segmento;
	}

}
