package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.Estado;
import com.tracking.dao.util.DaoDefinition;

@Repository("EstadoDaoDefinition")
public class EstadoDaoDefinition extends DaoDefinition<Estado> {
	
	
	public EstadoDaoDefinition() {
		super(Estado.class);
	}
	
	@Override
	public Estado mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Estado obj = super.mapRow(rs, rowNumber);
		return obj;
	}

}
