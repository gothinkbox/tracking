package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.TipoPago;
import com.tracking.dao.util.DaoDefinition;

@Repository("TipoPagoDaoDefinition")
public class TipoPagoDaoDefinition extends DaoDefinition<TipoPago> {
	
	
	public TipoPagoDaoDefinition() {
		super(TipoPago.class);
	}
	
	@Override
	public TipoPago mapRow(ResultSet rs, int rowNumber) throws SQLException {
		TipoPago tipoPago = super.mapRow(rs, rowNumber);
		return tipoPago;
	}

}
