package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.TipoDocPago;
import com.tracking.dao.util.DaoDefinition;

@Repository("TipoDocPagoDaoDefinition")
public class TipoDocPagoDaoDefinition extends DaoDefinition<TipoDocPago> {
	
	
	public TipoDocPagoDaoDefinition() {
		super(TipoDocPago.class);
	}
	
	@Override
	public TipoDocPago mapRow(ResultSet rs, int rowNumber) throws SQLException {
		TipoDocPago obj = super.mapRow(rs, rowNumber);
		return obj;
	}

}
