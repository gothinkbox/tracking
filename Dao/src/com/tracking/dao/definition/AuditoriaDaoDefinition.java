package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.Auditoria;
import com.tracking.dao.util.DaoDefinition;


@Repository("AuditoriaDaoDefinition")
public class AuditoriaDaoDefinition extends DaoDefinition<Auditoria> { 
	
	public AuditoriaDaoDefinition() {
		super(Auditoria.class);
	}

	@Override
	public Auditoria mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Auditoria auditoria = super.mapRow(rs, rowNumber);
		return auditoria;
	}

}
