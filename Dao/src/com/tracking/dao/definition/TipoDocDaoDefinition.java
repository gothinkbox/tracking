package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.TipoDoc;
import com.tracking.dao.util.DaoDefinition;

@Repository("TipoDocDaoDefinition")
public class TipoDocDaoDefinition extends DaoDefinition<TipoDoc> {
	
	
	public TipoDocDaoDefinition() {
		super(TipoDoc.class);
	}
	
	@Override
	public TipoDoc mapRow(ResultSet rs, int rowNumber) throws SQLException {
		TipoDoc obj = super.mapRow(rs, rowNumber);
		return obj;
	}

}
