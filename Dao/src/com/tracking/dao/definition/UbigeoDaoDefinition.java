package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.Ubigeo;
import com.tracking.dao.util.DaoDefinition;

@Repository("UbigeoDaoDefinition")
public class UbigeoDaoDefinition extends DaoDefinition<Ubigeo> {
	
	
	public UbigeoDaoDefinition() {
		super(Ubigeo.class);
	}
	
	@Override
	public Ubigeo mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Ubigeo ubigeo = super.mapRow(rs, rowNumber);
		return ubigeo;
	}

}
