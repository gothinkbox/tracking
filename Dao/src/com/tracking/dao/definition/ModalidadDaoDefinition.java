package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.Modalidad;
import com.tracking.dao.util.DaoDefinition;

@Repository("ModalidadDaoDefinition")
public class ModalidadDaoDefinition extends DaoDefinition<Modalidad> {
	
	
	public ModalidadDaoDefinition() {
		super(Modalidad.class);
	}
	
	@Override
	public Modalidad mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Modalidad obj = super.mapRow(rs, rowNumber);
		return obj;
	}

}
