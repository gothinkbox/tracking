package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.SubEstado;
import com.tracking.dao.util.DaoDefinition;

@Repository("SubEstadoDaoDefinition")
public class SubEstadoDaoDefinition extends DaoDefinition<SubEstado> {
	
	
	public SubEstadoDaoDefinition() {
		super(SubEstado.class);
	}
	
	@Override
	public SubEstado mapRow(ResultSet rs, int rowNumber) throws SQLException {
		SubEstado obj = super.mapRow(rs, rowNumber);
		return obj;
	}

}
