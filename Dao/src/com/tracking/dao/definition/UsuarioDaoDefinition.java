package com.tracking.dao.definition;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.tracking.bean.Usuario;
import com.tracking.dao.util.DaoDefinition;

@Repository("UsuarioDaoDefinition")
public class UsuarioDaoDefinition extends DaoDefinition<Usuario> {
	
	private RolDaoDefinition rolDaoDefinition;
	
	public UsuarioDaoDefinition() {
		super(Usuario.class);
		rolDaoDefinition = new RolDaoDefinition();
	}
	
	@Override
	public Usuario mapRow(ResultSet rs, int rowNumber) throws SQLException {
		
		Usuario usuario = super.mapRow(rs, rowNumber);
		usuario.setRol(rolDaoDefinition.mapRow(rs, rowNumber));
		return usuario;
	}

}
