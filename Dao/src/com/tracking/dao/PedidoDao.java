package com.tracking.dao;

import java.util.List;

import com.tracking.bean.Filtro;
import com.tracking.bean.Pedido;
import com.tracking.bean.Pedido2;

public interface PedidoDao {

	Integer registrar(Pedido2 pedido);
	Pedido consultar(Integer seqPedido);
	List<Pedido> listarPedido(Filtro filtro);
	Integer preparar(Pedido pedido);
}