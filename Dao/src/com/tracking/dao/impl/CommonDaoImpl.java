package com.tracking.dao.impl;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.tracking.bean.Filtro;
import com.tracking.bean.Segmento;
import com.tracking.bean.TipoPago;
import com.tracking.bean.Ubigeo;
import com.tracking.constantes.ConstantesDAO;
import com.tracking.dao.CommonDao;
import com.tracking.dao.definition.SegmentoDaoDefinition;
import com.tracking.dao.definition.TipoPagoDaoDefinition;
import com.tracking.dao.definition.UbigeoDaoDefinition;

@Repository(value="UbigeoDao")
public class CommonDaoImpl implements CommonDao {
	
	Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplateTest;
	
	private SimpleJdbcCall jdbcCall;
	
	@Autowired
	private UbigeoDaoDefinition ubigeoDaoDefinition;

	@Autowired
	private TipoPagoDaoDefinition tipoPagoDaoDefinition;
	
	@Autowired
	private SegmentoDaoDefinition segmentoDaoDefinition;

	@SuppressWarnings("unchecked")
	@Override
	public List<Ubigeo> listarUbigeos(Filtro filtro) {
		LOGGER.info("listarUbigeos");
		jdbcCall = new SimpleJdbcCall(jdbcTemplateTest);
		jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
		jdbcCall.withProcedureName(ConstantesDAO.SP_UBIGEO_LISTAR);
		jdbcCall.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				new SqlParameter("var_tipobusqueda", Types.INTEGER),
				new SqlParameter("var_coddep", Types.VARCHAR),
				new SqlParameter("var_codprov", Types.VARCHAR)
				);
		Map<String, Object> inParam = new HashMap<String, Object>();
		inParam.put("var_tipobusqueda", filtro.getTipoBusqueda());
		inParam.put("var_coddep", filtro.getUbigeo().getCodDep());
		inParam.put("var_codprov", filtro.getUbigeo().getCodProv());
		SqlParameterSource in = new MapSqlParameterSource(inParam);
		jdbcCall.returningResultSet("resultado", ubigeoDaoDefinition);
		List<Ubigeo> result = jdbcCall.executeObject(List.class, in);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TipoPago> listarTipoPago() {
		LOGGER.info("listarUbigeos");
		jdbcCall = new SimpleJdbcCall(jdbcTemplateTest);
		jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
		jdbcCall.withProcedureName(ConstantesDAO.SP_TIPOPAGO_LISTAR);
		jdbcCall.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				);
		Map<String, Object> inParam = new HashMap<String, Object>();
		SqlParameterSource in = new MapSqlParameterSource(inParam);
		jdbcCall.returningResultSet("resultado", tipoPagoDaoDefinition);
		List<TipoPago> result = jdbcCall.executeObject(List.class, in);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Segmento> listarSegmento() {
		LOGGER.info("listarSegmento");
		jdbcCall = new SimpleJdbcCall(jdbcTemplateTest);
		jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
		jdbcCall.withProcedureName(ConstantesDAO.SP_SEGMENTO_LISTAR);
		jdbcCall.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters();
		Map<String, Object> inParam = new HashMap<String, Object>();
		SqlParameterSource in = new MapSqlParameterSource(inParam);
		jdbcCall.returningResultSet("resultado", segmentoDaoDefinition);
		List<Segmento> result = jdbcCall.executeObject(List.class, in);
		return result;
	}

}
