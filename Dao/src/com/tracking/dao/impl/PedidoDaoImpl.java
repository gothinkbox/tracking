package com.tracking.dao.impl;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.tracking.bean.Filtro;
import com.tracking.bean.Pedido;
import com.tracking.bean.Pedido2;
import com.tracking.constantes.ConstantesDAO;
import com.tracking.dao.PedidoDao;
import com.tracking.dao.definition.PedidoDaoDefinition;

@Repository(value="PedidoDao")
public class PedidoDaoImpl implements PedidoDao {
	
	Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplateTest;
	
	private SimpleJdbcCall jdbcCall;
	
	@Autowired
	private PedidoDaoDefinition pedidoDaoDefinition;
	
	@Override
	public Integer registrar(Pedido2 pedido) {
		LOGGER.info("registrarPedido");
		jdbcCall = new SimpleJdbcCall(jdbcTemplateTest);
		jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
		jdbcCall.withProcedureName(ConstantesDAO.SP_PEDIDO_REGISTRAR);
		jdbcCall.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
					new SqlParameter("var_departamento",Types.VARCHAR),
					new SqlParameter("var_provincia",Types.VARCHAR),
					new SqlParameter("var_distrito",Types.VARCHAR),
					new SqlParameter("var_tipovia",Types.VARCHAR),
					new SqlParameter("var_nombrevia",Types.VARCHAR),
					new SqlParameter("var_numerovia",Types.VARCHAR),
					new SqlParameter("var_referencia",Types.VARCHAR),
					new SqlParameter("var_pisodepartamento",Types.VARCHAR),
					new SqlParameter("var_rangohorario",Types.VARCHAR),
					new SqlParameter("var_nombrecontacto",Types.VARCHAR),
					new SqlParameter("var_telefonocontacto",Types.VARCHAR),
					new SqlParameter("var_tipopago",Types.VARCHAR),
					new SqlParameter("var_fecha",Types.VARCHAR),
					new SqlParameter("var_hora",Types.VARCHAR),
					new SqlParameter("var_nombreusuario",Types.VARCHAR),
					new SqlParameter("var_telefonousuario",Types.VARCHAR),
					new SqlParameter("var_codigocomercio",Types.VARCHAR),
					new SqlParameter("var_nombrecomercio",Types.VARCHAR),
					new SqlParameter("var_ruccomercio",Types.VARCHAR),
					new SqlParameter("var_idpedido",Types.VARCHAR),
					new SqlParameter("var_monto",Types.VARCHAR),
					new SqlParameter("var_cantidaditems",Types.VARCHAR),
					//updated 10-04-2018
					new SqlParameter("var_idvisanet",Types.VARCHAR),
					new SqlParameter("var_tipocontraentrega",Types.VARCHAR),
					new SqlParameter("var_montoefectivo",Types.VARCHAR),
					new SqlParameter("var_dni",Types.VARCHAR),
					new SqlParameter("var_tipodocumentopago",Types.VARCHAR),
					new SqlParameter("var_emailrepresentante",Types.VARCHAR),
					new SqlOutParameter("resultado", Types.INTEGER));
		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("var_departamento", pedido.getDepartamento());					
		inParamMap.put("var_provincia", pedido.getProvincia());					
		inParamMap.put("var_distrito", pedido.getDistrito());
		inParamMap.put("var_tipovia", pedido.getTipoVia());
		inParamMap.put("var_nombrevia", pedido.getNombreVia());
		inParamMap.put("var_numerovia", pedido.getNumeroVia());
		inParamMap.put("var_referencia", pedido.getReferencia());
		inParamMap.put("var_pisodepartamento", pedido.getPisoDepartamento());
		inParamMap.put("var_rangohorario", pedido.getRangoHorario());
		inParamMap.put("var_nombrecontacto", pedido.getNombreContacto());
		inParamMap.put("var_telefonocontacto", pedido.getTelefonoContacto());
		inParamMap.put("var_tipopago", pedido.getTipoPago());
		inParamMap.put("var_fecha", pedido.getFecha());
		inParamMap.put("var_hora", pedido.getHora());
		inParamMap.put("var_nombreusuario", pedido.getNombreUsuario());
		inParamMap.put("var_telefonousuario", pedido.getTelefonoUsuario());
		inParamMap.put("var_codigocomercio", pedido.getCodigoComercio());
		inParamMap.put("var_nombrecomercio", pedido.getNombreComercio());
		inParamMap.put("var_ruccomercio", pedido.getRucComercio());
		inParamMap.put("var_idpedido", pedido.getIdPedido());
		inParamMap.put("var_monto", pedido.getMonto());
		inParamMap.put("var_cantidaditems", pedido.getCantidadItems());
		//updated 10-04-2018
		inParamMap.put("var_idvisanet", pedido.getIdVisanet());
		inParamMap.put("var_tipocontraentrega", pedido.getTipocontraentrega());
		inParamMap.put("var_montoefectivo", pedido.getMontoEfectivo());
		inParamMap.put("var_dni", pedido.getDni());
		inParamMap.put("var_tipodocumentopago", pedido.getTipoDocumentoPago());
		inParamMap.put("var_emailrepresentante", pedido.getEmailRepresentante());

		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		Integer result = jdbcCall.executeFunction(Integer.class, in);
		LOGGER.info("RESULTADO REGISTRO: "+result.toString());
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Pedido consultar(Integer seqpedido) {
		LOGGER.info("listarPedidoById");
		Pedido lista = new Pedido();
		jdbcCall = new SimpleJdbcCall(jdbcTemplateTest);
		jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
		jdbcCall.withProcedureName(ConstantesDAO.SP_PEDIDO_CONSULTAR);
		jdbcCall.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				new SqlParameter("var_seqpedido", Types.INTEGER)
				);
		Map<String, Object> inParam = new HashMap<String, Object>();
		inParam.put("var_seqpedido", seqpedido);
		SqlParameterSource in = new MapSqlParameterSource(inParam);
		jdbcCall.returningResultSet("resultado", pedidoDaoDefinition);
		List<Pedido> result = jdbcCall.executeObject(List.class, in);
		lista =  result!=null?result.get(0):lista;
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pedido> listarPedido(Filtro filtro) {
		LOGGER.info("listarPedido");
		List<Pedido> result =null;
		try {
			jdbcCall = new SimpleJdbcCall(jdbcTemplateTest);
			jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
			jdbcCall.withProcedureName(ConstantesDAO.SP_PEDIDO_CONSULTAR);
			jdbcCall.withoutProcedureColumnMetaDataAccess();
			//TODO
			jdbcCall.declareParameters(
					new SqlParameter("var_estado", Types.INTEGER),
					new SqlParameter("var_fecdesde", Types.VARCHAR),
					new SqlParameter("var_fechasta", Types.VARCHAR),
					new SqlParameter("var_departamento", Types.VARCHAR),
					new SqlParameter("var_provincia", Types.VARCHAR),
					new SqlParameter("var_tipopago", Types.INTEGER),
					new SqlParameter("var_rubro", Types.INTEGER),
					new SqlParameter("var_tipobusqueda", Types.INTEGER),
					new SqlParameter("var_valor", Types.VARCHAR),
					new SqlParameter("var_id", Types.INTEGER)
					);
			Map<String, Object> inParam = new HashMap<String, Object>();
			inParam.put("var_estado", filtro.getEstado());
			inParam.put("var_fecdesde", filtro.getFecini());
			inParam.put("var_fechasta", filtro.getFecfin());
			inParam.put("var_departamento", filtro.getDepartamento());
			inParam.put("var_provincia", filtro.getProvincia());
			inParam.put("var_tipopago", filtro.getTipoPago());
			inParam.put("var_rubro", filtro.getRubro());
			inParam.put("var_tipobusqueda", filtro.getTipoBusqueda());
			inParam.put("var_valor", filtro.getValor());
			inParam.put("var_id", filtro.getIdPedido());
			SqlParameterSource in = new MapSqlParameterSource(inParam);
			jdbcCall.returningResultSet("resultado", pedidoDaoDefinition);
			result = jdbcCall.executeObject(List.class, in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Integer preparar(Pedido pedido) {
		Integer result = 0;
		jdbcCall = new SimpleJdbcCall(jdbcTemplateTest);
		jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
		jdbcCall.withProcedureName(ConstantesDAO.SP_PEDIDO_PREPARAR);
		jdbcCall.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				new SqlParameter("var_id", Types.INTEGER),
				new SqlParameter("var_mpos", Types.VARCHAR),
				new SqlParameter("var_guia", Types.VARCHAR),
				new SqlParameter("var_factura", Types.VARCHAR),
				new SqlOutParameter("resultado", Types.INTEGER));
		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("var_id", pedido.getIdPedido());
		inParamMap.put("var_mpos", pedido.getSeriales());
		inParamMap.put("var_guia", pedido.getNumGuiaRemision());
		inParamMap.put("var_factura", pedido.getNumDocPago());
		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		result = jdbcCall.executeObject(Integer.class, in);
	    LOGGER.info("resultado: "+result.toString());
        return result;
	}

}
