package com.tracking.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.tracking.bean.Usuario;
import com.tracking.constantes.ConstantesDAO;
import com.tracking.dao.UsuarioDao;
import com.tracking.dao.definition.UsuarioDaoDefinition;

@Repository(value = "UsuarioDao")
public class UsuarioDaoImpl implements UsuarioDao{
	
	
	Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private SimpleJdbcCall jdbcCall;
	
	@Autowired
	UsuarioDaoDefinition usuarioDaoDefinition;
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Usuario obtenerUsuario(Usuario usuario) {
		List<Usuario> lista = new ArrayList<Usuario>();
		jdbcCall = new SimpleJdbcCall(jdbcTemplate);
		jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
		jdbcCall.withProcedureName(ConstantesDAO.SP_ADMIN_OBTENER_USUARIO);
		jdbcCall.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				new SqlParameter("var_cuenta",Types.VARCHAR));
		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("var_cuenta", usuario.getEmail());
		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		jdbcCall.returningResultSet("resultado", usuarioDaoDefinition);
		lista = jdbcCall.executeObject(List.class,in);
		if(!lista.isEmpty()){
			return lista.get(0);	
		}else{
			return new Usuario();
		}
	}

//	@Override
//	public Integer actualizarClaveUsuario(Usuario usuario) {
//		LOGGER.info("actualizarClaveParticipante");
//		Integer result = 0;
//		jdbcCall = new SimpleJdbcCall(jdbcTemplate);
//		jdbcCall.withSchemaName(ConstantesDAO.SCHEMA_NAME);
//		jdbcCall.withProcedureName(ConstantesDAO.SP_USUARIO_ACTUALIZAR_CLAVE);
//		jdbcCall.withoutProcedureColumnMetaDataAccess();
//		jdbcCall.declareParameters(
//				new SqlParameter("var_token", Types.VARCHAR),
//				new SqlParameter("var_clave", Types.VARCHAR),
//				new SqlParameter("var_usuario_actualizacion", Types.VARCHAR),
//				new SqlOutParameter("resultado", Types.INTEGER));
//		Map<String, Object> inParamMap = new HashMap<String, Object>();
//		inParamMap.put("var_token", usuario.getTransaccionToken().getToken());
//		inParamMap.put("var_clave", usuario.getClave());
//		inParamMap.put("var_usuario_actualizacion", "cambio de clave");
//		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
//		result = jdbcCall.executeObject(Integer.class, in);
//	    LOGGER.info("resultado: "+result.toString());
//        return result;
//	}
	
}