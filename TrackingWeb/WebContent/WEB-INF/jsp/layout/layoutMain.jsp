<%@ page contentType="text/html;charset=UTF-8" language="java"
	trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html class="fixed sidebar-left-collapsed">
<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Tracking Admin</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Tracking Admin - Responsive HTML5 Template">
		<meta name="author" content="thinkbox">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/animate/animate.css">

		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/owl.carousel/assets/owl.carousel.css" />
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/owl.carousel/assets/owl.theme.default.css" />
		
		
		<link rel="stylesheet" href="https://rawgit.com/wenzhixin/bootstrap-table/master/src/bootstrap-table.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}css/custom.css">

		<!-- Head Libs -->
		<script src="${prop['config.url.resources.base.web']}vendor/modernizr/modernizr.js"></script>
		<script src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
		<script src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>
		
		<!--  Validation -->
		<script src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
	</head>
	
	<body>
		<!--  SECTIONS -->
		<section class="body">
		
		
		<tiles:insertAttribute name="header" />
		
		<div class="inner-wrapper">
			
			<tiles:insertAttribute name="menu" />
			<tiles:insertAttribute name="content" />
		</div>
		
		</section>

		<!-- Vendor -->
		<script src="${prop['config.url.resources.base.web']}vendor/jquery/jquery.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/popper/umd/popper.min.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/bootstrap/js/bootstrap.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/common/common.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/nanoscroller/nanoscroller.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/jquery-placeholder/jquery-placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="${prop['config.url.resources.base.web']}vendor/jquery-appear/jquery-appear.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/owl.carousel/owl.carousel.js"></script>
		<script src="${prop['config.url.resources.base.web']}vendor/isotope/isotope.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="${prop['config.url.resources.base.web']}js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="${prop['config.url.resources.base.web']}js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="${prop['config.url.resources.base.web']}js/theme.init.js"></script>

		<!-- Examples -->
		<script src="${prop['config.url.resources.base.web']}js/examples/examples.landing.dashboard.js"></script>
		<script src="https://rawgit.com/wenzhixin/bootstrap-table/master/src/bootstrap-table.js"></script>
	</body>
</html>
