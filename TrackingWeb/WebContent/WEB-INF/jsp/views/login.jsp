<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Tracking Admin">
		<meta name="author" content="thinkbox">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/animate/animate.css">

		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="${prop['config.url.resources.base.web']}css/custom.css">

		<!-- Head Libs -->
		<script src="${prop['config.url.resources.base.web']}vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo float-left">
					<img src="${prop['config.url.resources.base.web']}img/logoVM.png" height="54" alt="Porto Admin" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fa fa-user mr-1"></i>LOGIN</h2>
					</div>
					<div class="card-body">
						<form id="form-login" action="<c:url value='j_spring_security_check' />" method="post">
							<div class="form-group mb-3">
								<label>Email</label>
								<div class="input-group input-group-icon">
									<input name="username" type="text" class="form-control form-control-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-envelope"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-3">
								<div class="clearfix">
									<label class="float-left">Clave</label>
									<a href="pages-recover-password.html" class="float-right">Olvidaste tu contraseņa?</a>
								</div>
								<div class="input-group input-group-icon">
									<input name="password" type="password" class="form-control form-control-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-8">
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary mt-2">Acceder</button>
								</div>
							</div>
						</form>
					</div>
				</div>

<!-- 				<p class="text-center text-muted mt-3 mb-3">&copy; Copyright 2017. All Rights Reserved.</p> -->
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="${prop['config.url.resources.base.web']}vendor/jquery/jquery.js"></script>		<script src="${prop['config.url.resources.base.web']}vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		<script src="${prop['config.url.resources.base.web']}vendor/popper/umd/popper.min.js"></script>		<script src="${prop['config.url.resources.base.web']}vendor/bootstrap/js/bootstrap.js"></script>		<script src="${prop['config.url.resources.base.web']}vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		<script src="${prop['config.url.resources.base.web']}vendor/common/common.js"></script>		<script src="${prop['config.url.resources.base.web']}vendor/nanoscroller/nanoscroller.js"></script>		<script src="${prop['config.url.resources.base.web']}vendor/magnific-popup/jquery.magnific-popup.js"></script>		<script src="${prop['config.url.resources.base.web']}vendor/jquery-placeholder/jquery-placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="${prop['config.url.resources.base.web']}js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="${prop['config.url.resources.base.web']}js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="${prop['config.url.resources.base.web']}js/theme.init.js"></script>

	</body>
</html>