<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<section role="main" class="content-body card-margin">
	<header class="page-header">
		<h2>Preparar pedidos</h2>

		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li><a href=""> <i class="fa fa-home"></i>
				</a></li>
				<li><span>Pedidos</span></li>
				<li><span>Pedidos Recibidos</span></li>
				<li><span>Preparar Pedidos</span></li>
			</ol>

			<a class="sidebar-right-toggle" data-open="sidebar-right"><i
				class="fa fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
	<div class="row">
		<div class="col">
			<section role="main" class="content-body card-margin">
				<div class="row">
					<div class="col-lg-6">
						<form id="form" class="form-horizontal">
							<section class="card">
								<header class="card-header">
									<h2 class="card-title">
										Informaci&oacute;n de Pedido #<span id="span-id"></span>
									</h2>
									<!-- 										<p class="card-subtitle"> -->
									<!-- 											Basic validation will display a label with the error after the form control. -->
									<!-- 										</p> -->
								</header>
								<div class="card-body">
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Fecha
											Pedido</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="fechaPedido" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Cantidad</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="cantidad" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Pago</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="pago" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Doc
											Cliente</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="documento" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Nombre</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="nombre" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Destinatario</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="destinatario" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Departamento</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="departamento" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Provincia</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="provincia" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Distrito</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="distrito" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Direcci&oacute;n</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="direccion" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Referencia</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="referencia" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Tel&eacute;fono</label>
										<div class="col-sm-9">
											<input type="text" name="fullname" class="form-control"
												id="telefono" disabled />
										</div>
									</div>
								</div>
							</section>
						</form>
					</div>

					<!-- SECOND PANEL -->

					<div class="col-lg-6">
						<form id="form" class="form-horizontal">
							<section class="card">
								<header class="card-header">

									<h2 class="card-title">
										Asignaci&oacute;n de Gu&iacute;a <span id="span-id"></span>
									</h2>
									<!-- 										<p class="card-subtitle"> -->
									<!-- 											Basic validation will display a label with the error after the form control. -->
									<!-- 										</p> -->
								</header>
								<div class="card-body">
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Rubro</label>
										<div class="col-sm-9">
											<input type="text" name="rubro" class="form-control"
												id="rubro" disabled />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Serie equipo</label>
										<div class="col-sm-9">
											<input type="text" name="serie" class="form-control"
												id="serie" />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Gu&iacute;a de Remisi&oacute;n</label>
										<div class="col-sm-9">
											<input type="text" name="guia" class="form-control"
												id="guia" />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 control-label text-sm-right pt-2">Factura/Boleta</label>
										<div class="col-sm-9">
											<input type="text" name="factura" class="form-control"
												id="factura" />
										</div>
									</div>
								</div>
								<footer class="card-footer">
										<div class="row justify-content-end">
											<div class="col-sm-9">
												<button type="reset" class="btn btn-default" id="btnOmitir">Omitir y Continuar</button>
												<button class="btn btn-primary" id="btnGuardar">Guardar y Continuar</button>
											</div>
										</div>
									</footer>
							</section>
						</form>
					</div>

				</div>
			</section>


		</div>
	</div>
	<!-- end: page -->
</section>

<script
	src="${prop['config.url.resources.base.web']}js/jsp/preparar.js?<%=new Date()%>"></script>

<script>
	Preparar.init("${pageContext.request.contextPath}/");
</script>
