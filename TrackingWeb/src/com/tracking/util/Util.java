package com.tracking.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


public class Util {
//	private static Logger LOGGER = LoggerFactory.getLogger(Util.class);
//
//	public static String getMessage(MessageSource messageSource, String key,
//			Object[] params) {
//		try {
//			if (messageSource != null) {
//				return messageSource.getMessage(key, params,LocaleContextHolder.getLocale());
//			}
//		} catch (Exception ex) {
//			LOGGER.info("no msg key :" + key);
//		}
//		return StringUtils.EMPTY;
//	}
//
//
	public static HttpServletRequest getRequest(){
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		if(requestAttributes!=null) {
			HttpServletRequest request =  (requestAttributes).getRequest();
			return request;
		}
		return null; 
	}
//	
	public static HttpSession getSession(){
		return getRequest().getSession(false);
	}
//	
//	public static Participante obtenerParticipanteLogin(){
//		try{
//			Participante  participante = (Participante) Util.getSession().getAttribute(Constantes.SESSION_USUARIO);
//			return participante;
//		}catch(Exception e){
//			return null;
//		}
//	}
//	
//	public static ConfiguracionWeb obtenerConfiguraciobWeb(){
//		ConfiguracionWeb configuracionWeb = (ConfiguracionWeb)Util.getSession().getAttribute(Constantes.SESSION_CONFIGURACION_WEB);
//		if(configuracionWeb!=null){
//			return configuracionWeb;
//		}
//		LOGGER.error("### No existe configuracion web");
//		return configuracionWeb;
//	}
//	
//	
//	public static Pedido obtenerPedidoSession(){
//		Pedido pedido = (Pedido) Util.getSession().getAttribute(Constantes.SESSION_PEDIDO);
//		if(pedido != null){
//			return pedido;
//		}
//		LOGGER.error("### No existe pedido en sesion");
//		return pedido;
//	}
//	
//	public static Integer obtenerIdCatalogoApi(){
//		Integer idcatalogoApi = null;
//		try {
//			idcatalogoApi =(Integer) Util.getSession().getAttribute(Constantes.SESSION_ID_CATALOGO_API);
//		} catch (Exception e) {
//			LOGGER.error("### No existe id catalogo api en sesion");
//		}
//		return idcatalogoApi;
//	}
//	
//	public static String obtenerLocales(Local locales){
//		StringBuilder sb = new StringBuilder();
//		 for (api.enjoy.local.Result local : locales.getResult()) {
//		 	String nombre = local.getNombre();
//        	String direccion = local.getDireccion();
//			sb.append("<p style='line-height:28px;text-align:left;font-size:15px;color:#767A7C;font-family:Lato, sans-serif;font-weight:300;margin-top:0; margin-bottom:0'>");
//			sb.append("<span style='line-height: 28px; text-align:left;font-size:15px;color:#767A7C;font-family:Lato, sans-serif;font-weight: 600;margin-top:0; margin-bottom:0'>");
//			sb.append("Local " + nombre + ": </span>" + direccion + "</p>");
//		}
//			return sb.toString();
//	}
//	
//	public static ConfiguracionWeb obtenerConfiguracionWeb(){
//		ConfiguracionWeb configuracionWeb = (ConfiguracionWeb) Util.getSession().getAttribute(Constantes.SESSION_CONFIGURACION_WEB);
//		if(configuracionWeb != null){
//			return configuracionWeb;
//		}
//		LOGGER.error("### No existe ConfiguracionWeb en sesion");
//		return configuracionWeb;
//	}

}
