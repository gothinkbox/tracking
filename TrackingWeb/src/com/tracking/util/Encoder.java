package com.tracking.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Encoder {
	static Logger LOGGER = LoggerFactory.getLogger(Encoder.class);
	
	static public String codificar(Integer idParticipante, String fecha) {
		LOGGER.info("codificar : #idParticipante: "+idParticipante);
		String encriptar = idParticipante + "|" + fecha;
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(encriptar);
		return hashedPassword;
	}
	
	static public String fechaActual() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date = new Date();
		String fecha = dateFormat.format(date).toString();
		return fecha;

	}
}