package com.tracking.util;

import java.io.Serializable;

public class MailBcc implements Serializable {


	private static final long serialVersionUID = -3645343198580837324L;
	
	private String pathFileHtml;
	private Object[] data;
	private String subject;
	private String to;
	private String from;
	private String fromName;
	private String[] cc;
	private String[] bcc;
	private String replyTo;
	private String pathAttachtment;
	private String nameAttachtment;
	private String user;
	private String pass;
	public String getPathFileHtml() {
		return pathFileHtml;
	}
	public void setPathFileHtml(String pathFileHtml) {
		this.pathFileHtml = pathFileHtml;
	}
	public Object[] getData() {
		return data;
	}
	public void setData(Object[] data) {
		this.data = data;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String[] getCc() {
		return cc;
	}
	public void setCc(String[] cc) {
		this.cc = cc;
	}
	public String[] getBcc() {
		return bcc;
	}
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}
	public String getReplyTo() {
		return replyTo;
	}
	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}
	public String getPathAttachtment() {
		return pathAttachtment;
	}
	public void setPathAttachtment(String pathAttachtment) {
		this.pathAttachtment = pathAttachtment;
	}
	public String getNameAttachtment() {
		return nameAttachtment;
	}
	public void setNameAttachtment(String nameAttachtment) {
		this.nameAttachtment = nameAttachtment;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getFromName() {
		return fromName;
	}
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}	
}