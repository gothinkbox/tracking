package com.tracking.util;

public class ConfigPropiedad {

	public enum URI {
		
		
		SERVICE_ACCESO_OBTENER_USUARIO("uri.service.admin.login"),
		SERVICE_ROL_ACCESO_OBTENER("uri.service.rol.acceso.obtener"),
		SERVICE_COMMON_UBIGEO("uri.service.common.ubigeo.listar"),
		SERVICE_COMMON_TIPOPAGO("uri.service.common.tipopago.listar"),
		SERVICE_COMMON_SEGMENTO("uri.service.common.segmento.listar"),
		SERVICE_TRACKING_LISTAR("uri.service.tracking.listar"), 
		SERVICE_PEDIDO_PREPARAR("uri.service.tracking.preparar"),
		;

		private final String uri;

		public String getUri() {
			return uri;
		}

		URI(String valor) {
			this.uri = valor;
		}

	}
	
	
	public enum URL_BASE {
		SERVICES_WEB("config.instancia.services.web");

		private final String url;

		URL_BASE(String url) {
			this.url = url;

		}

		public String getUrl() {
			return url;
		}
	}

}
