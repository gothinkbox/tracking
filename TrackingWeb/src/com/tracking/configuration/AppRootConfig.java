package com.tracking.configuration;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tracking.util.Propiedad;

@Configuration
@ComponentScan(basePackages = "com.tracking")
public class AppRootConfig {

	@Value(value = "file:/${propertiesHomeTracking}/configIPTracking.properties")
	Resource configIP;

	@Value(value = "file:/${propertiesHomeTracking}/configUriTracking.properties")
	Resource configUri;
	
	
	@Autowired
	ObjectMapper objectMapper;

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setMessageConverters(messageConverters());
		return restTemplate;
	}
	
	@Bean
	public MappingJackson2HttpMessageConverter jsonHttpMessageConverter() {
		MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
		jsonHttpMessageConverter.setPrefixJson(false);
		jsonHttpMessageConverter.setSupportedMediaTypes(Arrays
				.asList(MediaType.APPLICATION_JSON));
		jsonHttpMessageConverter.setObjectMapper(objectMapper);
		return jsonHttpMessageConverter;
	}

	public List<HttpMessageConverter<?>> messageConverters() {

		List<MediaType> mediatypes = new ArrayList<MediaType>();
		mediatypes.add(new MediaType("text", "plain", StandardCharsets.UTF_8));
		mediatypes.add(new MediaType("*", "*", StandardCharsets.UTF_8));

		StringHttpMessageConverter stringHttpConverter = new StringHttpMessageConverter();
		stringHttpConverter.setSupportedMediaTypes(mediatypes);

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(stringHttpConverter);
		messageConverters.add(jsonHttpMessageConverter());

		return messageConverters;
	}

	@Bean
	public ReloadableResourceBundleMessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("WEB-INF/messages/messages");
		messageSource.setDefaultEncoding("UTF-8");
		messageSource.setCacheSeconds(0);
		return messageSource;
	}

	@Bean
	public PropertiesFactoryBean propertiesHomeTracking() {
		PropertiesFactoryBean propertiesHomeTracking = new PropertiesFactoryBean();
		propertiesHomeTracking.setLocations(configIP, configUri);
		return propertiesHomeTracking;
	}

	@Scope(value = "singleton")
	@Bean(name = "propiedad", initMethod = "init")
	Propiedad propiedad() {
		return new Propiedad();
	}

}
