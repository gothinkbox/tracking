package com.tracking.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.tracking.security.LoginFailureHandler;
import com.tracking.security.LoginSuccessHandler;
import com.tracking.util.SpringUtil;


@Configuration
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter  {

	
	@Autowired
	@Qualifier("userDetailsService")
	UserDetailsService userDetailsService;
	
	@Autowired
	LoginSuccessHandler loginSuccessHandler;
	
	@Autowired
	LoginFailureHandler loginFailureHandler;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		
//		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		auth.userDetailsService(userDetailsService);
		
	}

	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

	  http.sessionManagement().maximumSessions(1);
	  http.authorizeRequests()
	  	.antMatchers("/login/**").permitAll()
	  	.antMatchers("/recuperar/**").permitAll()
		.antMatchers("/**").access("hasRole('ROLE_ADMINISTRADOR')")
		.and()
		.formLogin()
		.loginProcessingUrl(SpringUtil.LOGIN_PROCESSING_URL)
		.loginPage(SpringUtil.LOGIN_PAGE)
		.successHandler(loginSuccessHandler)
		.failureHandler(loginFailureHandler)
		.usernameParameter(SpringUtil.USERNAME_PARAMETER)
		.passwordParameter(SpringUtil.PASSWORD_PARAMETER)
		.and()
		.logout()
		.logoutUrl(SpringUtil.LOGOUT_URL)
		.logoutSuccessUrl(SpringUtil.LOGOUT_SUCCESS_URL)
		.invalidateHttpSession(true)
		.deleteCookies(SpringUtil.JSESSIONID)
		.and()
		.exceptionHandling()
		.accessDeniedPage(SpringUtil.ACCES_DENIED_PAGE)
		.and()
		.csrf()
		.disable()
		;
		
	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder(){
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	
}

