package com.tracking.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tracking.constantes.Constantes;
import com.tracking.util.ConstantesJSP;

@Controller
@RequestMapping("login")
public class LoginController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public String loginPage(
			@RequestParam(value = "error", required = false)String error,
			@RequestParam(value = "logout", required = false)String logout,
			Model model){
		LOGGER.info("# LoginController.login");
		if (error != null) {
			model.addAttribute("error", "Credenciales incorrectas");
		}
		if (logout != null) {
			model.addAttribute("msg", "You've been logged out successfully.");
		}
		return ConstantesJSP.PAGINA_LOGIN;
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    request.getSession().setAttribute(Constantes.SESSION_USUARIO_ADMIN, null);
	    return "redirect:/login?logout";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
	}
	
	/*@RequestMapping(value="403",method = RequestMethod.GET)
	public String error403(Model model){
		model.addAttribute("error_403", "No tiene permisos para acceder a esta secci�n");
		System.out.println("No tiene permisos para acceder a esta secci�n");
		System.out.println(ConstantesJSP.PAGINA_ERROR_403);
		return ConstantesJSP.PAGINA_ERROR_403;
	}*/

}
