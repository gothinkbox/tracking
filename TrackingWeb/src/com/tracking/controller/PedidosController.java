package com.tracking.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tracking.bean.Filtro;
import com.tracking.bean.Pedido;
import com.tracking.util.ConstantesJSP;
import com.tracking.util.Propiedad;
import com.tracking.util.Util;
import com.tracking.util.ConfigPropiedad.URI;

@Controller
@RequestMapping("/pedidos")
public class PedidosController {
	
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private Propiedad propiedad;
	
	@RequestMapping(method = RequestMethod.GET)
	public String inicio(ModelMap model,HttpServletRequest request) {
		return ConstantesJSP.PAGINA_RECIBIDOS;
	}

	@ResponseBody
	@RequestMapping(value="listarPedidos", method = RequestMethod.POST)
	public List<Pedido> listarPedidos(@RequestBody Filtro filtro) {
		
		List<Pedido> lista = new ArrayList<Pedido>();
		try {
			filtro.setIdPedido(0);
			String url = propiedad.getURIServiceWeb(URI.SERVICE_TRACKING_LISTAR);
			ResponseEntity<Pedido[]> result = restTemplate.postForEntity(url, filtro, Pedido[].class);
			for (Pedido pedido : result.getBody()) {
				lista.add(pedido);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
	@ResponseBody
	@RequestMapping(value="prep", method = RequestMethod.POST)
	public Integer listarPedidos(@RequestBody String cad) {
		if (cad.length()>0){
			Util.getSession().setAttribute("checked", cad);
			return 1;
		}else {
			return 0;
		}
	}
	
	@RequestMapping(value="preparar", method = RequestMethod.GET)
	public String preparados(ModelMap model,HttpServletRequest request) {
		return ConstantesJSP.PAGINA_PREPARAR;
	}
	
	@ResponseBody
	@RequestMapping(value="get", method = RequestMethod.GET)
	public Pedido listarPedidos() {
		
		Filtro filtro = new Filtro();
		Pedido pedido = null;
		List<Pedido> lista = new ArrayList<Pedido>();
		String cad = (String) Util.getSession().getAttribute("checked");
		String[] pedidos = cad.split(",");
		if(pedidos!= null && pedidos.length>0) {
			filtro.setIdPedido(Integer.parseInt(pedidos[0]));
			try {
				String url = propiedad.getURIServiceWeb(URI.SERVICE_TRACKING_LISTAR);
				ResponseEntity<Pedido[]> result = restTemplate.postForEntity(url, filtro, Pedido[].class);
				pedido = result.getBody()[0];
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			return null;
		}
		return pedido;
	}
	
	@ResponseBody
	@RequestMapping(value="guardarPrep", method = RequestMethod.POST)
	public Integer guardarPrep(@RequestBody Pedido pedido) {
		Integer resultado = 0;
		try {
			String url = propiedad.getURIServiceWeb(URI.SERVICE_PEDIDO_PREPARAR);
			ResponseEntity<Integer> result = restTemplate.postForEntity(url, pedido, Integer.class);
			resultado = result.getBody();
			String cad = (String) Util.getSession().getAttribute("checked");
			String[] pedidos = cad.split(",");
			String[] pedidosNew = removeElements(pedidos, pedido.getIdPedido().toString());
			Util.getSession().setAttribute("checked",toCSV(pedidosNew));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}
	
	public static String[] removeElements(String[] input, String deleteMe) {
	    List result = new LinkedList();

	    for(String item : input)
	        if(!deleteMe.equals(item))
	            result.add(item);

	    return (String[]) result.toArray(input);
	}
	
	public static String toCSV(String[] array) { String result = ""; if (array.length > 0) { StringBuilder sb = new StringBuilder(); for (String s : array) { sb.append(s).append(","); } result = sb.deleteCharAt(sb.length() - 1).toString(); } return result; }


}