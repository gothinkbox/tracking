package com.tracking.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tracking.bean.Filtro;
import com.tracking.bean.Segmento;
import com.tracking.bean.TipoPago;
import com.tracking.bean.Ubigeo;
import com.tracking.util.ConfigPropiedad.URI;
import com.tracking.util.Propiedad;

@RestController
@RequestMapping("/common")
public class CommonController {
	
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private Propiedad propiedad;

	@RequestMapping(value="listarUbigeos", method = RequestMethod.POST)
	public List<Ubigeo> listarUbigeos(@RequestBody Filtro filtro) {
		
		List<Ubigeo> lista = new ArrayList<Ubigeo>();
		try {
			String url = propiedad.getURIServiceWeb(URI.SERVICE_COMMON_UBIGEO);
			ResponseEntity<Ubigeo[]> result = restTemplate.postForEntity(url, filtro, Ubigeo[].class);
			for (Ubigeo ubigeo : result.getBody()) {
				lista.add(ubigeo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}
	
	@RequestMapping(value="listarSegmento", method = RequestMethod.GET)
	public List<Segmento> listarSegmento() {
		
		List<Segmento> lista = new ArrayList<Segmento>();
		try {
			String url = propiedad.getURIServiceWeb(URI.SERVICE_COMMON_SEGMENTO);
			Map<String, Object> param = new HashMap<String, Object>();
			ResponseEntity<Segmento[]> result = restTemplate.getForEntity(url, Segmento[].class, param);
			for (Segmento segmento : result.getBody()) {
				lista.add(segmento);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}
	
	@RequestMapping(value="listarTipoPago", method = RequestMethod.GET)
	public List<TipoPago> listarTipoPago() {
		
		List<TipoPago> lista = new ArrayList<TipoPago>();
		try {
			String url = propiedad.getURIServiceWeb(URI.SERVICE_COMMON_TIPOPAGO);
			Map<String, Object> param = new HashMap<String, Object>();
			ResponseEntity<TipoPago[]> result = restTemplate.getForEntity(url, TipoPago[].class, param);
			for (TipoPago tipopago : result.getBody()) {
				lista.add(tipopago);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}
}