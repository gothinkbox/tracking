package com.tracking.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tracking.bean.Usuario;
import com.tracking.constantes.Constantes;
import com.tracking.util.ConstantesJSP;

@Controller
@RequestMapping("/")
public class InicioController {

	private Logger LOGGER = LoggerFactory.getLogger(getClass());

	@RequestMapping(method = RequestMethod.GET)
	public String inicio(ModelMap model,HttpServletRequest request) {
		LOGGER.info("#pagina inicio");
		Usuario usuario = (Usuario) request.getSession().getAttribute(Constantes.SESSION_USUARIO_ADMIN);
		if(usuario!=null){
			if(usuario.getRol().getNombreRol().equals("ADMIN")) return "redirect:/reporteParticipante";
			else return "redirect:/home";
		}
		else{
			return "redirect:/login";
		}
	}
	
	@RequestMapping("/home")
	public String home(ModelMap model,HttpServletRequest request) {
		LOGGER.info("#pagina inicio");
		Usuario usuario = (Usuario) request.getSession().getAttribute(Constantes.SESSION_USUARIO_ADMIN);
		return ConstantesJSP.PAGINA_INICIO;
	}



}