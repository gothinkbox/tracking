package com.tracking.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.tracking.bean.RolAcceso;
import com.tracking.bean.Usuario;
import com.tracking.constantes.Constantes;
import com.tracking.util.Propiedad;
import com.tracking.util.ConfigPropiedad.URI;

@Component("LoginSuccessHandler")
public class LoginSuccessHandler extends
		SavedRequestAwareAuthenticationSuccessHandler {

	private static Logger LOGGER = LoggerFactory
			.getLogger(LoginSuccessHandler.class);

	@Autowired
	private Propiedad propiedad;

	@Autowired
	private RestTemplate restTemplate;

	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws ServletException, IOException {
		LOGGER.info("INVOKE LOGIN SUCCESS HANDLER");
		super.onAuthenticationSuccess(request, response, authentication);
		SpringUser user = (SpringUser) authentication.getPrincipal();
		LOGGER.info("## USER LOGIN:"+ user.getUsuario().toMultiLineString());

		Integer idRol = user.getUsuario().getRol().getIdRol();
//		List<RolAcceso> listaAccesos = obtenerAccesos(idRol);

		putParticipanteInSession(user.getUsuario(), request);
//		putSessionListaAccesosInSession(listaAccesos, request);
	}

	public void putParticipanteInSession(Usuario usuario,
			HttpServletRequest request) {
		request.getSession().setAttribute(Constantes.SESSION_USUARIO_ADMIN,usuario);
	}

	public void putSessionListaAccesosInSession(List<RolAcceso> listaAccesos,
			HttpServletRequest request) {
		LOGGER.info("## Colocando lista de accesos en session");
		request.getSession().setAttribute(Constantes.SESSION_LISTA_ACCESOS,listaAccesos);

	}

	private List<RolAcceso> obtenerAccesos(Integer idRol) {

		List<RolAcceso> lista = new ArrayList<RolAcceso>();
		try {
			String url = propiedad.getURIServiceWeb(URI.SERVICE_ROL_ACCESO_OBTENER);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("idRol", idRol);
			ResponseEntity<RolAcceso[]> result = restTemplate.getForEntity(url,RolAcceso[].class, param);
			for (RolAcceso rolAcceso : result.getBody()) {
				lista.add(rolAcceso);
			}
		} catch (Exception e) {
			LOGGER.error("##### Ocurrio un error de Exception : ", e);
		}
		return lista;

	}


}
