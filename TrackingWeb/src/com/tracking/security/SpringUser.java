package com.tracking.security;

import java.util.Collection;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.tracking.bean.Usuario;

public class SpringUser extends User {

	private static final long serialVersionUID = -3412180338469178220L;

	private Usuario usuario;
	
	public SpringUser(Usuario usuario, String user, String password,
			boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(user, password, enabled, accountNonExpired,
				credentialsNonExpired, accountNonLocked, authorities);
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String toJsonString(){
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.JSON_STYLE);
	}
	
	public String toMultiLineString(){
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
