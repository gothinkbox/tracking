package com.tracking.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tracking.bean.Rol;
import com.tracking.bean.Usuario;
import com.tracking.util.Propiedad;
import com.tracking.util.ConfigPropiedad.URI;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	private static Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private Propiedad propiedad;

	@Override
	public UserDetails loadUserByUsername(@RequestParam String username) throws UsernameNotFoundException {
		LOGGER.info("#userDetailsService.loadUserByUsername:" + username);
		
		Set<Rol> roles = null;
		Usuario usuario = new Usuario();
		try {
			String url = propiedad.getURIServiceWeb(URI.SERVICE_ACCESO_OBTENER_USUARIO);
			usuario.setEmail(username);
			ResponseEntity<Usuario> result=restTemplate.postForEntity(url, usuario, Usuario.class);
			usuario = result.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			usuario = new Usuario(); 
		}
		

		if (usuario.getEmail()== null) {
			throw new UsernameNotFoundException("Failed to login as ");
		}

		LOGGER.info("#Participante encontrado en la base de datos nueva web:"+ usuario.getEmail());
		roles = new HashSet<Rol>(Arrays.asList(usuario.getRol()));
		List<GrantedAuthority> authorities = buildUserAuthority(roles);

		return buildUserForAuthentication(usuario, authorities);
	}

	private User buildUserForAuthentication(Usuario usuario,
			List<GrantedAuthority> authorities) {
		boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;

		LOGGER.info("authorities : "+authorities.get(0));
		return new SpringUser(usuario, usuario.getEmail() , usuario.getClave(),
				enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		
	}

	private List<GrantedAuthority> buildUserAuthority(Set<Rol> roles) {
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>(0);
		for (Rol rol : roles) {
			setAuths.add(new SimpleGrantedAuthority("ROLE_" + rol.getNombreRol()));
		}
		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(setAuths);
		return result;
	}

}
