package com.tracking.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tracking.util.Propiedad;
import com.tracking.util.SpringUtil;
import com.tracking.bean.Usuario;
import com.tracking.util.ConfigPropiedad.URI;

@Component("LoginFailureHandler")
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private Propiedad propiedad;

	@Autowired
	private ObjectMapper objectMapper;

	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		
		String urlToRedirect = request.getContextPath()+ SpringUtil.AUTHENTICATION_FAILURE_URL;
		try {
			String username = request.getParameter(SpringUtil.USERNAME_PARAMETER);
			
			String urlToRedirectRegistro=request.getContextPath()+ SpringUtil.AUTHENTICATION_REGISTER_URL;
			
			if(!StringUtils.isBlank(username)){
				System.out.println("usuario no es vacio "+username);
				Integer estadoRegistro;
				Usuario usuario= obtenerUsuario(username);
				
				if(usuario != null){
					estadoRegistro = usuario.getEstadoUsuario();
				}else{
					estadoRegistro = -1;
				}
				System.out.println("ESTADO REGISTRO: "+estadoRegistro);
			 	if(estadoRegistro == 2) {
			 		System.out.println("response.sendRedirect:  "+urlToRedirectRegistro);
			 		Base64 base = new Base64();
			 		response.sendRedirect(urlToRedirectRegistro+new String(base.encodeBase64(username.getBytes())));
			 	}else{
			 		System.out.println("urlToRedirect");
			 		response.sendRedirect(urlToRedirect);
			 	}
			}else{
				System.out.println("urlToRedirect");
				response.sendRedirect(urlToRedirect);
			}
		} catch (Exception e) {
			response.sendRedirect(urlToRedirect);
		}

	}

	private  Usuario obtenerUsuario (String email){
		Usuario usuario = null;
		try {
			String url = propiedad.getURIServiceWeb(URI.SERVICE_ACCESO_OBTENER_USUARIO);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+url);
			usuario = new Usuario();
			usuario.setEmail(email);
			ResponseEntity<Usuario> result=restTemplate.postForEntity(url, usuario, Usuario.class);
			usuario = result.getBody();
		} catch (Exception e) {
			LOGGER.error("##### Ocurrio un error de Exception : ", e);
			e.printStackTrace();
			usuario = null;
		}
		return usuario;
	}


}
