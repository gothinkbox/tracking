<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>

<aside id="sidebar-left" class="sidebar-left">

	<div class="sidebar-header">
		<div class="sidebar-title">Men&uacute;</div>
		<div class="sidebar-toggle hidden-xs"
			data-toggle-class="sidebar-left-collapsed" data-target="html"
			data-fire-event="sidebar-left-toggle">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<div class="nano">
		<div class="nano-content">
			<nav id="menu" class="nav-main" role="navigation">

				<ul class="nav nav-main">
					<li class="nav-parent"><a class="nav-link" href="#"> <i
							class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Pedidos</span>
					</a>
						<ul class="nav nav-children">
							<li><a class="nav-link" id="btnRecibidos" href="#">Pedidos Recibidos</a></li>
							<li><a class="nav-link" id="btnPreparados" href="#">Pedidos Preparados</a></li>
							<li><a class="nav-link" id="btnAsignados" href="#">Pedidos Asignados</a></li>
						</ul></li>
				</ul>
			</nav>
			<hr class="separator" />
		</div>

		<script>
			// Maintain Scroll Position
			if (typeof localStorage !== 'undefined') {
				if (localStorage.getItem('sidebar-left-position') !== null) {
					var initialPosition = localStorage
							.getItem('sidebar-left-position'), sidebarLeft = document
							.querySelector('#sidebar-left .nano-content');

					sidebarLeft.scrollTop = initialPosition;
				}
			}
		</script>
		
		<script src="${prop['config.url.resources.base.web']}js/jsp/menu.js"></script>

		<script>
			Menu.init("${pageContext.request.contextPath}/");
		</script>


	</div>

</aside>