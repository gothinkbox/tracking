<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link href="${prop['config.url.resources.base.web']}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300' rel='stylesheet' type='text/css'>
    <link href="${prop['config.url.resources.base.web']}css/style.css" rel="stylesheet" type="text/css" />
    
    
    <script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jquery.min.js"></script>
    <script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" type="text/javascript"></script>
    
    <link rel="shortcut icon" href="${prop['config.url.resources.base.web']}imagenes/login/favicon-9.ico" />
     <title><spring:message code="participante.titulo.banco.general"/></title>
</head>

<style>
body.error_404{background: url("${prop['config.url.resources.base.web']}imagenes/login/bg-login.png");
               background-repeat: no-repeat;background-size: cover;}
</style>

<div class="content-push" style="min-height: 600px;">
   <div class="title-subpage"></div>
   <div class="col-md-8 col-md-offset-2" style="margin-top:50px;">
       <div class="text-center">
          <p style="font-size: xx-large;">ERROR</p>
          <h3 style="font-size: xx-large;">404</h3>
          <p style="font-size: xx-large;">P�GINA NO ENCONTRADA</p>
      </div>
   </div>

</div>
  

    <script src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/global.js"></script>

    <!-- custom scrollbar -->
    <script src="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"></script>

</body>
</html>
