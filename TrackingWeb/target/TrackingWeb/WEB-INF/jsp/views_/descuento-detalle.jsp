<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<!-- Banner -->

	<c:choose>
		<c:when test="${(descuento.idcategoria == 7) || (descuento.idcategoria == 50)}">	
	 <div class="banner_estatico desktop">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}img/bg/bg-beneficio.png);background-position: center center!important;">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

        <!-- Banner Responsive-->
        <div class="banner_estatico mobile">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/bg-beneficio.png);background-position: center center!important;">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>
	
	
	
		</c:when>
		<c:otherwise>	
	 <div class="banner_estatico desktop">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}img/bg/bg-catalogo.png); background-position: center!important;">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

        <!-- Banner Responsive-->
        <div class="banner_estatico mobile">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/familia.png)">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>
		</c:otherwise>
	</c:choose>	
		
<div class="content-push">
	<div class="bread-crumb">
		<a href="${pageContext.request.contextPath}">Home</a> <span
			class="lnr lnr-chevron-right"></span> <a
			href="${pageContext.request.contextPath}/descuentos">Cat�logo</a> <span
			class="lnr lnr-chevron-right"></span> <span>Detalle de
			descuento</span>
	</div>

	<div
		class="information-blocks detail-product div-descuento-detalle-cabecera">
	</div>
	
</div>
	
	<!-- COMIENZO NUEVO -->

<div class="full-width">	
	<div class="container">
		<div class="tabs-container style-1">
                <div class="swiper-tabs tabs-switch tabs-left" style="margin-top: 0;text-align: center;display: inline-block;padding-top: 15px;">
                    <a class="tab-switcher active">Restricciones</a>
                    <c:if test="${descuento.idcategoria != 7 && descuento.idcategoria != 50}">
                    	<a class="tab-switcher tab-locales">Locales</a>
                    </c:if>
                    <a class="tab-switcher">Detalles</a>                    
                    <div class="clear"></div>
                </div>
                <div class="contenido-tabs">
                	 <div class="tabs-entry">
                        <div class="article-container style-1">
                            <div class="swiper-tabs">
                                <div class="title">Restricciones</div>
                                <div class="list" style="display:block;">
                                    <div class="col-md-12 information-entry">
                                        <ul class="terminos-list">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                	
					<c:if test="${(descuento.idcategoria != 7) && (descuento.idcategoria != 50)}">
						<div class="tabs-entry">
						<div class="article-container style-1">
							<div class="swiper-tabs">
								 <div class="post-slider arrow-style">
								 	<div class="title tab-locales">Locales</div>
								 	<div class="wrap-item information-entry list text-center div-locales">
								 	
								 	</div>
								 </div>
							
							</div>
						</div>
					</div>
					</c:if>
                    <div class="tabs-entry">
                        <div class="article-container style-1">
                            <div class="swiper-tabs">
                                <div class="title">Detalles</div>
                                <div class="list">
	                                 <div class="col-md-12 information-entry">
	                                 	<ul class="pasos-descuento">
	                                 	</ul>
	                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
	</div>
	
</div>
	<!-- FIN NUEVO -->

	<!-- PODR�A INTERESARTE -->
 <div class="content-push">
            <!-- PODR�A INTERESARTE -->
            <div class="information-blocks">
            <div class="title-subpage">Podr�a interesarte</div>
		<div class="col-md-12 swiper-destacados" style="padding: 0;"></div>
	</div>
</div>




<div id="add-imprimir" class="overlay-popup">
	<div class="overflow">
		<div class="table-view">
			<div class="cell-view">
				<div class="close-layer"></div>
				<div class="popup-container small">
					<div class="row">
						<div class="col-sm-12 col-xs-8 information-entry">
							<div class="product-detail-box">
								<div class="alert_2">Felicitaciones, has canjeado un
									descuento.</div>
								<p>En breve recibir&aacute;s un correo electr&oacute;nico
									con el descuento. &iexcl;Recuerda verificar la secci&oacute;n
									de Spam de tu buz&oacute;n de correos!</p>
								<a class="button style-10 btn-aceptar" style="float: right;">ACEPTAR</a>
							</div>
						</div>
					</div>
					<div class="close-popup"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jsp/descuento-detalle.js?v=<%System.currentTimeMillis();%>>"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>

<script id="descuento-detalle-cabecera-template"
	type="text/x-handlebars-template">
		<div class="row">
					<div class="col-sm-6">
                        <div class="product-slider arrow-style">
                            <div class="row">
                                <div class="wrap-item items-carrusel">
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="col-sm-6">
                            <div class="product-detail-box">
                                <h1 class="product-title">{{nombremarca}}</h1>
                                <div class="descuento"><span>{{textoSecundario}}</span> {{texto}}</div>
                                <p style="font-weight:bold;" class="text-red">{{descripcion}}</p>
                                <div class="product-description type detail-info-entry" style="padding: 5px 0;border-bottom: 1px solid #e5e5e5;">
                                    <p><img src="${prop['config.url.resources.base.web']}img/icons/{{iconocanje}}" style="margin:0 10px;" alt="">{{nombrecanje}}</p>
                                </div>
                                <div class="product-description detail-info-entry">
                                <div class="buttons showPrint" style="visibility:hidden;">
                                    <a class="button style-10 open-imprimir" id="openImprimir">Imprimir cup�n</a>
                                    <a class="button style-10 open-mail" id="openMail" style=" margin-left: 20px;">Recibir por mail</a>
                                    <div class="clear"></div>
                                </div>
                            </div>
                    </div>
				</div>
</script>
<script id="beneficio-detalle-cabecera-template"
	type="text/x-handlebars-template">
		<div class="row">
                    <div class="col-sm-6">
                        <div class="product-preview-box">
                            <img src="{{imagendetalle}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                            <div class="product-detail-box">
                                <h1 class="product-title">{{nombremarca}}</h1>
                                <div class="descuento"><span>{{textoSecundario}}</span> {{texto}}</div>
 								<p style="font-weight:bold;" class="text-red">{{descripcion}}</p>
								<div class="product-description type detail-info-entry" style="padding: 5px 0;border-bottom: 1px solid #e5e5e5;">
                                    <p><img src="${prop['config.url.resources.base.web']}img/icons/icon-phone.png" style="margin:0 10px;" alt="">Comun&iacute;cate con Bienestar Social - Mapfre</p>
                                </div>
                            </div>
                    </div>
                </div>

</script>
<script id="descuento-template" type="text/x-handlebars-template">
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 shop-grid-item descuento"> 
                                <div class="item">
                                    <div class="item-product">
                                        <div class="item-product-thumb">
                                            <a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}" class="product-thumb-link"><img class="img-responsive" src="{{imagen}}" alt="" /></a>
                                            <div class="product-extra-mask">
                                                <div class="product-extra-link">
                                                    <a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}" class="product-detail">
														<span class="lnr lnr-magnifier"></span>
														<span class="vermas">Ver m�s</span>
													</a>
                                                </div>
                                            </div>
                                            <div class="descr">
                                                <span class="antes">{{textoSecundario}}</span>
                                                <p>{{texto}}</p>
                                            </div>
                                        </div>
                                        <div class="item-product-info">
                                            <h3 class="title-product"><a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}">{{nombre}}</a></h3>
                                            <div class="info-price">
                                                <span>{{descripcionCorta}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
</script>

<script id="locales-template" type="text/x-handlebars-template">
		 <div class="item">
			<a href="{{urlMapa}}" target="_blank">
				<img style="padding: 10px!important;" src="{{imagenGoogle}}" alt="">
			</a>
			<a href="{{urlMapa}}" target="_blank">
				<h4 title="{{nombre}}" class="titlelocal">{{nombre}}</h4>
			</a>
			<a href="{{urlMapa}}" target="_blank">
				<p title="{{direccion}}">{{direccion}}</p>
			</a>
 			
		</div>
</script>

<script>
	$(function() {
		
// 		$('#tab-terminos').click();
		
		
		$('.btn-aceptar').on('click', function() {
			$('.overlay-popup.visible').removeClass('active');
			setTimeout(function() {
				$('.overlay-popup.visible').removeClass('visible');
			}, 500);
		});
		DescuentoDet.init("${pageContext.request.contextPath}/",
				"${idDescuento}","${descuento.idcategoria}");
		
		
	});
</script>