<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link rel="shortcut icon" href="${prop['config.url.resources.base.web']}imagenes/login/favicon-9.ico" />
    
    <link href="${prop['config.url.resources.base.web']}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300' rel='stylesheet' type='text/css'>
    <link href="${prop['config.url.resources.base.web']}css/style.css" rel="stylesheet" type="text/css" />
            <script src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
     <script type="text/javascript" src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>

    <script src="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"></script>

    <!-- custom scrollbar -->
    <script src="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"></script>
</head>

<body class="style-10 fondo" style="background-image: url(${prop['config.url.resources.base.web']}imagenes/bg-login.png)!important; background-color:#004475;">
  <img src="${prop['config.url.resources.base.web']}imagenes/logo-15.png" alt="" class="logo-login" height="40px" width="auto">
    <div class="register-box login-fondo" align="center" style="border:0 !important;">
      	<form id="form-registro" method="POST">
            <div class="button-box">
  				<p style="font-style: normal;text-align:center;padding-bottom: 20%;color: #fff;font-size: 150%;padding-top: 40%;">Esta p�gina ya no est� disponible</p>
                <a href="${pageContext.request.contextPath}/" class="btn-registro button btn-3">Aceptar</a>
            </div> 
		</form>
	</div>
</body>
</html>