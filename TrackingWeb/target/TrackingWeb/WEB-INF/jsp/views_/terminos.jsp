<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>



<!-- Banner -->
 <div class="banner_estatico desktop">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/bg-carrito.png">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

        <!-- Banner Responsive-->
        <div class="banner_estatico mobile">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/bg-carrito.png)">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

<div class="information-blocks content-push">
<input type="hidden" id="tyc" value="${prop['config.url.resources.base.web']}terminos.html">
	<div class="bread-crumb">
		<a href="${pageContext.request.contextPath}">Home</a> <span
			class="lnr lnr-chevron-right"></span> <span>T�rminos y
			condiciones</span>
	</div>
	<div class="row col-md-10 col-md-offset-1">
		<div class="title-subpage">T�rminos y condiciones</div>
		<div class="tyc-body">
			<p class="text-justify"></p>
		</div>
	</div>
</div>


<script>
	$(function() {
		$.ajax({ url: $('#tyc').val(), success: function(data) { $('.tyc-body').prepend(data); } });
	});
</script>



