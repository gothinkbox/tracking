<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!-- Banner -->

<div class="banner_estatico mis_ventas">
    <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/bg-perfil.png);">
        <div class="internas-banner-content content-push">
            <div class="cell-view">
                <h1 class="title">Mi Perfil</h1>
                <ul class="submenu_banner"> 
                <li><a href="${pageContext.request.contextPath}/perfil">Datos personales</a></li>
                <li style="display: inherit !important;"><a href="${pageContext.request.contextPath}/perfil/sede">Mi sede de trabajo</a></li>
                <li><a class="active">Historial de compra</a></li>
                </ul>
            </div>
            <img class="icon-banner" width="auto" height="100px" src="${prop['config.url.resources.base.web']}imagenes/icon_perfil.png">
        </div>
    </div>
</div>

   <div class="content-push mi_perfil box-checkout">

                <div class="title-subpage">Historial de compra</div>
                    <div class="text-periodo">Periodo
                        <select id="cbo-dias" class="search" style="margin-left:0;width:195px;" id="" rel="4">
                                <option value="0">Todos</option>
                                <option value="1">�ltimos 30 d�as</option>
                                <option value="2">�ltimos 15 d�as</option>
                                <option value="3">�ltima semana</option>
                        </select>
                    </div>
                <div class="left-puntos table">
                    <table class="puntos-table" id="tabla-historial">                         
                        <tbody>
                        	<tr>
                                <th>Fecha</th>
                                <th>N� de pedido</th>
                                <th>Cantidad de art�culos</th>
                                <th>Precio</th>
                                <th>Ver detalle</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                 <div class="col-md-4 col-md-offset-8 spad paginado" align="right"></div>
                
                
<!--  DETALLE DE HISTORIAL          -->
             <div class="col-md-12 estado_pedido div-oculto" id='div-pedido_detalle'>
 				<p class="block-title"></p>
                    <div class="col-md-8 col-md-offset-2">

                        <div class="information-blocks">
                            <div class="block-title size-3 accordeon-title active"><span class="title-check-1 block-title">Detalle de la compra  </span></div>
                            <div class="accordeon accordeon-entry ae-1" style="display:block">
                                <div class="information-blocks">
                                    <div class="table-responsive">
                                        <table class="cart-table tabla-detalle-pedido">
<!--                                             <tr> -->
<!--                                                 <th class="column-1">Producto</th> -->
<!--                                                 <th class="column-3">Cantidad</th> -->
<!--                                                 <th class="column-4">Total Puntos</th> -->
<!--                                                 <th class="column-5"></th> -->
<!--                                             </tr> -->
<!--
---------------------------------------    DETALLE DE PEDIDO      ------------------------------------------
                                           <tr>
                                                <td>
                                                    <div class="traditional-cart-entry">
                                                        <a href="#" class="image"><img src="img/licuadora_2.png" alt=""></a>
                                                        <div class="content">
                                                            <div class="cell-view">
                                                                <a href="#" class="title">Licuadora Artisan Roja</a>
                                                                <a href="#" class="tag">Kitchen Aid</a>
                                                                <div class="inline-description">Color <span>rojo</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="quantity-selector detail-info-entry">
                                                        <div class="entry number-minus">&nbsp;</div>
                                                        <div class="entry number">2</div>
                                                        <div class="entry number-plus">&nbsp;</div>
                                                    </div>
                                                </td>
                                                <td><div class="subtotal">8800 Pts.</div></td>
                                            </tr>
----------------------------------------------------------------------------------------------------------------------

                                            <tr>
                                                <td>
                                                    <div class="traditional-cart-entry">
                                                        <a href="#" class="image"><img src="img/licuadora_2.png" alt=""></a>
                                                        <div class="content">
                                                            <div class="cell-view">
                                                                <a href="#" class="title">Licuadora Artisan Roja</a>
                                                                <a href="#" class="tag">Kitchen Aid</a>
                                                                <div class="inline-description">Color <span>rojo</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="quantity-selector detail-info-entry">
                                                        <div class="entry number-minus">&nbsp;</div>
                                                        <div class="entry number">2</div>
                                                        <div class="entry number-plus">&nbsp;</div>
                                                    </div>
                                                </td>
                                                <td><div class="subtotal">8800 Pts.</div></td>
                                            </tr> -->
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 information-entry">
                                            <div class="cart-summary-box">
<!--                                                 <div class="sub-total">Subtotal: 19800 Pts</div> -->
                                                <div class="grand-total total-puntos"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
	                    <div class="information-blocks">
                            <div class="block-title size-3 accordeon-title"><span class="title-check-2 block-title">Lugar de env�o</span></div>
                            <div class="range-wrapper accordeon-entry ae-2">
                                <form class="formCheckout" name="form_Checkout">
                                    <div class="col-sm-12 m_bottom_15 m_bottom_15 padding_null">

                                        <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null div-sede subtotal">
                                        <label style="margin-bottom:0;">Sede actual de trabajo</label>
<!--                                             <select id="h_tipo_doc" name="h_tipo_doc" rel="4"> -->
<!--                                                     <option value="0">Sede 1</option> -->
<!--                                                     <option value="1">Sede 2</option> -->
<!--                                                     <option value="2">Sede 3</option> -->
<!--                                             </select> -->
                                        </div>
                                    </div>                                   
                                </form>
                            </div>
                        </div>

                    </div> 
                </div>
                
<!--  - - - - - - - - - - - - -DETALLE DE HISTORIAL - - - - - - - - - - - - - -->                              
            </div>

<script src="${prop['config.url.resources.base.web']}js/jsp/historial.js"></script>

<script>
	$(function() {
		Historial.init("${pageContext.request.contextPath}/", "${prop['config.url.resources.base.web']}");
	});
</script>