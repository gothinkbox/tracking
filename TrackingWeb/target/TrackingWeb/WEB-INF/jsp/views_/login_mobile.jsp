<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
<script src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
</head>

<body>
	<div style="background-image: url(${prop['config.url.resources.base.web']}img/loading.gif);">
		<form style="display: none;" id="form-login" action="<c:url value='j_spring_security_check' />" method='POST'>
			<input type="hidden" name="password" value="${tipo}">
			<input type="hidden" name="username" value="${documento}">
		</form>
	</div>
</body>
<script>
	$('#form-login').submit();
</script>

</html>
