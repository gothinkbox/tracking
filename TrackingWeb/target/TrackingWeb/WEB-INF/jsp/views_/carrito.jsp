<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!-- Banner -->
<div class="banner_estatico desktop">
    <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}img/bg/bg-carrito.png)">
        <div class="internas-banner-content content-push">
            <div class="cell-view">
            </div>
        </div>
    </div>
     <div class="breadcrumb-banner">
        <div class="content-push">
            <a href="${pageContext.request.contextPath}/">Home</a><span class="acti">Carrito</span>
        </div>
    </div>
</div>


<div class="banner_estatico mobile">
    <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/bg-carrito.png)">
        <div class="internas-banner-content content-push">
            <div class="cell-view">
            </div>
        </div>
    </div>
     <div class="breadcrumb-banner">
        <div class="content-push">
            <a href="${pageContext.request.contextPath}/">Home</a><span class="acti">Carrito</span>
        </div>
    </div>
</div>




<input type="hidden" id="page-carrito" value="carrito">

<div class="content-push box-checkout carrito">
      <div class="mozaic-banners-wrapper type-2">
              <div class="text-left col-md-3 col-md-offset-9 col-xs-offset-4">
              		<a class="button style-10" href="${pageContext.request.contextPath}/checkout" style="margin-bottom:20px;float:right;">HACER CHECKOUT</a></div>

              <div class="banner-column col-sm-12">
                  <div class="information-blocks">
                          <div class="information-blocks">
                              <div class="table-responsive">
                                  <table class="cart-table pedido-dinamico">

                                  </table>
                              </div>
                          <div class="col-md-3" style="float:left; margin-top:20px;">
                          <a class="button style-11 btn-actualizar">ACTUALIZAR</a></div>
                          <div class="col-md-3" style="float:right; margin-top:20px; margin-bottom:20px;">
                          <a class="button style-11" href="${pageContext.request.contextPath}/productos" style="float:right;">SEGUIR COMPRANDO</a></div>

                          <div class="col-md-6 col-md-offset-6">
                                  <div class="col-md-6 information-entry box-right">
                                      <div class="cart-summary-box">
                                          <div class="grand-total">Total: <span id="puntos-totales">0</span> Pts</div>
                                          <a class="button style-10" href="${pageContext.request.contextPath}/checkout">HACER CHECKOUT</a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                         
                  </div>
          
              </div>
      </div>

      <div class="hidden-sm hidden-xs" style="height: 30px;"></div>
</div>



<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jsp/carrito.js"></script>           
<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>



<script id="pedido-detalle" type="text/x-handlebars-template" >
  <tr>
	  <td>
		  <div class="traditional-cart-entry" data-id="{{producto.idProducto}}">
			  <a class="image"><img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt=""></a>
			  <div class="content">
				  <div class="cell-view">
					  <a href="${pageContext.request.contextPath}/productos/detalle/{{producto.idProducto}}" class="title">{{producto.nombreProducto}}</a>
					  <div class="inline-description"><span>{{producto.marca.nombreMarca}}</span></div>
				  </div>
			  </div>
		  </div>
	  </td>
	  <td>{{producto.puntosProducto}} Pts.</td>
	  <td>
		  <div class="quantity-selector detail-info-entry">
			  <div class="entry number-minus">&nbsp;</div>
			  <div class="entry number" id="number-{{producto.idProducto}}">{{cantidad}}</div>
			  <div class="entry number-plus">&nbsp;</div>
		  </div>
	  </td>
	  <td><div class="subtotal">{{puntosTotal}} Pts.</div></td>
	  <td><a class="remove-button" id="{{producto.idProducto}}" ><i class="fa fa-times"></i></a></td>
  </tr>
</script>




<script>
	 $(function() {
		 Carrito.init("${pageContext.request.contextPath}/");
	  });
</script>











