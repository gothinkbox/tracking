<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<script>
if($('#usuarioSesion').val()== ''){
	$(location).attr('href', '${pageContext.request.contextPath}/');
}
</script>

<!-- Banner -->
 <div class="banner_estatico desktop">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}img/bg/bg-faq.png);background-position: center!important;">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

        <!-- Banner Responsive-->
        <div class="banner_estatico mobile">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/bg-mi-perfil.png)">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>





<div class="information-blocks content-push">
	<div class="bread-crumb">
		<a href="${pageContext.request.contextPath}/">Home</a> <span
			class="lnr lnr-chevron-right"></span> <span>Preguntas
			frecuentes</span>
	</div>

	<div class="row">
		<div class="title-subpage">Preguntas Frecuentes</div>
		<div class="col-md-6">
			<div class="accordeon">
				<c:forEach items="${impar}" var="preg1">
					<div class="accordeon-title">${preg1.pregunta}</div>
                        <div class="accordeon-entry">
                            <div class="article-container style-1">
                                <p>${preg1.respuesta}</p>
                            </div>
                        </div>				
				</c:forEach>
			</div>
		</div>
		<div class="col-md-6 ">
			<div class="accordeon">
				<c:forEach items="${par}" var="preg2">
					<div class="accordeon-title">${preg2.pregunta}</div>
                        <div class="accordeon-entry">
                            <div class="article-container style-1">
                                <p>${preg2.respuesta}</p>
                            </div>
                        </div>				
				</c:forEach>
			</div>
		</div>
	</div>
	
	
</div>
