<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link href="${prop['config.url.resources.base.web']}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/style.css" rel="stylesheet" type="text/css" />
    
    
    <script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jquery.min.js"></script>
    <script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jquery-migrate.min.js"></script>
    <script src="${prop['config.url.resources.base.admin']}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
    
    <link rel="shortcut icon" href="${prop['config.url.resources.base.web']}imagenes/login/favicon-9.ico" />
    <title><spring:message code="participante.titulo.banco.general"/></title>

<style type="text/css">

    .fondo{
        background-image: url("${prop['config.url.resources.base.web']}imagenes/login/bg-login.png")  !important;
        background-repeat: no-repeat !important;
        background-size:140%;
        background-position:top center;
        background-color: #29292d;
    }

    .btn-1, .btn-2{line-height: 5px;border-radius: 4px;width: 100%;padding: 18px;font-weight: 900;}

    .btn-1{background: #f2150a;border: 2px #f2150a solid;color: #fff;} 
    .btn-1:hover{background: #FFF;border: 2px #f2150a solid;color:#f2150a;}     

    .btn-2{background: #101c28;border: 2px #101c28 solid !important ;overflow: hidden;color: #fff;} 
    .btn-2:hover{background: #FFF;border: 2px #101c28 solid;color:#001848;} 
</style>
</head>

<body class="style-10 fondo">
    <div class="container-fluid">
        <div class="information-blocks">
            <div class="row">
                    <div class="login-box login-fondo col-lg-4 col-lg-offset-4 col-sm-8 col-sm-offset-2 col-xs-12" align="center" style="border:0 !important;">
                        <p>Muchas gracias por inscribirte al programa  Beneficios Chamgam Serie CS.</p>
                        <p>En breve nuestro servicio de atenci�n al cliente te enviar&aacute; la confirmaci&oacute;nde tu registro.</p>
                         <form style="margin-top:20px;">
                            <a href="${pageContext.request.contextPath}/" class="button btn-1">ACEPTAR</a>
                        </form>
                    </div>
            </div>
        </div>
    </div>
</body>


</html>