<%@ page contentType="text/html;charset=UTF-8" language="java"
	trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,500,600,700"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/font-linearicons.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/bootstrap.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/bootstrap-theme.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.transitions.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.theme.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/theme.css"
	media="all" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/responsive.css"
	media="all" />

<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-ui.min.js"></script>

<link
	href="${prop['config.url.resources.base.web']}css/components-rounded.css"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link
	href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300'
	rel='stylesheet' type='text/css'>
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />


<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-migrate.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"></script>
<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/global.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/validate/additional-methods.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-bootpag.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery.fancybox.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery-ui.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/owl.carousel.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery.bxslider.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/theme.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery.validate.min.js"></script>

<link href="${prop['config.url.resources.base.web']}fonts/fonts.css"
	rel="stylesheet" type="text/css" charset="utf-8" />
<link rel="shortcut icon"
	href="${prop['config.url.resources.base.web']}img/favicon.ico" />
<style>
.block-with-text {
  overflow: hidden;
  position: relative; 
  line-height: 16px !important;
  max-height: 16px !important; 
}

</style>
</head>
<body>


<!-- Banner -->
<div class="content-push">
	<div
		class="information-blocks detail-product div-descuento-detalle-cabecera">
	</div>

	<div class="tabs-container style-1">
                <div class="swiper-tabs tabs-switch tabs-left" style="margin-top: 0;text-align: center;display: inline-block;padding-top: 15px;">
                    <a class="tab-switcher active">Restricciones</a>
                    <c:if test="${descuento.idcategoria != 7 && descuento.idcategoria != 50}">
                    	<a class="tab-switcher tab-locales">Locales</a>
                    </c:if>
                    <a class="tab-switcher">Detalles</a>                    
                    <div class="clear"></div>
                </div>
                <div class="contenido-tabs">
                	 <div class="tabs-entry">
                        <div class="article-container style-1">
                            <div class="swiper-tabs">
                                <div class="title">Restricciones</div>
                                <div class="list" style="display:block;">
                                    <div class="col-md-12 information-entry">
                                        <ul class="terminos-list">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                	
					<c:if test="${(descuento.idcategoria != 7) && (descuento.idcategoria != 50)}">
						<div class="tabs-entry">
						<div class="article-container style-1">
							<div class="swiper-tabs">
								 <div class="post-slider arrow-style">
								 	<div class="title tab-locales">Locales</div>
								 	<div class="wrap-item information-entry list text-center div-locales">
								 	
								 	</div>
								 </div>
							
							</div>
						</div>
					</div>
					</c:if>
                    <div class="tabs-entry">
                        <div class="article-container style-1">
                            <div class="swiper-tabs">
                                <div class="title">Detalles</div>
                                <div class="list">
	                                 <div class="col-md-12 information-entry">
	                                 	<ul class="pasos-descuento">
	                                 	</ul>
	                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
 </div>
 
 <div class="content-push">
            <!-- PODRÍA INTERESARTE -->
            <div class="information-blocks">
            <div class="title-subpage">Podría interesarte</div>
		<div class="col-md-12 swiper-destacados" style="padding: 0;"></div>
	</div>
</div>



<div id="add-imprimir" class="overlay-popup">
	<div class="overflow">
		<div class="table-view">
			<div class="cell-view">
				<div class="close-layer"></div>
				<div class="popup-container small">
					<div class="row">
						<div class="col-sm-12 col-xs-8 information-entry">
							<div class="product-detail-box">
								<div class="alert_2">Felicitaciones, has canjeado un
									descuento.</div>
								<p>En breve recibir&aacute;s un correo electr&oacute;nico
									con el descuento. &iexcl;Recuerda verificar la secci&oacute;n
									de Spam de tu buz&oacute;n de correos!</p>
								<a class="button style-10 btn-aceptar" style="float: right;">ACEPTAR</a>
							</div>
						</div>
					</div>
					<div class="close-popup"></div>
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jsp/descuento-detalle-mobile.js?v=<%System.currentTimeMillis();%>>"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>

<script id="descuento-detalle-cabecera-template"
	type="text/x-handlebars-template">
		<div class="row">
                    <div class="col-sm-6">
                        <div class="product-preview-box">
                            <img src="{{imagendetalle}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                            <div class="product-detail-box">
                                <h1 class="product-title">{{nombremarca}}</h1>
                                <div class="descuento"><span>{{textoSecundario}}</span> {{texto}}</div>
 								<p style="font-weight:bold;" class="text-red">{{descripcion}}</p>
								<div class="product-description type detail-info-entry" style="padding: 5px 0;border-bottom: 1px solid #e5e5e5;">
                                    <p><img src="${prop['config.url.resources.base.web']}img/icons/icon-phone.png" style="margin:0 10px;" alt="">Comun&iacute;cate con Bienestar Social - Mapfre</p>
                                </div>
                            </div>
                    </div>
                </div>

</script>
<script id="descuento-template" type="text/x-handlebars-template">
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 shop-grid-item descuento"> 
                                <div class="item">
                                    <div class="item-product">
                                        <div class="item-product-thumb">
                                            <a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}" class="product-thumb-link"><img class="img-responsive" src="{{imagen}}" alt="" /></a>
                                            <div class="product-extra-mask">
                                                <div class="product-extra-link">
                                                    <a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}" class="product-detail">
														<span class="lnr lnr-magnifier"></span>
														<span class="vermas">Ver más</span>
													</a>
                                                </div>
                                            </div>
                                            <div class="descr">
                                                <span class="antes">{{textoSecundario}}</span>
                                                <p>{{texto}}</p>
                                            </div>
                                        </div>
                                        <div class="item-product-info">
                                            <h3 class="title-product"><a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}">{{nombre}}</a></h3>
                                            <div class="info-price">
                                                <span>{{descripcionCorta}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
</script>

<script id="locales-template" type="text/x-handlebars-template">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="margin-bottom: 0px;min-height: 250px !important;">
			<a href="{{urlMapa}}" target="_blank">
				<img src="{{imagenGoogle}}" alt="" class="imagen-google">
			</a>
			<h4 class="title block-with-text-title" title="{{nombre}}">{{nombre}}</h4>
			<p class="block-with-text" title="{{direccion}}">{{direccion}}</p>
		</div>
</script>

<script>
	$(function() {
		$('.btn-aceptar').on('click', function() {
			$('.overlay-popup.visible').removeClass('active');
			setTimeout(function() {
				$('.overlay-popup.visible').removeClass('visible');
			}, 500);
		});
		DescuentoDet.init("${pageContext.request.contextPath}/",
				"${idDescuento}");
	});
</script>

</body></html>

