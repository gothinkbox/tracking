<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<script>
if($('#usuarioSesion').val()== ''){
	$(location).attr('href', '${pageContext.request.contextPath}/');
}
</script>
<!-- Banner -->
    <div class="banner_estatico desktop">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}img/bg/bg-beneficio.png);background-position: center center!important;">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

        <!-- Banner Responsive-->
        <div class="banner_estatico mobile">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/bg-beneficio.png);background-position: center center!important;">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

<div class="content-push beneficios catalogo">
	<div class="information-blocks">

		<div class="col-md-12 filtro-desc">
			<div class="entry">
				<div class="title-subpage">Categor�as</div>
				<div class="sort-pagi-bar top clearfix pull-right">
					<div class="product-filter">
						<select style="display:block !important;display: block;height: 34px;line-height: 34px;padding: 0 30px 0 20px;width: 100%;" 
						name="orderby" class="" id="select-ordenar">
							<option value="1">ORDENAR POR:</option>
							<c:forEach items="${listaOrden}" var="orden">
								<option value="${orden.id}">${orden.nombre}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="tabs-container type2">
				<div class="swiper-tabs tabs-switch">
				<div class="title">Categor�as</div>
				<div class="list list-categorias">
						<a id="7" class="block-title active">Beneficios flexibles</a>
						<a id="50" class="block-title">Descuentos</a>
					<div class="clear"></div>
				</div>
			</div>
			<div>
				<div class="tabs-entry">
					<div class="products-swiper">
						<div class="swiper-container" data-autoplay="0" data-loop="0"
							data-speed="500" data-center="0"
							data-slides-per-view="responsive" data-xs-slides="2"
							data-int-slides="2" data-sm-slides="3" data-md-slides="4"
							data-lg-slides="5" data-add-slides="5">
							<div class="swiper-wrapper col-md-12"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="buscar" value="${texto}" />


<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jsp/descuento.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>


<script id="banner-template" type="text/x-handlebars-template">
     <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/{{imagenBanner}});background-position: 45% 0;">
         <div class="internas-banner-content content-push">
             <div class="cell-view">
                 <img class="img-banner" src="${prop['config.url.resources.base.web']}imagenes/{{imagenTexto}}" height="80px" width="auto">
             </div>
             <img class="icon-banner" width="auto" height="100px" src="${prop['config.url.resources.base.web']}imagenes/{{iconoBanner}}">
         </div>
     </div>
</script>


<script id="descuento-template" type="text/x-handlebars-template">
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 shop-grid-item descuento"> 
                                <div class="item">
                                    <div class="item-product">
                                        <div class="item-product-thumb">
                                            <a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}" class="product-thumb-link"><img class="img-responsive" src="{{imagen}}" alt="" /></a>
                                            <div class="product-extra-mask">
                                                 <div class="product-extra-link">
                                                    <a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}" class="product-detail">
														<span class="lnr lnr-magnifier"></span>
														<span class="vermas">Ver m�s</span>
													</a>
                                                </div>
                                            </div>
                                            <div class="descr">
                                                <span class="antes">{{textoSecundario}}</span>
                                                <p>{{texto}}</p>
                                            </div>
                                        </div>
                                        <div class="item-product-info">
                                            <h3 class="title-product"><a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}">{{nombre}}</a></h3>
											<div class="info-price">
                                                <span title="{{descripcion}}">{{descripcionCorta}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
</script>

<script>
	$(function() {
		Descuento.init("${pageContext.request.contextPath}/" ,2);
	});
</script>