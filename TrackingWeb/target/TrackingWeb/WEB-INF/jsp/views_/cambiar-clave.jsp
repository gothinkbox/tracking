<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link rel="shortcut icon" href="${prop['config.url.resources.base.web']}imagenes/login/favicon-9.ico" />
    
    <link href="${prop['config.url.resources.base.web']}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300' rel='stylesheet' type='text/css'>
    <link href="${prop['config.url.resources.base.web']}css/style.css" rel="stylesheet" type="text/css" />
    <script src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/jquery-migrate.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/global.js"></script>

    <!-- custom scrollbar -->
    <script src="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"></script>
</head>


<!-- <body class="style-10 fondo"> -->
<!--     <div class="container-fluid"> -->
<!--         <div class="information-blocks"> -->
<!--             <div class="row"> -->
<!--                 <div class="personaje-6 col-lg-12 col-sm-12 col-xs-12 information-entry"> -->
<!--                     <div class="login-box login-fondo" align="center" style="border:0 !important; padding: 60px 40px 20px 40px;"> -->
<%--                         <img  src="${prop['config.url.resources.base.web']}imagenes/login/logo-login.png" style="width: 200px;height: auto;margin-bottom: 15px;"> --%>
<!--                         <form id="form-cambiarclave"> -->
<!--                              <div class="box-form"><input class="simple-field" id="inpt-clave" name="clave" type="password" placeholder="NUEVA CONTRASE&Ntilde;A"/></div> -->
<!--                              <div class="box-form"><input class="simple-field" id="inpt-claverepit" name="claverepit" type="password" placeholder="REPITA CONTRASE&Ntilde;A"/></div> -->
<!--                              <a id="btn-aceptar" class="button btn-1">ACEPTAR</a> -->
<%--                              <a href="${pageContext.request.contextPath}/" class="button btn-2">CANCELAR</a> --%>
<!--                             <div class="col-md-12"> -->
<!--                              <div class="centered text-center" id="mensajeMostrarAlerta"></div> -->
<!--                             </div> -->
<!--                         </form> -->
<!--                     </div> -->
<!--                 </div> -->
<!--             </div> -->
<!--         </div> -->
<!--     </div> -->
<!-- </body> -->

<body class="style-10 fondo" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/bg-login.png)!important; background-color:#004475;">
    <div class="login-box login-fondo reestablec" align="center">
        <img src="${prop['config.url.resources.base.web']}imagenes/logo-15.png" style="width: 200px;height: auto;margin-bottom: 15px;">
        <form id="form-cambiarclave">
             <div><input id="inpt-clave" name="clave" type="password" placeholder="Contraseņa"/></div>
             <div><input id="inpt-claverepit" name="claverepit" type="password" placeholder="Nueva Contraseņa"/></div>

            <div class="button-box">
                <a id="btn-aceptar-cambiar" class="button btn-1">ACEPTAR</a>
                <a href="${pageContext.request.contextPath}/" class="button btn-2">CANCELAR</a>
            </div>

       		<div class="centered text-center" id="mensajeMostrarAlerta"></div>

        </form>
    </div>
</body>


<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jsp/cambiar-clave.js"></script>

<script>
	$(function() {
		Recuperar.init("${pageContext.request.contextPath}/","${token}");
	});
</script>

</html>