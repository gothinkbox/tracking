<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<style>
form span.help-block {
    top: 45px !important;
}
</style>

<script>
if($('#usuarioSesion').val()== ''){
	$(location).attr('href', '${pageContext.request.contextPath}/');
}
</script>
  <!-- Banner -->
        <div class="banner_estatico desktop">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/bg-tyc.png);background-position: center!important;)">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

        <!-- Banner Responsive-->
        <div class="banner_estatico mobile">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/bg-tyc.png)">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>


<div class="information-blocks content-push sub-int">
	<div class="bread-crumb">
		<a href="${pageContext.request.contextPath}/">Home</a> <span
			class="lnr lnr-chevron-right"></span> <span>Contacto</span>
	</div>
	<div class="row">
		<div class="col-md-9 col-md-offset-2">
                    <div class="col-md-7 col-sm-6 col-xs-12 col-md-offset-2">
                        <div class="item-contact-box">
                            <a href="#"><span class="lnr lnr-envelope"></span></a>
                            <label>E-mail:</label>
                        </div>
                        <div class="mail-box">
                            <a href="mailto:${prop['email.mapfre']}">${prop['email.mapfre']}</a>
                        </div>
                    </div>
                </div>


		<div class="contact-form">
			<div class="title-subpage">Formulario de contacto</div>
			<form class="comment-form" id="form-contacto">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" placeholder="Nombres y apellidos *" id="inpt-nombres" name="nombres"
							onfocus="if (this.value==this.defaultValue) this.value = ''"
							onblur="if (this.value=='') this.value = this.defaultValue" />
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" placeholder="Tu E-mail *" name="correo" id="inpt-correo"/>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<textarea placeholder="Escribe aqu� tu mensaje *"
							id="inpt-mensaje" maxlength="350" name="mensaje"
							cols="30" rows="10"></textarea>
					</div>
				</div>
				<div>
					<input type="button" value="Enviar" class="button style-11" id="btn-enviar"
						style="width: 150px; display: inline-block; border-radius: 0;" />
					<input type="reset" value="Borrar" class="button style-10"
						style="width: 150px; display: inline-block; border-radius: 0;" />
				</div>
				<div class="col-sm-12">
					<br /> <br />
					<div class="clear-fix"></div>
					<div class="centered text-center" id="mensajeMostrarAlerta"></div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jsp/contacto.js"></script>

<script>
	$(function() {
		Contacto.init("${pageContext.request.contextPath}/");
	});
</script>






