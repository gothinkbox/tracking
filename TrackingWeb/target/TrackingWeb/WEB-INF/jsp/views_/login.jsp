<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300'
	rel='stylesheet' type='text/css'>
<link href="${prop['config.url.resources.base.web']}fonts/fonts.css"
	rel="stylesheet" type="text/css" charset="utf-8">
<script
	src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>

<link rel="shortcut icon"
	href="${prop['config.url.resources.base.web']}img/favicon.ico" />	
<title>Mapfre</title>
</head>

<body class="fondo">
	<div class="login-box" align="center">
		<img src="${prop['config.url.resources.base.web']}img/logored.png">
		<form id="form-login" action="<c:url value='j_spring_security_check' />" method='POST'>
			<select id="slct-tipodoc" data-live-search="true" placeholder="TIPO DE DOCUMENTO" name="password">
				<option value="0">TIPO DE DOCUMENTO</option>
				<c:forEach items="${listarTipoDocumento}" var="tipoDocumento">
					<option value="${tipoDocumento.idTipoDocumento}"
						data-min="${tipoDocumento.tamanioMinimo}"
						data-max="${tipoDocumento.tamanioMaximo}"
						data-alfan="${tipoDocumento.alfanumerico}">${tipoDocumento.nombreTipoDocumento}</option>
				</c:forEach>
			</select> 
			<input type="text" placeholder="N� DE DOCUMENTO" id="username1" name="username1"/>
			<input type="hidden"  name="username" id="username"/> 
			<a class="button btn-1" id="btn-login">Aceptar</a>
			</form>
	</div>
</body>

<script src="${prop['config.url.resources.base.web']}js/jsp/login.js"></script>

<script>
	Login.init("${pageContext.request.contextPath}/");
</script>

</html>





