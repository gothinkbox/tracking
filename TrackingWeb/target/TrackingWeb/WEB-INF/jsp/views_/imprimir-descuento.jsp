<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Impresion Descuento</title>
<link rel="shortcut icon" href="${prop['config.url.resources.base.web']}imagenes/favicon-9.ico" />
<style>
li{
	line-height: 20px;
    text-align: left;
    font-size: 15px;
    color: #767A7C;
    font-family: Lato, sans-serif;
    font-weight: 300;
    margin-top: 15px;
    margin-bottom: 15px;
}

</style>
</head>
<body onLoad="imprimir();">
${html}
<script type="text/javascript">
	function imprimir() {
	$('.title').html("");
	$('.title').text("");
	print();
}
</script>
    
</html> 