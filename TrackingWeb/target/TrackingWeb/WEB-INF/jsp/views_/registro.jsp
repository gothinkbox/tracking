<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />

<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,500,600,700"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/font-linearicons.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/bootstrap.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/bootstrap-theme.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.transitions.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.theme.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/theme.css"
	media="all" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/responsive.css"
	media="all" />

<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery-ui.min.js"></script>

<link
	href="${prop['config.url.resources.base.web']}css/components-rounded.css"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link
	href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300'
	rel='stylesheet' type='text/css'>
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />


<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-migrate.min.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"></script>
<script src="${prop['config.url.resources.base.web']}js/global.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/bootstrap.min.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/validate/additional-methods.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery-bootpag.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"></script>
<!-- <script type="text/javascript" -->
<%-- 	src="${prop['config.url.resources.base.web']}bigc/js/jquery-1.10.2.min.js"></script> --%>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery.fancybox.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery-ui.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/owl.carousel.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery.bxslider.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/theme.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery.validate.min.js"></script>


<link href="${prop['config.url.resources.base.web']}js/jquery-ui.min.js"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"
	rel="stylesheet">
<link href="${prop['config.url.resources.base.web']}js/global.js"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"
	rel="stylesheet">

<link
	href="${prop['config.url.resources.base.web']}js/bootstrap-select.js"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}js/bootstrap-select.min.js"
	rel="stylesheet">

<link href="${prop['config.url.resources.base.web']}fonts/fonts.css"
	rel="stylesheet" type="text/css" charset="utf-8" />
<link rel="shortcut icon"
	href="${prop['config.url.resources.base.web']}img/favicon.ico" />
<title><spring:message code="participante.titulo.banco.general" /></title>

<style type="text/css">
.error-registro{
color: red;
}
</style>


</head>


<body class="style-10 fondo"
	style="background-image:url(${prop['config.url.resources.base.web']}img/bg/bg-login.png)!important; background-color:#004475;">
	
<script>

if('${estado}' == '0'){
	$(location).attr('href', '${pageContext.request.contextPath}/');
}
</script>	
	
	<div class="container-fluid" style="margin-top: 5%;">
		<div class="information-blocks content-push">
			<div class="row">
				<div
					class="col-lg-4 col-lg-offset-4 col-sm-8 col-sm-offset-2 col-xs-12"
					align="center" style="border: 0 !important;">
					<div class="article-container style-1"
						style="text-align: center; margin-top: 0; margin-bottom: 15px;">
						<p style="font-size: 18px; line-height: 20px; color: #fff;">Por
							favor registre los siguientes campos:</p>
						<div class="forms-login">
							<form id="form-registro"
								action="${pageContext.request.contextPath}/login/registro">
								<input id="inp-nrodocumento" name="nrodocumento"
									class="simple-field caja-contrasenia input-registro"
									type="text" disabled="disabled" value="${username}" /> <input
									id="inp-correo" name="correo"
									class="form-group simple-field caja-contrasenia input-registro"
									type="email" placeholder="Correo electr&oacute;nico" /> <input
									id="inp-celular" 
									name="celular"
									class="form-group simple-field caja-contrasenia input-registro"
									type="text" placeholder="Celular" maxlength="9" />
								<div style="margin-top: 20px;">
									<input id="inp-check" type="checkbox"
										style="margin: 0px !important; float: left; margin-top: 0px !important;"
										class="form-group" name="terminos" />
									<p style="margin: -5px 0px 10px 15px; float: left;">
										He le&iacute;do y acepto los <a class="open-terminos"
											id="link-terminos"
											style="color: #5e5d5d; text-decoration: underline;">T&eacute;rminos
											y Condiciones</a>
									</p>
								</div>
								<div class="mensaje-error-terminos error-registro"></div>
								<button type="button" id="btn-registro" class="button btn-2">REG&Iacute;STRATE</button>
								<button type="button" id="btn-cancelar" class="button btn-1">CANCELAR</button>
								<div id="input-registro-msg"></div>
								<div class="centered text-center" id="mensajeMostrarAlerta"></div>
								<div class="button-box"
									style="min-height: 20px; margin-top: 5px;"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" id="urlTerminos" value="${urlTerminos}">
		
		<div id="terminos" class="overlay-popup">
			<div class="overflow">
				<div class="table-view">
					<div class="cell-view">
						<div class="close-layer"></div>
						<div class="popup-container small"
							style="margin-top: 20px; margin-bottom: 20px;">
							<div class="row">
								<div class="col-sm-12 col-xs-8 information-entry">
									<div class="htmlTerminos">
									</div>
								</div>
							</div>
							<div class="close-popup"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>



<div class="modal fade in" id="modal-terminos" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="display: none; padding-right: 17px;">
	<input type="hidden" id="tyc" value="${prop['config.url.resources.base.web']}terminos.html">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">T&eacute;rminos y
					condiciones</h4>
			</div>
			<div class="modal-body tyc-body">
				<p class="text-justify"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
			</div>
		</div>
	</div>
</div>



<!-- CORREO DE CONFIRMACION -->

<div class="modal fade" id="correo-participante" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" style="width: 510px;" role="document">
		<div class="modal-content">
			<div class="modal-header" style="border-bottom: 0px !important;">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Correo de
					Confirmación</h4>
			</div>
			<div class="modal-body"
				style="clear: both; border-bottom: 0px !important;">
				<div class="col-md-12 col-sm-12 spad">
					<p>Acabamos de enviarte un correo de Bienvenida.</p>
				</div>
			</div>
			<div class="modal-footer"
				style="border-bottom: 0px !important; border-top: 0px !important;">
			</div>
		</div>
	</div>
</div>


<div style="display: none !important;">
	<form id="form-login-register"
		action="<c:url value='/j_spring_security_check'/>" method='POST'>
		<input type="hidden" name="password" value="${tipodoc}">
		<input type="hidden" name="username" value="${username},${tipodoc}">
	</form>
</div>



<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jsp/registro.js?"></script>
<script
	src="${prop['config.url.resources.base.web']}js/validate/additional-methods.js"></script>

<script>
	$(function() {
		Registro.init("${pageContext.request.contextPath}/");
	});
</script>


</html>