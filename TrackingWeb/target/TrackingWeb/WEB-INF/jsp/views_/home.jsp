<%@ page contentType="text/html;charset=UTF-8" language="java"
	trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<div>

    <h4>Ingreso con exito</h4>

	<a href="?locale=en">English </a> | <a href="?locale=es">Spanish</a>

    ${test} 

	<h3>
		<spring:message code="user.title" />
	</h3>
	
	<div><spring:message code="user.name" /></div>
	<div><spring:message code="user.age" /></div>
	<div><spring:message code="user.submit" /></div>
	
	<a href="test">click </a> 
	




</div>