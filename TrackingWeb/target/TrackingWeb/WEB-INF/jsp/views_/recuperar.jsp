<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link rel="shortcut icon" href="${prop['config.url.resources.base.web']}imagenes/login/favicon-9.ico" />
    
    <link href="${prop['config.url.resources.base.web']}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
    <link href="${prop['config.url.resources.base.web']}css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300' rel='stylesheet' type='text/css'>
    <link href="${prop['config.url.resources.base.web']}css/style.css" rel="stylesheet" type="text/css" />
    <script src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/jquery-migrate.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/global.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/isotope.pkgd.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/jquery-ui.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"></script>

    <script src="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
    <script src="${prop['config.url.resources.base.web']}js/jquery-bootpag.js"></script>

</head>

<body class="style-10 fondo" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/bg-login.png)!important; background-color:#004475;">

    <div class="login-box login-fondo reestablec" align="center">
        <img src="${prop['config.url.resources.base.web']}imagenes/logo-15.png" style="width: 200px;height: auto;margin-bottom: 15px;">
        <div class="restablecer-div">
        <form id="form-recuperar">
             <p style="font-style: normal; text-align:center; padding-bottom:15px;">Ingresa tu correo electrónico para restablecer tu contraseña</p>
            <div class=""><input id="inpt-correo" name="correo" type="mail" placeholder="Correo electrónico"/></div>

            <div class="button-box">
                <button id="btn-aceptar" class="button btn-1">ACEPTAR</button>
                <a href="${pageContext.request.contextPath}/" class="button btn-2">CANCELAR</a>
            </div>
        </form>
        </div>
    </div>

</body>



<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jsp/recuperar.js"></script>
<script src="${prop['config.url.resources.base.web']}js/validate/additional-methods.js"></script>

<script>
	$(function() {
		Recuperar.init("${pageContext.request.contextPath}/");
	});
</script>

</html>