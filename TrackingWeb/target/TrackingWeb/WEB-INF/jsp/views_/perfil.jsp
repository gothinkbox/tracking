<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<script>
if($('#usuarioSesion').val()== ''){
	$(location).attr('href', '${pageContext.request.contextPath}/');
}
</script>


<!-- Banner -->
<div class="banner_estatico desktop">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}img/bg/bg-mi-perfil.png)">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>

        <!-- Banner Responsive-->
        <div class="banner_estatico mobile">
            <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/bg-mi-perfil.png)">
                <div class="internas-banner-content content-push">
                    <div class="cell-view">
                    </div>
                </div>
            </div>
        </div>


<div class="container">
	<div class="title-subpage" style="padding-top: 20px;">Datos
		personales</div>
	<div class="col-md-8 col-md-offset-2 sinpad">
		<form class="formCheckout" id="form-perfil" name="form_Checkout">
			<div class="col-sm-12 sinpad-resp">
				<div class="col-sm-6 col-xs-12 sinpad-resp form-group">
					<label for="" class="obl">Nombre</label> <input type="text"
						name="nombres" id="intp-nombres" disabled="disabled"
						placeholder="Nombres" value="${usuario.nombreParticipante}">
				</div>
				<div class="col-sm-6 col-xs-12 sinpad-resp form-group">
					<label for="" class="obl">Apellido paterno</label> <input
						type="text" name="apellidopaterno" id="inpt-apellido-paterno"
						disabled="disabled" placeholder="Apellido Paterno"
						value="${usuario.apellidoPaterno}">
				</div>
				<div class="col-sm-6 col-xs-12 sinpad-resp form-group">
					<label for="" class="obl">Apellido materno</label> <input
						type="text" name="apellidomaterno" id="inpt-apellido-materno"
						disabled="disabled" placeholder="Apellido Materno"
						value="${usuario.apellidoMaterno}">
				</div>
				<div class="col-sm-6 col-xs-12 sinpad-resp form-group">
					<label for="" class="obl">Email</label> <input type="text"
						name="correo" id="inpt-email" placeholder="E-mail"
						value="${usuario.emailParticipante}">
				</div>
				<div class="col-sm-6 col-xs-12 sinpad-resp form-group">
					<label for="">Tel�fono fijo</label> <input type="text"
						name=telefono id="inpt-telefono"
						placeholder="Tel�fono de contacto"
						value="${usuario.telefonoParticipante}">
				</div>
				<div class="col-sm-6 col-xs-12 sinpad-resp form-group">
					<label for="">Celular</label> <input type="text" name="celular"
						id="inpt-celular" placeholder="Tel�fono de contacto"
						value="${usuario.movilParticipante}">
				</div>
				<div class="col-sm-6 col-xs-12 sinpad-resp">
					<label for="" class="obl">Tipo de documento</label> <select
						id="h_tipo_doc" name="h_tipo_doc" disabled="disabled">
						<c:forEach items="${listaTiposDocumentos}" var="tipoDocumentos">
							<option value="${tipoDocumentos.idTipoDocumento}"
								${tipoDocumentos.idTipoDocumento == usuario.tipoDocumento ? 'selected="selected"' : ''}>${tipoDocumentos.nombreTipoDocumento}</option>
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-6 col-xs-12 sinpad-resp">
					<label for="" class="obl">N� de documento</label> <input
						type="text" name="h_nombre" id="h_nombre" placeholder="DNI"
						disabled="disabled" value="${usuario.nroDocumento}">
				</div>
			</div>
		</form>
		<div
			class="col-xs-8 col-xs-offset-2 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 sinpad-resp group-btn">
			<div class="col-xs-12 col-sm-6 col-md-6 information-entry">
				<a class="button style-11"
					href="${pageContext.request.contextPath}/"><spring:message
						code="perfil.btn.cancelar" /></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 information-entry">
				<a id="btn-actualizar-perfil" class="button style-10" href="#"><spring:message
						code="perfil.btn.guardar.cambios" /></a>
			</div>
		</div>
		<div
			class="col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5 col-sm-7 col-sm-offset-5 col-xs-12">
			<div class="centered text-center" id="mensajeMostrarAlerta"></div>
		</div>
	</div>
</div>

<!-- <div class="content-push mi_perfil"> -->
<!-- 	<div class="title-subpage"> -->
<%-- 		<spring:message code="perfil.datos.personales" /> --%>
<!-- 	</div> -->
<!-- 	<div class="col-md-8 col-md-offset-2"> -->
<%-- 		<form class="formCheckout" id="form-perfil" name="form_Checkout"> --%>
<!-- 			<div class="col-sm-12 m_bottom_15 m_bottom_15 padding_null" -->
<!-- 				style="padding: 0 !important"> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null form-group"> -->
<!-- 					<input type="text" name="nombres" id="intp-nombres" -->
<!-- 						disabled="disabled" placeholder="Nombres" -->
<%-- 						value="${usuario.nombreParticipante}"> --%>
<!-- 				</div> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null form-group"> -->
<!-- 					<input type="text" name="apellidopaterno" -->
<!-- 						id="inpt-apellido-paterno" disabled="disabled" -->
<%-- 						placeholder="Apellido Paterno" value="${usuario.apellidoPaterno}"> --%>
<!-- 				</div> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null form-group"> -->
<!-- 					<input type="text" name="apellidomaterno" -->
<!-- 						id="inpt-apellido-materno" disabled="disabled" -->
<%-- 						placeholder="Apellido Materno" value="${usuario.apellidoMaterno}"> --%>
<!-- 				</div> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null form-group"> -->
<!-- 					<input type="text" name="correo" id="inpt-email" -->
<%-- 						placeholder="E-mail" value="${usuario.emailParticipante}"> --%>
<!-- 				</div> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null"> -->
<!-- 					<select id="h_tipo_doc" name="h_tipo_doc" disabled="disabled"> -->
<%-- 						<c:forEach items="${listaTiposDocumentos}" var="tipoDocumentos"> --%>
<%-- 							<option value="${tipoDocumentos.idTipoDocumento}" --%>
<%-- 								${tipoDocumentos.idTipoDocumento == usuario.tipoDocumento ? 'selected="selected"' : ''}>${tipoDocumentos.nombreTipoDocumento}</option> --%>
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
<!-- 				</div> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null"> -->
<!-- 					<input type="text" name="h_nombre" id="h_nombre" placeholder="DNI" -->
<%-- 						disabled="disabled" value="${usuario.nroDocumento}"> --%>
<!-- 				</div> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null form-group" -->
<!-- 					style="float: inherit;"> -->
<!-- 					<input type="text" name="celular" id="inpt-celular" -->
<!-- 						placeholder="Tel�fono de contacto" -->
<%-- 						value="${usuario.movilParticipante}"> --%>
<!-- 				</div> -->
<!-- 			</div> -->

<!-- 			<div -->
<!-- 				class="col-xs-10 col-xs-offset-3 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5" -->
<!-- 				style="padding: 0; margin-top: 20px;"> -->
<!-- 				<div class="col-xs-8 col-sm-6 col-md-6"> -->
<!-- 					<div class="cart-summary-box"> -->
<!-- 						<a class="button style-11" -->
<%-- 							href="${pageContext.request.contextPath}/"><spring:message --%>
<%-- 								code="perfil.btn.cancelar" /></a> --%>
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="col-xs-8 col-sm-6 col-md-6"> -->
<!-- 					<div class="cart-summary-box"> -->
<%-- 						<a id="btn-actualizar-perfil" class="button style-10"><spring:message --%>
<%-- 								code="perfil.btn.guardar.cambios" /></a> --%>
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<div -->
<!-- 				class="col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5 col-sm-7 col-sm-offset-5 col-xs-12"> -->
<!-- 				<div class="centered text-center" id="mensajeMostrarAlerta"></div> -->
<!-- 			</div> -->
<%-- 		</form> --%>
<!-- 	</div> -->

<!-- 	<div class="title-subpage">Contrase&ntilde;a</div> -->
<!-- 	<div class="col-md-8 col-md-offset-2"> -->
<%-- 		<form class="formCheckout" id="form-clave" name="form_Checkout"> --%>
<!-- 			<div class="col-sm-12 m_bottom_15 m_bottom_15 padding_null" -->
<!-- 				style="padding: 0 !important"> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null	 form-group"> -->
<!-- 					<input type="password" name="contrasenia" id="inp-contrasenia" -->
<!-- 						placeholder="Nueva contrase�a"> -->
<!-- 				</div> -->
<!-- 				<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null form-group"> -->
<!-- 					<input type="password" name="contraseniarepit" -->
<!-- 						id="inp-contraseniaRepit" placeholder="Confirmar nueva contrase�a"> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<div -->
<!-- 				class="col-xs-10 col-xs-offset-3 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5" -->
<!-- 				style="padding: 0; margin-top: 20px;"> -->
<!-- 				<div class="col-xs-8 col-sm-6 col-md-6"> -->
<!-- 					<div class="cart-summary-box"> -->
<!-- 						<a class="button style-11" -->
<%-- 							href="${pageContext.request.contextPath}/">CANCELAR</a> --%>
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="col-xs-8 col-sm-6 col-md-6"> -->
<!-- 					<div class="cart-summary-box"> -->
<!-- 						<a id="btn-actualizar-clave" class="button style-10">GUARDAR -->
<!-- 							CAMBIOS</a> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<div -->
<!-- 				class="col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5 col-sm-7 col-sm-offset-5 col-xs-12"> -->
<!-- 				<div class="centered text-center" id="mensajeMostrarAlertaClave"></div> -->
<!-- 			</div> -->
<%-- 		</form> --%>
<!-- 	</div> -->
<!-- </div> -->

<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jsp/perfil.js"></script>

<script>
	$(function() {
		Perfil.init("${pageContext.request.contextPath}/");
	});
</script>



