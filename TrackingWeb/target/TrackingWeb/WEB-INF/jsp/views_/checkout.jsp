<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>



<!-- Banner -->
<div class="banner_estatico">
    <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/bg-4.png);background-position: 45% 0;">
        <div class="internas-banner-content content-push">
            <div class="cell-view">
            </div>
        </div>
    </div>
     <div class="breadcrumb-banner">
        <div class="content-push">
            <a href="${pageContext.request.contextPath}/">Home</a><span class="acti">Checkout</span>
        </div>
    </div>
</div><input type="hidden" id="page-checkout" value="checkout">

<div class="content-push box-checkout" id="div-checkout">
    <div class="mozaic-banners-wrapper type-2">
        <div class="banner-column col-sm-9">
            <div class="information-blocks">
                <div class="block-title size-3 accordeon-title active"><span>1</span><span class="title-check-1 block-title">Revisa tu compra  </span></div>
                <div class="accordeon accordeon-entry ae-1" style="display:block;">
                    <div class="information-blocks">
                        <div class="table-responsive">
                            <table class="cart-table pedido-dinamico">

                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-7 col-md-offset-5">
                                <div class="col-md-7 information-entry">
                                    <div class="cart-summary-box">
                                    <a class="button style-11 btn-actualizar">ACTUALIZAR</a>
                                    </div>
                                </div>
                                <div class="col-md-7 information-entry box-right">
                                    <div class="cart-summary-box">
                                        <div class="grand-total">Total:<span class="puntos-totales primer">0</span> Pts</div>
                                        <a class="button style-10 primer-aceptar">ACEPTAR</a>
                                        <div id="mensajePuntos"></div>
                                    </div>
                                    <div class="cart-summary-box">
                                    	<div id="mensajePuntos"></div>
                                	</div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
           </div>

                <div class="information-blocks">
                    <div class="block-title size-3 accordeon-title"><span>2</span><span class="title-check-2 block-title">Datos de env�o  </span></div>
                    <div class="range-wrapper accordeon-entry ae-2">

                        <div class="formCheckout" id="form_Checkout" name="form_Checkout">
                        	<form id="formParticipante">
                            <div class="col-sm-12 m_bottom_15 m_bottom_15 padding_null">
                                <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null" style="min-height: 80px;">
                                    <input type="text" name="nombres" id="inpt-nombre" disabled="disabled" placeholder="Nombre" value="${participante.nombreParticipante}">
                                </div>
                                <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null" style="min-height: 80px;">
                                    <input type="text" name="apellidopaterno" id="inpt-apellido-paterno" disabled="disabled" placeholder="Apellido paterno" value="${participante.apellidoPaterno}">
                                </div>
                                <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null" style="min-height: 80px;">
                                    <input type="text" name="apellidomaterno" id="inpt-apellido-materno" disabled="disabled" placeholder="Apellido materno" value="${participante.apellidoMaterno}">
                                </div>
                                <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null" style="min-height: 80px;">
                                    <input type="text" name="email" id="inpt-email" placeholder="E-mail" value="${participante.emailParticipante}">
                                </div>
        						<div class="col-sm-6 col-xs-12 m_bottom_15 padding_null" style="min-height: 80px;">
           							<select id="inpt-tipo-doc" name="tipodoc" disabled="disabled">
										<c:forEach items="${listaTiposDocumentos}" var="tipoDocumento">
											<option value="${tipoDocumento.idTipoDocumento}" ${tipoDocumento.idTipoDocumento == participante.tipoDocumento ? 'selected' : ''}
			     									data-min="${tipoDocumento.tamanioMinimo}"
			     									data-max="${tipoDocumento.tamanioMaximo}"
			     									data-alfan="${tipoDocumento.alfanumerico}"> 
			      									${tipoDocumento.nombreTipoDocumento}
											</option>
										</c:forEach>
          	 						</select>
          						</div>
					            <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null" style="min-height: 80px;">
					               <input type="text" name="numdoc" id="inpt-numdoc" disabled="disabled" placeholder="N�mero de documento" value="${participante.nroDocumento}">
<!-- 					               <div class="cart-summary-box"> -->
<!-- 					                   <a class="button style-10 show_hide mostrar-block">ACEPTAR</a> -->
<!-- 					               </div> -->
					            </div>
					            <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null" style="min-height: 80px;">
                                    <input type="text" name="telefono" id="inpt-telefono" placeholder="Tel�fono de contacto" value="${participante.telefonoParticipante}">
                                </div>
                                <div class="col-sm-12 col-xs-12 m_bottom_15 padding_null">
                                	<div class="cart-summary-box">
					                   <a class="button style-10 show_hide mostrar-block">ACEPTAR</a>
					               </div>
                                </div>                           
                            </div>
                            </form>
                            
                            
<!--                                 <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null"> -->
<%--                                     <input type="text" name="telefono" id="inpt-telefono" placeholder="Tel�fono de contacto" value="${participante.telefonoParticipante}"> --%>
<!--                                 </div> -->
                            
                            
                            
                            
                            <div class="slidingDiv ver-block" style="display:none">
                                <span class="title-check-21 block-title" style="clear: both;display: block;margin-bottom: 0;border: none;padding-left: 20px;padding-bottom:5px!important;"> 
                                Tu pedido se entregar� en tu Sede actual de trabajo:</span>
								<div class="col-sm-12 m_bottom_15 m_bottom_15 padding_null s_padding">
									<input type="radio" name="rdDistribuidor" value="${participante.distribuidor.idDistribuidor}" checked="checked">
									<label style="padding-top: 18px"> ${participante.distribuidor.nombreDistribuidor} </label>
         								<div class="col-sm-4 col-xs-12 m_bottom_15 padding_null" style="float:right;">
                                        <div class="cart-summary-box">
                                             <a class="button style-10 segundo-aceptar" style="margin-top:20px;">ACEPTAR</a>
                                        </div>                                          
                                     </div>
                                </div>   
                            </div>
                    	</div>
                	</div>

                <div class="information-blocks">
                    <div class="block-title size-3 accordeon-title"><span>3</span><span class="title-check-3 block-title">Tipo de env�o  </span></div>
                    <div class="range-wrapper accordeon-entry ae-3 t_envio">
                            <div class="col-sm-12 m_bottom_15 m_bottom_15 padding_null s_padding">
                                <div class="col-sm-4 col-xs-12 m_bottom_15 padding_null ">
                                    <form action="" class="solo_pts">
                                    	<input type="radio" name="option" checked="checked" style="margin-top:3px !important;">
                                    </form>
                                    <div class="env-norm" style="padding-top: 15px;">
                                        <b>Env&iacute;o normal</b>
                                        <span>Gratuito</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12 m_bottom_15 padding_null" style="float:right;">
                                    <div class="cart-summary-box">
                                        <a class="button style-10 tercer-aceptar">ACEPTAR</a>
                                    </div>                                          
                                </div>
                            </div>
                    </div>
                </div>
                <div class="information-blocks">
                    <div class="block-title size-3 accordeon-title"><span>4</span><span class="title-check-4 block-title">Forma de pago  </span></div>
                    <div class="range-wrapper accordeon-entry ae-4 t_envio formas_dpago">
                            <div class="col-sm-12 m_bottom_15 m_bottom_15 padding_null">
                                 <div class="p_border">
                                     <p>Selecciona la forma de pago</p>
                                     <div style="display:block;clear:both;overflow: hidden;">
                                         <div class="col-sm-3 col-xs-12 m_bottom_15 padding_null">
                                             <form action="" class="solo_pts" style="width: 100%;">
                                             	<input type="radio" name="option" checked="checked" style="margin-top:1px!important;">
                                             	<span style="padding-top: 13px;">S�lo puntos</span>
                                             </form>
                                         </div>
                                     </div>
                                 </div>
                                 <div class=""><p>Puntos disponibles: <b> ${usuario.puntosParticipante} Pts</b></p></div>
                                 <div class=""><p>Total de puntos que se usar�n:<b> <b class="puntos-totales">0</b> Pts</b></p></div>
                                 
                                 <div class="col-sm-12 col-xs-12 m_bottom_15 cart-summary-box" style="padding:0;">
                                     <div>
                                         <a class="button style-10" id="btn-confimar-compra">CONFIRMAR COMPRA</a>
                                     </div>                                          
                                 </div>
                                  <div class="col-lg-12" id="mensajeMostrarAlerta"></div>
                                </div> 
                            </div>
                            
                    </div>
                </div>
            </div>

            <div class="col-md-3 information-entry">
               <div class="cart-summary-box">
                   <div class="sub-total">Total de tu compra</div>
                   <div class="grand-total">Total: <span class="puntos-totales"> 0</span> Pts</div>
               </div>
            </div>
    <div class="hidden-sm hidden-xs" style="height: 30px;"></div>
        

   </div>
</div>


<div class="content-push box-checkout gracias col-md-8 col-md-offset-2" style="display:none;" id="div-checkout-gracias">
     <div class="text-center">
         <h2>�Gracias por tu compra!</h2>
         <p>En breve recibir�s un email de confirmaci�n por tu compra:</p>
     </div>
     <div class="mozaic-banners-wrapper type-2">
             <div class="banner-column">
                 <div class="information-blocks">
                      <div class="accordeon">
                          <div class="information-blocks">
                              <div class="table-responsive">
                                  <table class="cart-table pedido-dinamico-gracias">

                                  </table>
                              </div>
                              <div class="row">
                                  <div class="col-md-4 information-entry">
                                      <div class="cart-summary-box">
                                          <div class="grand-total">Total: <span class="puntos-totales"> 0</span> Pts</div>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-12 information-entry text-center">
                                          <a class="button style-10" href="${pageContext.request.contextPath}/productos">Seguir comprando</a>
                                          <p class="df">�Alguna pregunta sobre su compra?</p>
                                          <span>Cont�ctanos a ${prop['email.info']}</span>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
             </div>
      </div>
</div>



<script id="pedido-detalle" type="text/x-handlebars-template" >
  <tr>
	  <td>
		  <div class="traditional-cart-entry" data-id="{{producto.idProducto}}">
			  <a class="image"><img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt=""></a>
			  <div class="content">
				  <div class="cell-view">
					  <a href="${pageContext.request.contextPath}/productos/detalle/{{producto.idProducto}}" class="title">{{producto.nombreProducto}}</a>
					  <div class="inline-description"><span>{{producto.marca.nombreMarca}}</span></div>
				  </div>
			  </div>
		  </div>
	  </td>
	  <td>{{producto.puntosProducto}} Pts.</td>
	  <td>
		  <div class="quantity-selector detail-info-entry">
			  <div class="entry number-minus">&nbsp;</div>
			  <div class="entry number" id="number-{{producto.idProducto}}">{{cantidad}}</div>
			  <div class="entry number-plus">&nbsp;</div>
		  </div>
	  </td>
	  <td><div class="subtotal">{{puntosTotal}} Pts.</div></td>
	  <td><a class="remove-button" id="{{producto.idProducto}}" ><i class="fa fa-times"></i></a></td>
  </tr>
</script>



<script id="pedido-detalle-gracias" type="text/x-handlebars-template" >
<tr>
    <td>
        <div class="traditional-cart-entry">
            <a href="#" class="image"><img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt=""></a>
            <div class="content">
                <div class="cell-view">
                    <a class="title">{{producto.nombreProducto}}</a>
                    <div class="inline-description"><span>{{producto.marca.nombreMarca}}</span></div>
                </div>
            </div>
        </div>
    </td>
 	<td>{{producto.puntosProducto}} Pts.</td>
    <td>
        <div class="quantity-selector detail-info-entry">
            <div class="entry number">{{cantidad}}</div>
        </div>
    </td>
    <td><div class="subtotal">{{puntosTotal}} Pts.</div></td>
    
</tr>
</script>


<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jsp/checkout.js"></script>           
<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>


	<script>
		 $(function() {
			 
	          $(".slidingDiv").hide();
	          $(".show_hide").show();
	          $('.show_hide').click(function(){
	          $(".slidingDiv").slideToggle();
	          });
	          $(".mostrar-block").click(function () {
	        	  $(this).hide();
		           	$(".ver-block").show();
		      });
	          $(".accept-2").click(function () {
		           $(".ver-block").addClass("mostrar");

		         
		           
				});
			 Checkout.init("${pageContext.request.contextPath}/");
		  });
	</script>

