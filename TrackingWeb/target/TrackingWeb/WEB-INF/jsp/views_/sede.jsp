<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!-- Banner -->

<div class="banner_estatico mis_ventas">
    <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/bg-perfil.png);">
        <div class="internas-banner-content content-push">
            <div class="cell-view">
                <h1 class="title">Mi Perfil</h1>
                <ul class="submenu_banner"> 
                <li><a href="${pageContext.request.contextPath}/perfil">Datos personales</a></li>
                <li style="display: inherit !important;"><a class="active">Mi sede de trabajo</a></li>
                <li><a href="${pageContext.request.contextPath}/perfil/historial">Historial de compra</a></li>
                </ul>
            </div>
            <img class="icon-banner" width="auto" height="100px" src="${prop['config.url.resources.base.web']}imagenes/icon_perfil.png">
        </div>
    </div>
</div>

<div class="content-push mi_perfil">
   
<div class="title-subpage">Mi sede de trabajo</div>
    <div class="col-md-8 col-md-offset-2">
        <form class="formCheckout" id="form_Checkout" name="form_Checkout">
            <div class="col-sm-12 m_bottom_15 m_bottom_15 padding_null">

                <div class="col-sm-6 col-xs-12 m_bottom_15 padding_null">
                <label style="margin-bottom:0;">Sede actual de trabajo</label>
                    <select class="type_doc" data-live-search="true" id="cbo-distribuidores">
                   		<c:forEach items="${listaSedes}" var="listaSedes">
							<option value="${listaSedes.idDistribuidor}" ${listaSedes.idDistribuidor==usuario.distribuidor.idDistribuidor? 'selected="selected"' : ''}>${listaSedes.nombreDistribuidor}</option>
						</c:forEach>
                	</select>
                </div>
            </div>
            <div class="col-xs-9 col-xs-offset-3 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5" style="padding:0;margin-top:20px;">
                <div class="col-xs-8 col-sm-6 col-md-6">
                        <div class="cart-summary-box">
                            <a class="button style-11" href="${pageContext.request.contextPath}/">CANCELAR</a>
                        </div>
                </div>
                <div class="col-xs-8 col-sm-6 col-md-6">
                    <div class="cart-summary-box">
                        <a class="button style-10" id="btn-actualizar-sede">GUARDAR CAMBIOS</a>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5 col-sm-7 col-sm-offset-5 col-xs-12">
                 <div class="centered text-center" id="mensajeMostrarAlertaSede"></div>
            </div>
        </form>
    </div>                   
</div>

<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jsp/sede.js"></script>

<script>
	$(function() {
		Sede.init("${pageContext.request.contextPath}/");
	});
</script>
