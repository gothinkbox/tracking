<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300'
	rel='stylesheet' type='text/css'>
<link href="${prop['config.url.resources.base.web']}fonts/fonts.css"
	rel="stylesheet" type="text/css" charset="utf-8">
<script
	src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
<script src="${prop['config.url.resources.base.web']}js/global.js"></script>

<title>Mapfre</title>
<style type="text/css">
#terminos-error{
	float: right;
}
</style>
</head>

<body class="fondo">
	
	<div class="login-box" align="center">
        <form id="form-actualizacion">
            <img src="${prop['config.url.resources.base.web']}img/logo.png" width="210" height="auto">
            <p style="margin-bottom:20px;">Club MAPFRE te da la bienvenida a su nueva red de beneficios.<br>
            Conf&iacute;rmanos tu Email y Celular para activarte y comienza a  disfrutar de tus descuentos.</p> 
            <input type="email" name="email" id="email" placeholder="E-MAIL(*)" style="margin-top: 15px !important; margin-bottom: 0!important;" />
            <input type="text" name="celular" id="celular" placeholder="CELULAR(*)" style="color: rgb(0, 0, 0); margin-top: 15px !important; margin-bottom: 0!important;" />
            <div class="col-md-12" style="overflow:hidden;">
                <input type="checkbox"  name="terminos" style="display: inline; margin-top: 15px !important;margin-bottom: 0px !important;">
                <a class="open-login" style="display: inline; margin-top: 15px !important;margin-bottom: 15px !important; display: inline-block; color:#d81e05; margin-bottom:15px;text-decoration:underline">Acepto los T&eacute;rminos y condiciones.</a>
            </div>
            <div class="button-box" style="clear:both;">
                <a href="#" id="btn-actualizar" class="button btn-1">login</a>
                <a href="#" id="btn-cancelar" class="button btn-2">Cancelar</a>
            </div>
            <div class="message-error" style="clear:both;"></div>
        </form>
    </div>
	
	
	<div id="login-popup" class="overlay-popup">
	<input type="hidden" id="tyc" value="${prop['config.url.resources.base.web']}terminos.html">
		<div class="overflow">
			<div class="table-view">
				<div class="cell-view">
					<div class="close-layer"></div>
					<div class="popup-container">
						<p style=" text-align: justify;">Toda la informaci�n que nos proporcione es necesaria para poder ofrecerle los beneficios de la plataforma y sin su consentimiento para su tratamiento no se le podr� ofrecer tales beneficios.</p>
						<p style=" text-align: justify;">De conformidad con la Ley de Protecci�n de Datos Personales - Ley 29733 y su Reglamento, en virtud de este documento y siendo que legalmente en el entorno digital, se considera expresa 
							la manifestaci�n consistente en "hacer clic", a trav�s de la aceptaci�n de estos "T�rminos y Condiciones" de la p�gina web de Beneficios Mapfre (beneficios.mapfre.com.pe), otorgo mi total y 
							absoluto consentimiento a Mapfre para que, en sus calidades de titulares de bancos de datos personales de administraci�n privada, realicen el tratamiento de mis datos personales 
							(los "Datos Personales") a los que tenga acceso o sean informados directamente por mi persona a trav�s de la p�gina web de Beneficios Mapfre (beneficios.mapfre.com.pe), llamadas telef�nicas, 
							por medio digital, entre otros, especialmente mis nombres y apellidos completos, tipo y n�mero de documento de identidad, correo electr�nico, tel�fono fijo, tel�fono celular, fecha de nacimiento, 
							domicilio, entre otros datos personales afines.</p>
						<p style=" text-align: justify;">Asimismo, he tomado conocimiento de que la finalidad del tratamiento de los Datos Personales es recibir de manera id�nea informaci�n respecto a promociones, encuestas y campa�as comerciales 
						de los productos y servicios de Mapfre, as� como para la solicitud y/o reclamo que pueda tener frente a Mapfre. En consiguiente, manifiesto tener conocimiento y otorgo mi consentimiento para 
						que los Datos Personales sean utilizados: (i) con fines estad�sticos, hist�ricos; (ii) para que Mapfre pueda llevar un registro y banco de datos de los reclamos y/o solicitudes que he realizado; 
						y, (iii) para que Mapfre pueda atender el reclamo y/o solicitud que tengo frente a dicha empresa, (iv) acepto recibir informaci�n, publicidad y novedades, campa�as comerciales y/o encuestas de 
						Mapfre o de otras marcas y productos comercializadas por Mapfre.</p>
						<p style=" text-align: justify;">Finalmente, declaro tener pleno conocimiento de la existencia del banco de datos personales de titularidad de Mapfre en que se almacenar�n los Datos Personales, cuya oficina se encuentra 
						ubicada en Av. 28 de Julio 873, distrito de Miraflores, provincia y departamento de Lima. De la misma manera, estoy informado de: (i) las consecuencias legales de otorgar mi consentimiento 
						para el tratamiento de los Datos Personales y la posibilidad de revocar el mismo para lo cual podr� contactarme al correo electr�nico de Mapfre: clubmapfre@mapfre.com.pe; (ii) del tiempo 
						durante el cual los Datos Personales ser�n conservados, el cual no exceder� de diez (10) a�os; (iii) la Pol�tica Corporativa de Tratamiento De Datos Personales en Mapfre, la cual se encuentra 
						publicada en la presente p�gina web, la misma que declaro haber le�do en su integridad y respecto de la cual me encuentro conforme; y, (iv) de la posibilidad de ejercer los derechos que la Ley 
						de Protecci�n de Datos Personales y su Reglamento me concede y los medios y procedimientos previstos para ello en Mapfre."</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
</body>



<script src="${prop['config.url.resources.base.web']}js/jsp/login.js"></script>

<script>
	Login.init("${pageContext.request.contextPath}/");
</script>

</html>





