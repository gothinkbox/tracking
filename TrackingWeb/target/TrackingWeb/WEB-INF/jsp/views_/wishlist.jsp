<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>




<!-- Banner -->
<div class="banner_estatico">
    <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/bg-4.png);background-position: 45% 0;">
        <div class="internas-banner-content content-push">
            <div class="cell-view">
            </div>
        </div>
    </div>
     <div class="breadcrumb-banner">
        <div class="content-push">
            <a href="${pageContext.request.contextPath}/">Home</a><span class="acti">Wishlist</span>
        </div>
    </div>
</div><input type="hidden" id="page-carrito" value="carrito">


<div class="content-push box-checkout wishlist">

    <div class="mozaic-banners-wrapper type-2">

        <div class="banner-column col-sm-12">
            <div class="information-blocks">
                 <div class="information-blocks">
                     <div class="table-responsive">
                         <table class="cart-table wishlist-dinamico">

                         </table>
                     </div>
                 <div class="col-md-3" style="float:left; margin-top:20px; margin-bottom:24px;">
                	<a class="button style-11" href="${pageContext.request.contextPath}/productos">SEGUIR COMPRANDO</a>
                 </div>
<!--                  <div class="col-md-3" style="float:right; margin-top:20px; margin-bottom:20px;"> -->
<!--                  	<a class="button style-11" href="#" style="float:right;">ACTUALIZAR</a> -->
<!--                  </div> -->

<!--             	 <div class="col-md-6 col-md-offset-6"> -->
<!--                      <div class="col-md-6 information-entry box-right"> -->
<!--                          <div class="cart-summary-box"> -->
<!-- 							<div class="grand-total">Total: <span id="puntos-totales">0</span> Pts</div> -->
<!--                          </div> -->
<!--                      </div> -->
<!--                  </div> -->
             </div>
        </div>
    </div>
</div>

    <div class="hidden-sm hidden-xs" style="height: 30px;"></div>
    
</div>

<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/jsp/wishlist.js"></script>           
<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>



<script id="wishlist-list" type="text/x-handlebars-template" >
  <tr>
	  <td style="padding-left: 0px!important; text-align: center; width: 150px;">
		  <div class="traditional-cart-entry" 
				data-id="{{producto.idProducto}}" data-nombre="{{producto.nombreProducto}}" 
				data-marca="{{producto.marca.nombreMarca}}"
				data-puntos="{{producto.puntosProducto}}" data-imagenuno="{{producto.imagenUno}}">
			  <a class="image"><img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt=""></a>
			  <div class="content">
				  <div class="cell-view">
					  <a href="${pageContext.request.contextPath}/productos/detalle/{{producto.idProducto}}" class="title">{{producto.nombreProducto}}</a>
					  <div class="inline-description"><span>{{producto.marca.nombreMarca}}</span></div>
				  </div>
			  </div>
		  </div>
	  </td>
	  <td style="padding-left: 0px!important; text-align: center; width: 120px;">{{producto.puntosProducto}} Pts.</td>
	  <td style="padding-left: 10%!important; text-align: center; width: 120px; padding-top: 3% !important;">
		  <div class="quantity-selector detail-info-entry" style="display:none;">
			  <div class="entry number-minus">&nbsp;</div>
			  <div class="entry number" id="number-{{producto.idProducto}}">1</div>
			  <div class="entry number-plus">&nbsp;</div>
		  </div>
		<div class="content">
			<div class="cell-view">
				<a class="button style-10 open-addcar-wish"
				data-id="{{producto.idProducto}}" data-nombre="{{producto.nombreProducto}}" 
				data-marca="{{producto.marca.nombreMarca}}" data-puntos="{{producto.puntosProducto}}" data-imagenuno="{{producto.imagenUno}}">
				Agregar al carrito</a>
			</div>
		</div>
	  </td>
	  <td style="padding-left: 0px!important; padding-top: 0px !important;text-align: center; width: 70px;"><a class="remove-button" id="{{producto.idProducto}}" ><i class="fa fa-times"></i></a></td>
  </tr>
</script>




<script>
	 $(function() {
		 Wishlist.init("${pageContext.request.contextPath}/");
	  });
</script>
