<%@ page contentType="text/html;charset=UTF-8" language="java"
	trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,500,600,700"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/font-linearicons.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/bootstrap.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/bootstrap-theme.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.transitions.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.theme.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/theme.css"
	media="all" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/responsive.css"
	media="all" />

<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-ui.min.js"></script>

<link
	href="${prop['config.url.resources.base.web']}css/components-rounded.css"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link
	href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300'
	rel='stylesheet' type='text/css'>
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />


<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-migrate.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"></script>
<script type="text/javascript" src="${prop['config.url.resources.base.web']}js/global.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/validate/additional-methods.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-bootpag.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery.fancybox.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery-ui.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/owl.carousel.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery.bxslider.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/theme.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery.validate.min.js"></script>

<link href="${prop['config.url.resources.base.web']}fonts/fonts.css"
	rel="stylesheet" type="text/css" charset="utf-8" />
<link rel="shortcut icon"
	href="${prop['config.url.resources.base.web']}img/favicon.ico" />
</head>
<body>
	<div class="content-push beneficios catalogo">
		<div class="information-blocks">

			<div class="col-md-12 filtro-desc">
				<div class="entry">
					<div class="title-subpage">Categorías</div>
					<div class="sort-pagi-bar top clearfix pull-right">
						<div class="product-filter">
							<select
								style="display: block !important; display: block; height: 34px; line-height: 34px; padding: 0 30px 0 20px; width: 100%;"
								name="orderby" class="" id="select-ordenar">
								<option value="1">ORDENAR POR:</option>
								<c:forEach items="${listaOrden}" var="orden">
									<option value="${orden.id}">${orden.nombre}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="tabs-container type2">
				<div class="swiper-tabs tabs-switch">
					<div class="title">Categorías</div>
					<div class="list list-categorias">
						<c:forEach items="${listaCategorias}" var="categoria"
							varStatus="loop">
							<a id="${categoria.id}" class="block-title">${categoria.nombre}</a>
						</c:forEach>
						<div class="clear"></div>
					</div>
				</div>
				<div>
					<div class="tabs-entry">
						<div class="products-swiper">
							<div class="swiper-container" data-autoplay="0" data-loop="0"
								data-speed="500" data-center="0"
								data-slides-per-view="responsive" data-xs-slides="2"
								data-int-slides="2" data-sm-slides="3" data-md-slides="4"
								data-lg-slides="5" data-add-slides="5">
								<div class="swiper-wrapper col-md-12"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="buscar" value="${texto}" />


	<script type="text/javascript"
		src="${prop['config.url.resources.base.web']}js/jsp/descuento_mobile.js"></script>
	<script type="text/javascript"
		src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>


	<script id="banner-template" type="text/x-handlebars-template">
     <div class="banner_internas" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/{{imagenBanner}});background-position: 45% 0;">
         <div class="internas-banner-content content-push">
             <div class="cell-view">
                 <img class="img-banner" src="${prop['config.url.resources.base.web']}imagenes/{{imagenTexto}}" height="80px" width="auto">
             </div>
             <img class="icon-banner" width="auto" height="100px" src="${prop['config.url.resources.base.web']}imagenes/{{iconoBanner}}">
         </div>
     </div>
</script>


	<script id="descuento-template" type="text/x-handlebars-template">
<!--
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 shop-grid-item descuento"> 
<div class="item">
	<div class="item-product">
		<div class="item-product-thumb">
			<a href="${pageContext.request.contextPath}/mobile/detalle/{{iddescuento}}" class="product-thumb-link"><img class="img-responsive" src="{{imagen}}" alt="" /></a>
			<div class="product-extra-mask">
				<div class="product-extra-link">
					<a href="${pageContext.request.contextPath}/mobile/detalle/{{iddescuento}}" class="product-detail"><span class="lnr lnr-magnifier"></span></a>
				</div>
			</div>
		</div>
		<div class="item-product-info">
			<h3 class="title-product"><a href="${pageContext.request.contextPath}/mobile/detalle/{{iddescuento}}">{{nombre}}</a></h3>
			<div class="info-price" style="font-size: 20px;color: #d81e05;font-weight: 600; text-align: center; padding-top: 0px;">
				<div class="current before" style="padding-left: 0px; font-size: 15px !important;display: inline-block;" id={{tipodescuento}}>{{textoSecundario}}</div>
				<div class="current after" style="padding-left: 0px;" id={{tipodescuento}}>{{texto}}</div>
			</div>
		</div>
	</div>
</div>
</div> -->

<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 shop-grid-item descuento"> 
                                <div class="item">
                                    <div class="item-product">
                                        <div class="item-product-thumb">
                                            <a href="${pageContext.request.contextPath}/mobile/detalle/{{iddescuento}}" class="product-thumb-link"><img class="img-responsive" src="{{imagen}}" alt="" /></a>
                                            <div class="product-extra-mask">
                                                <div class="product-extra-link">
                                                    <a href="${pageContext.request.contextPath}/mobile/detalle/{{iddescuento}}" class="product-detail">
														<span class="lnr lnr-magnifier"></span>
														<span class="vermas">Ver más</span>
													</a>
                                                </div>
                                            </div>
                                            <div class="descr">
                                                <span class="antes">{{textoSecundario}}</span>
                                                <p>{{texto}}</p>
                                            </div>
                                        </div>
                                        <div class="item-product-info">
                                            <h3 class="title-product"><a href="${pageContext.request.contextPath}/mobile/detalle/{{iddescuento}}">{{nombre}}</a></h3>
                                            <div class="info-price">
                                                <span title="{{descripcion}}">{{descripcionCorta}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
</script>

	<script>
		$(function() {
			Descuento.init("${pageContext.request.contextPath}/", 1);
		});
	</script>

</body>