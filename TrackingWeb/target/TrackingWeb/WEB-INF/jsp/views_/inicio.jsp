<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!-- Banner-->
<div class="navigation-banner-swiper desktop">
            <div class="swiper-container" data-autoplay="5000" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                <div class="swiper-wrapper banner-dinamico">
                </div>
                <div class="clear"></div>
                <div class="pagination"></div>
            </div>
        </div>

<!-- Banner Responsive-->
        <div class="navigation-banner-swiper mobile">
            <div class="swiper-container" data-autoplay="5000" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                <div class="swiper-wrapper banner-dinamico-responsive">
                </div>
                <div class="clear"></div>
                <div class="pagination"></div>
            </div>
        </div>


<div class="container">
	<div class="relate-product">
		<!-- Nav tabs -->
		<div class="nav-tabs-icon">
			<ul class="nav nav-tabs list-categorias" role="tablist">
				<li role="presentation" class="active"><a href="#destacados"
					aria-controls="furniter" role="tab" data-toggle="tab">Destacados</a></li>
				<li role="presentation"><a href="#masvistos"
					aria-controls="decor" role="tab" data-toggle="tab">M�s vistos</a></li>
				<li role="presentation"><a 
					aria-controls="lighting" role="tab" data-toggle="tab" id="tabCategorias">Ver Categor&iacute;as</a></li>
			</ul>
		</div>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="destacados">
				<div class="col-md-12 destacadosContent" style="padding:0"></div>
			</div>
			<div role="tabpanel" class="tab-pane" id="masvistos">
				<div class="product-slider arrow-style">
					<div class="row">
						<div class="wrap-item vistosContent"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="${pageContext.request.contextPath}/descuentos" class="button style-10" style="margin: 0 auto;display: block; width: 240px; margin-bottom: 15px;">Descubre m�s beneficios 
	<img src="${prop['config.url.resources.base.web']}img/icons/icon-right.svg"  alt="" style="width: 22px;vertical-align: middle;margin-left: 5px;">
	</a>
</div>
<input type="hidden" id="bannerInferior" value="${prop['config.url.resources.base.web']}bannerInferior.html">
<div class="full-width">
	<div class="container bannerInferior">
	
	</div>
</div>


<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jsp/inicio.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>
	
<script id="banner" type="text/x-handlebars-template">
		<div class="swiper-slide active" data-val="0"> 
                        <div class="navigation-banner-wrapper light-text align-3" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/{{imagenBanner}});background-position: center center;">
                            <div class="navigation-banner-content content-push">
                                <div class="cell-view banner-info-text">
                                    <h2>{{textoUno}}</h2>
                                    <a href="{{urlBoton}}"  target="_blank" class="button style-10">{{textoBoton}}</a>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div> 
</script>

<script id="banner-responsive" type="text/x-handlebars-template">
		<div class="swiper-slide active" data-val="0"> 
                        <div class="navigation-banner-wrapper light-text align-3" style="background-image:url(${prop['config.url.resources.base.web']}imagenes/responsive/{{imagenBanner}});background-position: center center;">
                            <div class="navigation-banner-content content-push">
                                <div class="cell-view banner-info-text">
                                    <h2>{{textoUno}}</h2>
                                    <a href="{{urlBoton}}" target="_blank" class="button style-10">{{textoBoton}}</a>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div> 
</script>


<script id="producto-banner" type="text/x-handlebars-template">
							<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 shop-grid-item descuento"> 
                                <div class="item">
                                    <div class="item-product">
                                        <div class="item-product-thumb">
                                            <a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}" class="product-thumb-link"><img class="img-responsive" src="{{imagen}}" alt="" /></a>
                                            <div class="product-extra-mask">
                                                <div class="product-extra-link">
                                                    <a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}" class="product-detail">
														<span class="lnr lnr-magnifier"></span>
														<span class="vermas">Ver m�s</span>
													</a>
                                                </div>
                                            </div>
                                            <div class="descr">
                                                <span class="antes">{{textoSecundario}}</span>
                                                <p>{{texto}}</p>
                                            </div>
                                        </div>
                                        <div class="item-product-info">
                                            <h3 class="title-product"><a href="${pageContext.request.contextPath}/descuentos/detalle/{{iddescuento}}">{{nombre}}</a></h3>
                                            <div class="info-price">
                                                <span title="{{descripcion}}">{{descripcionCorta}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
</script>

<script id="producto-banner-loked" type="text/x-handlebars-template">
    <div class="col-lg-3 col-md-3 col-sm-6 shop-grid-item pr-locked"> 
		<div class="paddings-container">
			<div class="product-slide-entry shift-image">
				<div class="product-image">
					<img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt="" />
					<img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt="" />
					<a class="top-line-a right open-product" id="{{producto.idProducto}}"><i class="fa fa-expand"></i> <span>Vista Previa</span></a>
					<div class="hover-white"></div>
					<a class="btn-verdetalle" href="${pageContext.request.contextPath}/productos/detalle/{{producto.idProducto}}">
					Te faltan {{producto.puntosFaltantes}} Puntos para este producto</a>
				</div>
				<a class="title" href="${pageContext.request.contextPath}/productos/detalle/{{producto.idProducto}}">{{producto.nombreProducto}}</a>
				<div class="price">
					<div class="current">{{producto.puntosProducto}} Pts.</div>
				</div>
			</div>
		</div>
	</div>  
</script>


<script>
	$(function() {
		$.ajax({ url: $('#bannerInferior').val(), success: function(data) { $('.bannerInferior').prepend(data); } });
	});

	$(function() {
		$('body').addClass('home');
		Inicio.init("${pageContext.request.contextPath}/",
				"${usuario.puntosParticipante}");
	});
</script>



