<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>

<header class="type-4">
	<div class="header-top">
		<div class="menu-button responsive-menu-toggle-class">
			<i class="fa fa-reorder"></i>
		</div>
		<div class="clear"></div>
	</div>

	<input type="hidden" id="usuarioSesion"
		value="${usuario.idParticipante}">

	<div class="close-header-layer"></div>
	<div class="navigation content-push">
		<div class="navigation-header responsive-menu-toggle-class">
			<div class="close-menu"></div>
			<div class="logo-responsive">
				<a href="index.html"><img
					src="${prop['config.url.resources.base.web']}img/logo.png" alt=""></a>
			</div>
		</div>
		<div class="nav-overflow">
			<div class="content-push">
				<div class="col-sm-12 col-md-4 col-lg-4" style="font-weight: 300">
					<nav>
						<ul>
							<li class="column-1"><a
								href="${pageContext.request.contextPath}" class="active">Inicio</a>
							</li>
							<li class="column-1"><a
									href="${pageContext.request.contextPath}/descuentos">Categorķas</a>
							</li>
							<c:if test="${usuario.catalogo.tipoSegmento == 1}">
							    <li class="column-1"><a
									href="${pageContext.request.contextPath}/beneficios">Beneficios internos</a>
								</li>
							</c:if>
							<li class="column-1"><a
								href="${pageContext.request.contextPath}/preguntas-frecuentes">Preguntas
									Frecuentes</a></li>
							<li class="column-1"><a
								href="${pageContext.request.contextPath}/perfil">Perfil</a></li>
							<li class="column-1"><a
								href="${pageContext.request.contextPath}/contacto">Contacto</a></li>
							<li class="column-1"><a
								href="${pageContext.request.contextPath}/terminos-condiciones">T&eacute;rminos y Condiciones</a>
							</li>
							<li class="column-1"><a class="logout open-login"
								href="/j_spring_security_logout">Logout</a></li>
						</ul>
					</nav>
				</div>

				<div class="col-sm-12 col-md-4 col-lg-4">
					<a href="${pageContext.request.contextPath}"
						class="fixed-header-visible additional-header-logo top"
						style="display: block; float: left;"><img
						src="${prop['config.url.resources.base.web']}img/logo.png" alt=""></a>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4 info-top-right">
					<div class="info-header info-header2">
						<ul class="info-total">
							<li><a class="header-functionality-entry open-cart-popup"
								href="#"><img
									src="${prop['config.url.resources.base.web']}img/icons/icon-car.png"
									style="height: 20px; width: auto;"><span class="count">2</span></a>
							</li>
							<li><a class="header-functionality-entry"
								href="whislist.html"><span class="lnr lnr-heart"></span></a></li>
							<li class="info-search"><a href="#"
								class="btn icon-search open-search-popup">BUSCAR <span
									class="lnr lnr-magnifier"></span></a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
</header>

