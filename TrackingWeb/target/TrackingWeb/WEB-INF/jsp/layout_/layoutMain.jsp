<%@ page contentType="text/html;charset=UTF-8" language="java"
	trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />

<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,500,600,700"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/font-linearicons.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/bootstrap.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/bootstrap-theme.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.transitions.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/owl.theme.css" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/theme.css"
	media="all" />
<link rel="stylesheet" type="text/css"
	href="${prop['config.url.resources.base.web']}bigc/css/responsive.css"
	media="all" />

<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="${prop['config.url.resources.base.web']}js/jquery-2.1.3.min.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery-ui.min.js"></script>

<link
	href="${prop['config.url.resources.base.web']}css/components-rounded.css"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${prop['config.url.resources.base.web']}css/idangerous.swiper.css"
	rel="stylesheet" type="text/css" />
<link
	href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600italic,600,400italic,300'
	rel='stylesheet' type='text/css'>
<link
	href="${prop['config.url.resources.base.web']}css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link href="${prop['config.url.resources.base.web']}css/style.css"
	rel="stylesheet" type="text/css" />


<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jquery-migrate.min.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"></script>
<script src="${prop['config.url.resources.base.web']}js/global.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/bootstrap.min.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/validate/jquery.validation.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/validate/additional-methods.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery-bootpag.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"></script>
<!-- <script type="text/javascript" -->
<%-- 	src="${prop['config.url.resources.base.web']}bigc/js/jquery-1.10.2.min.js"></script> --%>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery.fancybox.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery-ui.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/owl.carousel.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/jquery.bxslider.js"></script>
<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}bigc/js/theme.js"></script>
<script
	src="${prop['config.url.resources.base.web']}js/jquery.validate.min.js"></script>


<link href="${prop['config.url.resources.base.web']}js/jquery-ui.min.js"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}js/idangerous.swiper.min.js"
	rel="stylesheet">
<link href="${prop['config.url.resources.base.web']}js/global.js"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}js/jquery.mousewheel.js"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}js/jquery.jscrollpane.min.js"
	rel="stylesheet">

<link
	href="${prop['config.url.resources.base.web']}js/bootstrap-select.js"
	rel="stylesheet">
<link
	href="${prop['config.url.resources.base.web']}js/bootstrap-select.min.js"
	rel="stylesheet">

<link href="${prop['config.url.resources.base.web']}fonts/fonts.css"
	rel="stylesheet" type="text/css" charset="utf-8" />
<link rel="shortcut icon"
	href="${prop['config.url.resources.base.web']}img/favicon.ico" />

<title>Mapfre</title>

</head>
<body class="style-15 home">

	<div id="content-block">
		<div class="pre-header col-sm-12 col-md-5 col-lg-5"
			style="font-weight: 300">
			<div class="content-push">
				<div class="pull-right">
					<ul>
						<li class="column-1"><a
							href="${pageContext.request.contextPath}/preguntas-frecuentes"
							class="active">Preguntas Frecuentes</a></li>
						<li class="column-1"><a
							href="${pageContext.request.contextPath}/perfil">Perfil</a></li>
						<li class="column-1"><a
							href="${pageContext.request.contextPath}/contacto">Contacto</a></li>
						<li class="column-1"><a
							href="${pageContext.request.contextPath}/terminos-condiciones">T&eacute;rminos y condiciones</a>
						</li>
						<li class="column-1"><a class="logout"
							href="${pageContext.request.contextPath}/j_spring_security_logout">Logout</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="header-middle text-center resp">
			<a href="${pageContext.request.contextPath}/"><img
				class="logo-responsive"
				src="${prop['config.url.resources.base.web']}img/logo.png" alt=""></a>
		</div>
		<div class="header-wrapper style-15">
			<tiles:insertAttribute name="header" />
			<div class="clear"></div>
		</div>
		<tiles:insertAttribute name="content" />
		<footer id="footer">
			<tiles:insertAttribute name="footer" />
		</footer>
	</div>


</body>
</html>
