<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<div class="content-push">
	<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 contacto">
		<p>
			Lima: <br> <span>01-2133333</span>
		</p>

		<p>
			Provincia: <br> <span>0801-1-1133</span>
		</p>

		<p>Atenci&oacute;n al cliente 24 horas</p>
		<p class="web">
			<a href="https://www.mapfre.com.pe" target="a_blank">mapfre.com.pe</a>
		</p>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 social">
		<ul>
			<li><a href="https://www.facebook.com/mapfre.pe"
				target="a_blank"><img
					src="${prop['config.url.resources.base.web']}img/fb.png" alt="">/Mapfre.Pe</a></li>
			<li><a href="https://twitter.com/mapfre_pe" target="a_blank"><img
					src="${prop['config.url.resources.base.web']}img/tw.png" alt="">/Mapfre_Pe</a></li>
			<li><a href="https://www.youtube.com/user/MAPFREPeruOficial"
				target="a_blank"><img
					src="${prop['config.url.resources.base.web']}img/yt.png" alt="">/MapfrePeruOficial</a></li>
		</ul>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 logo">
		<img class="logo-footer"
			src="${prop['config.url.resources.base.web']}img/logo-footer.png"
			alt="">
	</div>
</div>
<div class="footer-bottom"></div>
<div class="footer-bottom"></div>
<!-- Modales - Popups-->

<div class="search-box popup">
	<div class="search-form">
		<form>
			<div class="wrap-toggle-search">
				<input type="text" placeholder="Descuento, categoria, ..."
					id="textoDescuento">
			</div>
			<input class="btn-link-default" type="button" value="Buscar"
				id="buscarTexto" />
		</form>
	</div>
</div>


<div class="cart-box popup">
	<div class="popup-container cartbox-dinamico"></div>
</div>


<div id="login-popup" class="overlay-popup">
	<div class="overflow">
		<div class="table-view">
			<div class="cell-view">
				<div class="close-layer"></div>
				<div class="popup-container">
					<img class="icon-login"
						src="${prop['config.url.resources.base.web']}img/logo-login.png"
						width="100" height="100">
					<div class="login-msj-1">
						<h2>Bienvenido a MAPFRE</h2>
						<form id="form-login"
							action="<c:url value='/j_spring_security_check'/>" method='POST'>
							<p>Ingresa a tu cuenta para continuar</p>
							<label for="">Tipo de documento</label> <select name="password"
								id="select_tipo">
								<option value="1">DNI</option>
								<option value="2">Pasaporte</option>
							</select> <label for="">N� de documento</label> <input type="text"
								id="inpt-username" name="" placeholder="Nro de documento" /> <input
								type="hidden" id="inpt-usuario" name="username"
								placeholder="Nro de documento" /> <input type="hidden"
								id="inpt-password" name="password" value="123456" /> <a
								class="button style-10 cerrar-modal" id="btn-login">Ingresar</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="add-car" class="overlay-popup">
	<div class="overflow">
		<div class="table-view">
			<div class="cell-view">
				<div class="close-layer"></div>
				<div class="popup-container add-cart-dinamico"></div>
			</div>
		</div>
	</div>
</div>

<div id="add-wishlist" class="overlay-popup">
	<div class="overflow">
		<div class="table-view">
			<div class="cell-view">
				<div class="close-layer"></div>
				<div class="popup-container add-wish-dinamico"></div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/handlebars.js"></script>


<div id="product-popup" class="overlay-popup">
	<div class="overflow">
		<div class="table-view">
			<div class="cell-view">
				<div class="close-layer"></div>
				<div class="popup-container popup-dinamico"></div>
			</div>
		</div>
	</div>
</div>
<script id="popup-producto" type="text/x-handlebars-template">
	<div class="row">
		<div class="col-sm-5 information-entry">
			<div class="product-preview-box">
				<img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}">
			</div>
		</div>
		<div class="col-sm-7 information-entry">
			<div class="product-detail-box">
				<h1 class="product-title">{{producto.nombreProducto}}</h1>
				<h3 class="product-subtitle">{{producto.marca.nombreMarca}}</h3>
				<div class="product-description detail-info-entry">{{producto.descripcionProducto}}</div>
				<div class="price detail-info-entry">
					<div class="prev">{{producto.puntosProducto}} puntos</div>
				</div>
				<div class="quantity-selector detail-info-entry">
					<div class="detail-info-entry-title">Cantidad</div>
					<div class="entry number-minus">&nbsp;</div>
					<div class="entry number">1</div>
					<div class="entry number-plus">&nbsp;</div>
				</div>

				<div class="detail-info-entry">
					<a class="button style-10 agregar-carrito" 	 data-id="{{producto.idProducto}}">
					Agregar al carrito </a><br>
					<div class="clear"></div>
					<p class="product-description no-puntos" style="clear:both;"></p>
				</div>
			</div>
		</div>
	</div>
	<div class="close-popup"></div>
</script>


<script id="cart-box" type="text/x-handlebars-template">
	<div class="cart-entry">
		<a class="image"><img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt="" /></a>
		<div class="content">
			<a class="title block-with-text" href="${pageContext.request.contextPath}/productos/detalle/{{producto.idProducto}}" title="{{producto.nombreProducto}}">{{producto.nombreProducto}}</a>
			<div class="quantity">cantidad : {{cantidad}}</div>
			<div class="price">{{producto.puntosProducto}} Pts.</div>
		</div>
		<div class="button-x"><i class="fa fa-close" id="{{producto.idProducto}}"></i></div>
	</div>
</script>

<script id="product-add-car" type="text/x-handlebars-template">
	<div class="row">
	<div class="col-sm-3 col-xs-4 img-addcar">
		<div><img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt=""></div>
	</div>
	<div class="col-sm-9 col-xs-8 information-entry">
		<div class="product-detail-box">
			<div class="alert_2">{{mensaje}}</div>
			<h1 class="product-title">{{producto.nombreProducto}}</h1>
			<h3 class="product-subtitle">{{producto.marca.nombreMarca}}</h3>
			<div class="info-puntos">{{producto.puntosProducto}} Pts</div>
			<div class="detail-info-entry col-md-offset-4">
				<a class="button style-11 seguir-comprando">Seguir comprando</a>
				<a href="${pageContext.request.contextPath}/carrito" class="button style-10">Ir a carrito</a>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	</div>
	<div class="close-popup"></div>
</script>

<script id="product-add-wish" type="text/x-handlebars-template">
	<div class="row">
		<div class="col-sm-3 col-xs-4 img-addcar">
			<div><img src="${prop['config.url.resources.base.web']}imagenes/producto/{{producto.imagenUno}}" alt=""></div>
		</div>
		<div class="col-sm-9 col-xs-8 information-entry">
			<div class="product-detail-box">
				<div class="alert_3">{{mensaje}}</div>
				<h1 class="product-title">{{producto.nombreProducto}}</h1>
				<h3 class="product-subtitle">{{producto.marca.nombreMarca}}</h3>
				<div class="info-puntos">{{producto.puntosProducto}} Pts</div>
				<div class="detail-info-entry col-md-offset-4">
					<a class="button style-11 seguir-comprando">Seguir comprando</a>
					<a href="${pageContext.request.contextPath}/wishlist" class="button style-10">Ir a wishlist</a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="close-popup"></div>
</script>




<script type="text/javascript"
	src="${prop['config.url.resources.base.web']}js/jsp/footer.js"></script>

<script>
	$(function() {
		Footer.init("${pageContext.request.contextPath}/",
				"${usuario.puntosParticipante}");
	});
</script>



