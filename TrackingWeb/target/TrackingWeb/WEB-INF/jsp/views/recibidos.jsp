<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<section role="main" class="content-body card-margin">
	<header class="page-header">
		<h2>Pedidos Recibidos</h2>

		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li><a href="index.html"> <i class="fa fa-home"></i>
				</a></li>
				<li><span>Pedidos</span></li>
				<li><span>Pedidos Recibidos</span></li>
			</ol>

			<a class="sidebar-right-toggle" data-open="sidebar-right"><i
				class="fa fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
	<div class="row">
		<div class="col">
			<section class="card">
				<header class="card-header">
					<div class="card-actions">
						<a href="#" class="card-action card-action-toggle"
							data-card-toggle></a> <a href="#"
							class="card-action card-action-dismiss" data-card-dismiss></a>
					</div>

					<h2 class="card-title">Pedidos en espera de asignaci&oacute;n</h2>
				</header>
				<div class="card-body">
					<form class="form-horizontal form-bordered" id="form-filtro">
						<div class="form-group row"
							style="padding-bottom: 0px !important; margin-bottom: 0px; border-bottom: 0px;">
							<label class="col-lg-2 control-label text-lg-right pt-2">Estado</label>
							<div class="col-lg-2">
								<select class="form-control mb-3" name="estado" id="estado">
									<option value="1">RECIBIDO</option>
									<option value="2">REPROGRAMADO</option>
								</select>
							</div>
							<label class="col-lg-2 control-label text-lg-right pt-2">Fecha
								desde</label>
							<div class="col-lg-2">
								<div class="input-group">
									<span class="input-group-addon"> <i
										class="fa fa-calendar"></i>
									</span> <input type="text" data-plugin-datepicker class="form-control"
										name="fecini" id="fecini">
								</div>
							</div>
							<label class="col-lg-2 control-label text-lg-right pt-2">Fecha
								hasta</label>
							<div class="col-lg-2">
								<div class="input-group">
									<span class="input-group-addon"> <i
										class="fa fa-calendar"></i>
									</span> <input type="text" data-plugin-datepicker class="form-control"
										name="fecfin" id="fecfin">
								</div>
							</div>
						</div>

						<div class="form-group row"
							style="padding-bottom: 0px !important; margin-bottom: 0px; border-bottom: 0px;">
							<label class="col-lg-2 control-label text-lg-right pt-2">Departamento</label>
							<div class="col-lg-2">
								<select class="form-control mb-3" name="departamento"
									id="departamento">
								</select>
							</div>
							<label class="col-lg-2 control-label text-lg-right pt-2">Provincia</label>
							<div class="col-lg-2">
								<select class="form-control mb-3" name="provincia"
									id="provincia">
								</select>
							</div>
							<label class="col-lg-2 control-label text-lg-right pt-2">Pago</label>
							<div class="col-lg-2">
								<select class="form-control mb-3" name="tipoPago" id="tipoPago">
								</select>
							</div>
						</div>

						<div class="form-group row"
							style="border-bottom: 0px; padding-bottom: 5px !important; margin-bottom: 5px;">
							<label class="col-lg-2 control-label text-lg-right pt-2">Segmento</label>
							<div class="col-lg-2">
								<select class="form-control mb-3" name="segmento" id="segmento">
								</select>
							</div>
							<label class="col-lg-2 control-label text-lg-right pt-2">Buscar
								por</label>
							<div class="col-lg-2">
								<select class="form-control mb-3" name="tipoBusqueda">
									<option>DNI</option>
									<option>NOMBRE</option>
									<option>ID PEDIDO</option>
								</select>
							</div>
							<div class="col-lg-4">
								<div class="input-group input-search">
									<input type="text" class="form-control" name="valor" id="valor"
										placeholder="Buscar...">
								</div>
							</div>
						</div>


						<div class="form-group row"
							style="padding-bottom: 0px !important; margin-bottom: 0px; border-bottom: 0px;">
							<div class="col-lg-12 text-lg-right">
<!-- 								<button class="btn btn-primary btnDescargar" value="Buscar">Descargar</button> -->
								<button class="btn btnBuscar" id="btnBuscar" value="Buscar"
									style="background-color: #00a1f2; border-color: #00aaff #00aaff #0088cc; color: #ffffff;">Buscar</button>
							</div>
						</div>
					</form>
				</div>
			</section>


		</div>
	</div>


	<div class="card-body">
		<table class="table table-bordered table-striped" id="datatable_recibidos">
			<thead>
				<tr>
					<th>#</th>
					<th>ID</th>
					<th>DEP</th>
					<th>PROV</th>
					<th>DIST</th>
					<th>DIRECCION</th>
					<th>ESTADO</th>
					<th>OBS</th>
					<th>ACCION</th>
				</tr>
			</thead>
			 <tbody>
            <tr>
                <td></td>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>$320,800</td>
            </tr>
			</tbody>
		</table>

	</div>



	<!-- end: page -->
</section>

<script id="row-recibidos" type="text/x-handlebars-template">
	<tr>
                <td></td>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>$320,800</td>
            </tr>
</script>

<script
	src="${prop['config.url.resources.base.web']}js/jsp/recibidos.js?<%=new Date()%>"></script>

<script>
	Recibidos.init("${pageContext.request.contextPath}/");
</script>
