package com.tracking.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tracking.bean.Pedido;
import com.tracking.bean.Pedido2;
import com.tracking.service.PedidoService;

@RestController
@RequestMapping("pedido")
public class PedidoServiceController {
	
private static Logger LOGGER = LoggerFactory.getLogger(PedidoServiceController.class);
	
	@Autowired
	PedidoService pedidoService;
	
	@RequestMapping(value = "test", method = RequestMethod.GET)
	public String test() {
		return "SUCCESSFUL";
	}
	
	@RequestMapping(value="registrar" , method = RequestMethod.POST)
	public Map<String, Object> registrarPedido(@RequestBody Pedido2 pedido){
		LOGGER.info("registrarPedido: "+pedido.toJsonString());
		Integer resultado;
		Map<String, Object> mapa = new HashMap<String, Object>();
		try {
			resultado = pedidoService.registrar(pedido);
			
			mapa.put("estado", 1);
			mapa.put("desEstado", "EXITO");
			mapa.put("idPedido", pedido.getIdPedido());
			mapa.put("seqPedido", resultado);
			
		} catch (Exception e) {
			e.printStackTrace();
			resultado = -1;
			mapa.put("estado", resultado);
			mapa.put("desEstado", "ERROR EN REGISTRO");
			mapa.put("idPedido", "");
			mapa.put("seqPedido", 0);
		}
		
		
		return mapa;
	}
	
	@RequestMapping(value="{idPedido}" , method = RequestMethod.GET)
	public Pedido pedido(@PathVariable("idPedido") Integer seqPedido){
		LOGGER.info("consultarPedido: "+ seqPedido);
		try {
			return pedidoService.consultar(seqPedido);
		} catch (Exception e) {
			return null;
		}
	}
	
}