package com.tracking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tracking.bean.Filtro;
import com.tracking.bean.Segmento;
import com.tracking.bean.TipoPago;
import com.tracking.bean.Ubigeo;
import com.tracking.service.CommonService;

@RestController
@RequestMapping("common")
public class CommonServiceController {
	
	@Autowired
	CommonService commonService;
	
	@RequestMapping(value="listarUbigeos", method = RequestMethod.POST)
	public List<Ubigeo> listarUbigeos(@RequestBody Filtro filtro) {
		return commonService.listarUbigeos(filtro);
	}
	
	@RequestMapping(value="listarSegmento", method = RequestMethod.GET)
	public List<Segmento> listarSegmento() {
		return commonService.listarSegmento();
	}
	
	@RequestMapping(value="listarTipoPago", method = RequestMethod.GET)
	public List<TipoPago> listarTipoPago() {
		return commonService.listarTipoPago();
	}
	
}