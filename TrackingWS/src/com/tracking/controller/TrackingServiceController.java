package com.tracking.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tracking.bean.Filtro;
import com.tracking.bean.Pedido;
import com.tracking.service.PedidoService;

@RestController
@RequestMapping("tracking")
public class TrackingServiceController {
	
private static Logger LOGGER = LoggerFactory.getLogger(TrackingServiceController.class);
	
	@Autowired
	PedidoService pedidoService;
	
	@RequestMapping(value="recibidos" , method = RequestMethod.POST)
	public List<Pedido> recibidos(@RequestBody Filtro filtro){
		return pedidoService.listarPedidos(filtro);
	}
	
	@RequestMapping(value="{idPedido}" , method = RequestMethod.GET)
	public Pedido pedido(@PathVariable("idPedido") Integer seqPedido){
		LOGGER.info("consultarPedido: "+ seqPedido);
		try {
			return pedidoService.consultar(seqPedido);
		} catch (Exception e) {
			return null;
		}
	}
	
	@RequestMapping(value="preparar" , method = RequestMethod.POST)
	public Integer recibidos(@RequestBody Pedido pedido){
		return pedidoService.preparar(pedido);
	}
	
	
}