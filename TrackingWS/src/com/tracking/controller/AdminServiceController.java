package com.tracking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tracking.bean.Usuario;
import com.tracking.service.UsuarioService;

@RestController
@RequestMapping("admin")
public class AdminServiceController {
	
	@Autowired
	UsuarioService usuarioService; 
	
	@RequestMapping(value="obtenerUsuario", method = RequestMethod.POST)
	public Usuario obtenerUsuario(@RequestBody Usuario usuario){
		return usuarioService.obtenerUsuario(usuario);
	}
	
}