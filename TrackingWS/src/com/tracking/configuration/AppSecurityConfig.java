package com.tracking.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.tracking.security.AuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@PropertySources({
	@PropertySource(value = "file:/${propertiesHomeTracking}/configIPTracking.properties", ignoreResourceNotFound = false) })
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {
 
    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;
    
    @Autowired
	private Environment environment;
 
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    		auth.inMemoryAuthentication()
        .withUser(environment.getRequiredProperty("basic-user")).password(environment.getRequiredProperty("basic-password")).roles("USER");
    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
          .antMatchers("/admin/**").permitAll()
          .antMatchers("/common/**").permitAll()
          .antMatchers("/tracking/**").permitAll()
          .anyRequest().authenticated()
          .and()
          .httpBasic()
          .authenticationEntryPoint(authenticationEntryPoint);
    }
}