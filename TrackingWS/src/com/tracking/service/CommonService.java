package com.tracking.service;

import java.util.List;

import com.tracking.bean.Filtro;
import com.tracking.bean.Segmento;
import com.tracking.bean.TipoPago;
import com.tracking.bean.Ubigeo;

public interface CommonService {

	List<Ubigeo> listarUbigeos(Filtro filtro);

	List<Segmento> listarSegmento();

	List<TipoPago> listarTipoPago();
}