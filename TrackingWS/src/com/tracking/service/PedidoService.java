package com.tracking.service;

import java.util.List;

import com.tracking.bean.Filtro;
import com.tracking.bean.Pedido;
import com.tracking.bean.Pedido2;

public interface PedidoService {

	Integer registrar(Pedido2 pedido);
	Pedido consultar(Integer seqPedido);
	List<Pedido> listarPedidos(Filtro filtro);
	Integer preparar(Pedido pedido);
}