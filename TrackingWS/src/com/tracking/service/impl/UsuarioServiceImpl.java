package com.tracking.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tracking.bean.Usuario;
import com.tracking.dao.UsuarioDao;
import com.tracking.service.UsuarioService;

@Service(value = "UsuarioService")
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioDao usuarioDao;
	
	@Override
	public Usuario obtenerUsuario(Usuario usuario) {
		return usuarioDao.obtenerUsuario(usuario);
	}

//	@Transactional
//	@Override
//	public Integer actualizarClaveParticipante(Participante participante) {
//		return participanteDao.actualizarClaveParticipante(participante);
//	}

}
