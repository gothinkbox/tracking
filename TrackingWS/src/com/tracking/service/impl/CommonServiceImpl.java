package com.tracking.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tracking.bean.Filtro;
import com.tracking.bean.Segmento;
import com.tracking.bean.TipoPago;
import com.tracking.bean.Ubigeo;
import com.tracking.dao.CommonDao;
import com.tracking.service.CommonService;

@Service(value="CommonService")
public class CommonServiceImpl implements CommonService {
	
	@Autowired
	CommonDao commonDao;
	
	@Override
	@Transactional
	public List<Ubigeo> listarUbigeos(Filtro filtro) {
		return commonDao.listarUbigeos(filtro);
	}
	
	@Override
	@Transactional
	public List<Segmento> listarSegmento() {
		return commonDao.listarSegmento();
	}
	
	@Override
	@Transactional
	public List<TipoPago> listarTipoPago() {
		return commonDao.listarTipoPago();
	}

}