package com.tracking.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tracking.bean.Filtro;
import com.tracking.bean.Pedido;
import com.tracking.bean.Pedido2;
import com.tracking.dao.PedidoDao;
import com.tracking.service.PedidoService;

@Service(value="PedidoService")
public class PedidoServiceImpl implements PedidoService {
	
	@Autowired
	PedidoDao pedidoDao;
	
	@Transactional
	@Override
	public Integer registrar(Pedido2 pedido) {
		return pedidoDao.registrar(pedido);
	}

	@Override
	public Pedido consultar(Integer seqPedido) {
		return pedidoDao.consultar(seqPedido);
	}

	@Override
	@Transactional
	public List<Pedido> listarPedidos(Filtro filtro) {
		return pedidoDao.listarPedido(filtro);
	}

	@Override
	@Transactional
	public Integer preparar(Pedido pedido) {
		return pedidoDao.preparar(pedido);
	}
}