package com.tracking.util;
import java.io.Serializable;

public class BResult implements Serializable {

	private static final long serialVersionUID = -7654134389421516532L;

		private Integer codigo;
		private String mensaje;
		private Boolean estado;
		private Integer successResult;
		private Integer errorResult;
		private Integer updateResult;
		private Integer ignoreResult;
		private Object result;
		
		
		public BResult(){
			
		}
		
		public BResult(Boolean estado, Integer  codigo){
			this.estado = estado;
			this.codigo = codigo;
		}
		
		public Integer getCodigo() {
			return codigo;
		}

		public void setCodigo(Integer codigo) {
			this.codigo = codigo;
		}

		public String getMensaje() {
			return mensaje;
		}

		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}

		public Boolean getEstado() {
			return estado;
		}

		public void setEstado(Boolean estado) {
			this.estado = estado;
		}

		public Integer getSuccessResult() {
			return successResult;
		}

		public void setSuccessResult(Integer successResult) {
			this.successResult = successResult;
		}

		public Integer getErrorResult() {
			return errorResult;
		}

		public void setErrorResult(Integer errorResult) {
			this.errorResult = errorResult;
		}
		
		public Integer getUpdateResult() {
			return updateResult;
		}

		public void setUpdateResult(Integer updateResult) {
			this.updateResult = updateResult;
		}

		public Integer getIgnoreResult() {
			return ignoreResult;
		}

		public void setIgnoreResult(Integer ignoreResult) {
			this.ignoreResult = ignoreResult;
		}

		public Object getResult() {
			return result;
		}

		public void setResult(Object result) {
			this.result = result;
		}

	}
