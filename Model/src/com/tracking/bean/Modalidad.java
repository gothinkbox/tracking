package com.tracking.bean;

import com.tracking.util.BeanBase;

public class Modalidad extends BeanBase {
	private static final long serialVersionUID = -3223526575095331647L;

	Integer idModalidad;
	String nombreModalidad;
	public Integer getIdModalidad() {
		return idModalidad;
	}
	public void setIdModalidad(Integer idModalidad) {
		this.idModalidad = idModalidad;
	}
	public String getNombreModalidad() {
		return nombreModalidad;
	}
	public void setNombreModalidad(String nombreModalidad) {
		this.nombreModalidad = nombreModalidad;
	}
	
	
}
