package com.tracking.bean;

import java.io.Serializable;

import com.tracking.util.BeanBase;

public class TipoPago extends BeanBase{
	private static final long serialVersionUID = 8889211382003687404L;
	private Integer tipoPago;
	private String nombreTipoPago;
	public Integer getIdTipoPago() {
		return tipoPago;
	}
	public void setIdTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getNombreTipoPago() {
		return nombreTipoPago;
	}
	public void setNombreTipoPago(String nombreTipoPago) {
		this.nombreTipoPago = nombreTipoPago;
	}
}

