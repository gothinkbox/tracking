package com.tracking.bean;


import java.io.Serializable;

public class Auditoria implements Serializable {

	private static final long serialVersionUID = -5129288387374452850L;

	private String usuarioCreacion;
	private String usuarioActualizacion;
	private String fechaCreacion;
	private String fechaActualizacion;

	public Auditoria() {
		super();
	}

	public Auditoria(String usuarioCreacion, String usuarioActualizacion,
			String fechaCreacion, String fechaActualizacion) {
		super();
		this.usuarioCreacion = usuarioCreacion;
		this.usuarioActualizacion = usuarioActualizacion;
		this.fechaCreacion = fechaCreacion;
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(String fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

}
