package com.tracking.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.tracking.util.BeanBase;

public class Pedido2 extends BeanBase {
	private static final long serialVersionUID = -3223526575095331647L;

	private Integer seqPedido;
	private String idPedido;
	private String departamento;
	private String provincia;
	private String distrito;
	private String tipoVia;
	private String nombreVia;
	private String numeroVia;
	private String referencia;
	private String pisoDepartamento;
	private String rangoHorario;
	private String nombreContacto;
	private String telefonoContacto;
	private String tipoPago;
	private String fecha;
	private String hora;
	private String estado;
	private String desEstado;
	private String nombreUsuario;
	private String telefonoUsuario;
	private String codigoComercio;
	private String nombreComercio;
	private String rucComercio;
	private String monto;
	private String cantidadItems;
	
	//update 10-04-2018
	private String idVisanet;
	private String tipocontraentrega;
	private String montoEfectivo;
	private String dni;
	private String tipoDocumentoPago;
	private String emailRepresentante;
	
	
	
	public String getIdVisanet() {
		return idVisanet;
	}
	public void setIdVisanet(String idVisanet) {
		this.idVisanet = idVisanet;
	}
	public String getTipocontraentrega() {
		return tipocontraentrega;
	}
	public void setTipocontraentrega(String tipocontraentrega) {
		this.tipocontraentrega = tipocontraentrega;
	}
	public String getMontoEfectivo() {
		return montoEfectivo;
	}
	public void setMontoEfectivo(String montoEfectivo) {
		this.montoEfectivo = montoEfectivo;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dNI) {
		dni = dNI;
	}
	public String getTipoDocumentoPago() {
		return tipoDocumentoPago;
	}
	public void setTipoDocumentoPago(String tipoDocumentoPago) {
		this.tipoDocumentoPago = tipoDocumentoPago;
	}
	public String getEmailRepresentante() {
		return emailRepresentante;
	}
	public void setEmailRepresentante(String emailRepresentante) {
		this.emailRepresentante = emailRepresentante;
	}
	public String getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(String idPedido) {
		this.idPedido = idPedido;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getTipoVia() {
		return tipoVia;
	}
	public void setTipoVia(String tipoVia) {
		this.tipoVia = tipoVia;
	}
	public String getNombreVia() {
		return nombreVia;
	}
	public void setNombreVia(String nombreVia) {
		this.nombreVia = nombreVia;
	}
	public String getNumeroVia() {
		return numeroVia;
	}
	public void setNumeroVia(String numeroVia) {
		this.numeroVia = numeroVia;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getPisoDepartamento() {
		return pisoDepartamento;
	}
	public void setPisoDepartamento(String pisoDepartamento) {
		this.pisoDepartamento = pisoDepartamento;
	}
	public String getRangoHorario() {
		return rangoHorario;
	}
	public void setRangoHorario(String rangoHorario) {
		this.rangoHorario = rangoHorario;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getTelefonoContacto() {
		return telefonoContacto;
	}
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDesEstado() {
		return desEstado;
	}
	public void setDesEstado(String desEstado) {
		this.desEstado = desEstado;
	}
	
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getTelefonoUsuario() {
		return telefonoUsuario;
	}
	public void setTelefonoUsuario(String telefonoUsuario) {
		this.telefonoUsuario = telefonoUsuario;
	}
	public String getCodigoComercio() {
		return codigoComercio;
	}
	public void setCodigoComercio(String codigoComercio) {
		this.codigoComercio = codigoComercio;
	}
	public String getNombreComercio() {
		return nombreComercio;
	}
	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}
	public String getRucComercio() {
		return rucComercio;
	}
	public void setRucComercio(String rucComercio) {
		this.rucComercio = rucComercio;
	}
	
	public Integer getSeqPedido() {
		return seqPedido;
	}
	public void setSeqPedido(Integer seqPedido) {
		this.seqPedido = seqPedido;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getCantidadItems() {
		return cantidadItems;
	}
	public void setCantidadItems(String cantidadItems) {
		this.cantidadItems = cantidadItems;
	}
	public String toJsonString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.JSON_STYLE);
	}

	public String toMultiLineString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
