package com.tracking.bean;

import java.io.Serializable;

public class TipoDocPago implements Serializable{
	private static final long serialVersionUID = -5298601517891699497L;
	Integer tipoDocPago;
	String nombreTipoDocPago;
	public Integer getTipoDocPago() {
		return tipoDocPago;
	}
	public void setTipoDocPago(Integer tipoDocPago) {
		this.tipoDocPago = tipoDocPago;
	}
	public String getNombreTipoDocPago() {
		return nombreTipoDocPago;
	}
	public void setNombreTipoDocPago(String nombreTipoDocPago) {
		this.nombreTipoDocPago = nombreTipoDocPago;
	}
}