package com.tracking.bean;

import java.io.Serializable;
import java.util.List;

import com.tracking.util.BeanBase;

public class Pedido extends BeanBase {
	private static final long serialVersionUID = -3223526575095331647L;
	
	private Integer idPedido;
	private String	idPedidoCli;
	private String  codigoComercio;
	private String  nombreComercio;
	private String  rucComercio;
	private String nombreUsuario;
	private TipoDoc tipoDocBean;
	private String numDocIdentidad;
	private String telefonoUsuario;
	private String emailUsuario;
	private String cantidadItems;
	private TipoPago tipoPagoBean;
	private Segmento segmentoBean;
	private String nombreContacto;
	private String telefonoContacto;
	private Modalidad modalidadBean;
	private String departamento;
	private String provincia;
	private String distrito;
	private String coddep;
	private String codprov;
	private String coddist;
	private String direccion;
	private String numero;
	private String referencia;
	private String fechaRegistro;
	private TipoDocPago tipoDocPagoBean;
	private String numGuiaRemision;
	private String fechaPreparado;
	private String fechaProgramado;
	private String fechaSalidaAlmacen;
	private String fechaEntrega;
	private Estado estado;
	private SubEstado subEstadoBean;
	private String comentario;
	private String observacion;
	private List<Mpos> mpos ;
	private String numDocPago;
	private String seriales;
	public Integer getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(Integer idPedido) {
		this.idPedido = idPedido;
	}
	public String getIdPedidoCli() {
		return idPedidoCli;
	}
	public void setIdPedidoCli(String idPedidoCli) {
		this.idPedidoCli = idPedidoCli;
	}
	public String getCodigoComercio() {
		return codigoComercio;
	}
	public void setCodigoComercio(String codigoComercio) {
		this.codigoComercio = codigoComercio;
	}
	public String getNombreComercio() {
		return nombreComercio;
	}
	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}
	public String getRucComercio() {
		return rucComercio;
	}
	public void setRucComercio(String rucComercio) {
		this.rucComercio = rucComercio;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public TipoDoc getTipoDocBean() {
		return tipoDocBean;
	}
	public void setTipoDocBean(TipoDoc tipoDocBean) {
		this.tipoDocBean = tipoDocBean;
	}
	public String getNumDocIdentidad() {
		return numDocIdentidad;
	}
	public void setNumDocIdentidad(String numDocIdentidad) {
		this.numDocIdentidad = numDocIdentidad;
	}
	public String getTelefonoUsuario() {
		return telefonoUsuario;
	}
	public void setTelefonoUsuario(String telefonoUsuario) {
		this.telefonoUsuario = telefonoUsuario;
	}
	public String getEmailUsuario() {
		return emailUsuario;
	}
	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}
	public String getCantidadItems() {
		return cantidadItems;
	}
	public void setCantidadItems(String cantidadItems) {
		this.cantidadItems = cantidadItems;
	}
	public TipoPago getTipoPagoBean() {
		return tipoPagoBean;
	}
	public void setTipoPagoBean(TipoPago tipoPagoBean) {
		this.tipoPagoBean = tipoPagoBean;
	}
	public Segmento getSegmentoBean() {
		return segmentoBean;
	}
	public void setSegmentoBean(Segmento segmentoBean) {
		this.segmentoBean = segmentoBean;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getTelefonoContacto() {
		return telefonoContacto;
	}
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	public Modalidad getModalidadBean() {
		return modalidadBean;
	}
	public void setModalidadBean(Modalidad modalidadBean) {
		this.modalidadBean = modalidadBean;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getCoddep() {
		return coddep;
	}
	public void setCoddep(String coddep) {
		this.coddep = coddep;
	}
	public String getCodprov() {
		return codprov;
	}
	public void setCodprov(String codprov) {
		this.codprov = codprov;
	}
	public String getCoddist() {
		return coddist;
	}
	public void setCoddist(String coddist) {
		this.coddist = coddist;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public TipoDocPago getTipoDocPagoBean() {
		return tipoDocPagoBean;
	}
	public void setTipoDocPagoBean(TipoDocPago tipoDocPagoBean) {
		this.tipoDocPagoBean = tipoDocPagoBean;
	}
	public String getNumGuiaRemision() {
		return numGuiaRemision;
	}
	public void setNumGuiaRemision(String numGuiaRemision) {
		this.numGuiaRemision = numGuiaRemision;
	}
	public String getFechaPreparado() {
		return fechaPreparado;
	}
	public void setFechaPreparado(String fechaPreparado) {
		this.fechaPreparado = fechaPreparado;
	}
	public String getFechaProgramado() {
		return fechaProgramado;
	}
	public void setFechaProgramado(String fechaProgramado) {
		this.fechaProgramado = fechaProgramado;
	}
	public String getFechaSalidaAlmacen() {
		return fechaSalidaAlmacen;
	}
	public void setFechaSalidaAlmacen(String fechaSalidaAlmacen) {
		this.fechaSalidaAlmacen = fechaSalidaAlmacen;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	public SubEstado getSubEstadoBean() {
		return subEstadoBean;
	}
	public void setSubEstadoBean(SubEstado subEstadoBean) {
		this.subEstadoBean = subEstadoBean;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public List<Mpos> getMpos() {
		return mpos;
	}
	public void setMpos(List<Mpos> mpos) {
		this.mpos = mpos;
	}
	public String getNumDocPago() {
		return numDocPago;
	}
	public void setNumDocPago(String numDocPago) {
		this.numDocPago = numDocPago;
	}
	public String getSeriales() {
		return seriales;
	}
	public void setSeriales(String seriales) {
		this.seriales = seriales;
	}
	

}