package com.tracking.bean;

import java.io.Serializable;

import com.tracking.util.BeanBase;

public class Segmento extends BeanBase{
	private static final long serialVersionUID = -6346834847333259575L;
	private Integer idSegmento;
	private String nombreSegmento;
	public Integer getIdSegmento() {
		return idSegmento;
	}
	public void setIdSegmento(Integer idSegmento) {
		this.idSegmento = idSegmento;
	}
	public String getNombreSegmento() {
		return nombreSegmento;
	}
	public void setNombreSegmento(String nombreSegmento) {
		this.nombreSegmento = nombreSegmento;
	}
}
