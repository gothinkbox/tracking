package com.tracking.bean;

import com.tracking.util.BeanBase;

public class SubEstado extends BeanBase {
	private static final long serialVersionUID = -3223526575095331647L;

	Integer subEstadoPedido;
	Integer estadoPedido;
	String nombreSubestado;
	public Integer getSubEstadoPedido() {
		return subEstadoPedido;
	}
	public void setSubEstadoPedido(Integer subEstadoPedido) {
		this.subEstadoPedido = subEstadoPedido;
	}
	public Integer getEstadoPedido() {
		return estadoPedido;
	}
	public void setEstadoPedido(Integer estadoPedido) {
		this.estadoPedido = estadoPedido;
	}
	public String getNombreSubestado() {
		return nombreSubestado;
	}
	public void setNombreSubestado(String nombreSubestado) {
		this.nombreSubestado = nombreSubestado;
	}
	
	
}
