package com.tracking.bean;

import com.tracking.util.BeanBase;

public class Ubigeo extends BeanBase {

	private static final long serialVersionUID = 5916478360702837361L;

	private Integer idUbigeo;
	private String codUbigeo;
	private String nombreUbigeo;
	private String codPais;
	private String codDep;
	private String codProv;
	private String codDist;
	private String estadoUbigeo;
	private Integer orderUbigeo;
	
	public Integer getIdUbigeo() {
		return idUbigeo;
	}
	public void setIdUbigeo(Integer idUbigeo) {
		this.idUbigeo = idUbigeo;
	}
	public String getCodUbigeo() {
		return codUbigeo;
	}
	public void setCodUbigeo(String codUbigeo) {
		this.codUbigeo = codUbigeo;
	}
	public String getNombreUbigeo() {
		return nombreUbigeo;
	}
	public void setNombreUbigeo(String nombreUbigeo) {
		this.nombreUbigeo = nombreUbigeo;
	}
	public String getCodPais() {
		return codPais;
	}
	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}
	public String getCodDep() {
		return codDep;
	}
	public void setCodDep(String codDep) {
		this.codDep = codDep;
	}
	public String getCodProv() {
		return codProv;
	}
	public void setCodProv(String codProv) {
		this.codProv = codProv;
	}
	public String getCodDist() {
		return codDist;
	}
	public void setCodDist(String codDist) {
		this.codDist = codDist;
	}
	public String getEstadoUbigeo() {
		return estadoUbigeo;
	}
	public void setEstadoUbigeo(String estadoUbigeo) {
		this.estadoUbigeo = estadoUbigeo;
	}
	public Integer getOrderUbigeo() {
		return orderUbigeo;
	}
	public void setOrderUbigeo(Integer orderUbigeo) {
		this.orderUbigeo = orderUbigeo;
	}
	
}
