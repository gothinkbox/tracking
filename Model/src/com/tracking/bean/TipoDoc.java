package com.tracking.bean;

import java.io.Serializable;

public class TipoDoc implements Serializable{
	private static final long serialVersionUID = -3944344287573290506L;
	Integer tipoDocIdentidad;
	String nombreTipoDoc;
	public Integer getTipoDocIdentidad() {
		return tipoDocIdentidad;
	}
	public void setTipoDocIdentidad(Integer tipoDocIdentidad) {
		this.tipoDocIdentidad = tipoDocIdentidad;
	}
	public String getNombreTipoDoc() {
		return nombreTipoDoc;
	}
	public void setNombreTipoDoc(String nombreTipoDoc) {
		this.nombreTipoDoc = nombreTipoDoc;
	}
}