package com.tracking.bean;

import java.io.Serializable;

public class RolAcceso implements Serializable  {

	private static final long serialVersionUID = -8231458513900244499L;

	private Integer idRolAcceso;
	private String nombreAcceso;
	private String urlAcceso;
	private Integer estadoRolAcceso;
	private Integer idRol;
	private Integer OrdenRolAcceso;

	public Integer getIdRolAcceso() {
		return idRolAcceso;
	}

	public void setIdRolAcceso(Integer idRolAcceso) {
		this.idRolAcceso = idRolAcceso;
	}

	public String getNombreAcceso() {
		return nombreAcceso;
	}

	public void setNombreAcceso(String nombreAcceso) {
		this.nombreAcceso = nombreAcceso;
	}

	public String getUrlAcceso() {
		return urlAcceso;
	}

	public void setUrlAcceso(String urlAcceso) {
		this.urlAcceso = urlAcceso;
	}

	public Integer getEstadoRolAcceso() {
		return estadoRolAcceso;
	}

	public void setEstadoRolAcceso(Integer estadoRolAcceso) {
		this.estadoRolAcceso = estadoRolAcceso;
	}

	public Integer getIdRol() {
		return idRol;
	}

	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	public Integer getOrdenRolAcceso() {
		return OrdenRolAcceso;
	}

	public void setOrdenRolAcceso(Integer ordenRolAcceso) {
		OrdenRolAcceso = ordenRolAcceso;
	}

}