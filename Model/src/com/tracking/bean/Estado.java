package com.tracking.bean;

import com.tracking.util.BeanBase;

public class Estado extends BeanBase {
	private static final long serialVersionUID = -3223526575095331647L;

	Integer estadoPedido;
	String nombreEstado;
	public Integer getEstadoPedido() {
		return estadoPedido;
	}
	public void setEstadoPedido(Integer estadoPedido) {
		this.estadoPedido = estadoPedido;
	}
	public String getNombreEstado() {
		return nombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}
}
