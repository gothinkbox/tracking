package com.tracking.bean;

import com.tracking.util.BeanBase;

public class Mpos extends BeanBase {
	private static final long serialVersionUID = -3223526575095331647L;

	String serial;

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}
	
}
