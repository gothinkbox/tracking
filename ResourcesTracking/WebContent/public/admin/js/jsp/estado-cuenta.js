var EstadoCuenta = function() {

	return {
		init : function(uri,gifloading) {
			
			
			$('.Default').MonthPicker();
			$('#finicio').attr("disabled",true);
			$('#ffin').attr("disabled",true);
			
			var cantidad = 0;
			var total_paginas = 0;
			var pagina = 1;
			
			totalEstadoCuenta(function(data) {
				cantidad  = data;
				total_paginas = Math.ceil(cantidad/5);
			});
			
			$('#paginado').bootpag({
				   total: total_paginas,
				   page: 1,
				   maxVisible: 5,
				   leaps: false,
				   next: '>',
				   prev: '<'
				}).on('page', function(event, num){
					pagina = num;
					generarReporte(obtenerDatos());
			});
			
			
			$('#btn-generar').on('click',function(){
				generarReporte(obtenerDatos());
			});
			
			$('#btn-exportar').on('click',function(){
				window.location.href = uri + 'reporteEstadoCuenta/generarEstadoCuentaExcel';
			});
			
		    function obtenerDatos(){
				var fechaInicio = $('#finicio').val();
				var fechaFin = $('#ffin').val();
		        var  obj = {
		        		fechaInicio : fechaInicio,
		        		fechaFin : fechaFin,
		        		pagina : pagina
				   };
				return JSON.stringify(obj);
			}
		    
		    
		    
			function totalEstadoCuenta (retorno){
				$.ajax({
				url : uri + 'reporteEstadoCuenta/totalEstadoCuenta',
				async : false,
				type : "GET",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				beforeSend : function() {},
				error : function(a, b) {},
				success : function(data) {
					retorno(data);
					}
			   });
			}
		    
		    
		    function generarReporte (obj){
		    	$.ajax({
			    	url: uri + 'reporteEstadoCuenta/generarReporte',
			    	data : obj,
			    	async : false,
			    	type : 'POST',
			    	contentType : "application/json; charset=utf-8",
					dataType : "json",
					beforeSend : function (){
			    	},
					error : function() {
					},
					success : function (data){
						$('.error').remove();
						 var tbl_body = "";
						if(data.codigo > 0) {
							console.log(data.result)
							$.each(data.result,function(i,e){
								 var tbl_row = "";
							        HeadKeys = '<tr></tr>';
							        $.each(this, function(k , v){        
							            tbl_row += "<td>"+v+"</td>";
							            console.log( k +' : '+ v );
							            HeadKeys += '<th>'+k+'</th>';
							        })
							       tbl_body += "<tr>"+tbl_row+"</tr>"; 
							})
							$("#tabla-reporte").html(HeadKeys + tbl_body);
							$('#paginado').css('display','block');
							$('#btn-exportar').css('display','block');
						} else{
							$("#tabla-reporte").html('');
							$('#paginado').css('display','none');
							$('#btn-exportar').css('display','none');
							$("#tabla-reporte").before('<span class="error">'+data.mensaje+'</span>');
						}
			    	},
		    	});
		    }
		    
		    
		    
		    
		    

	 		
			
		}
	};

}();








