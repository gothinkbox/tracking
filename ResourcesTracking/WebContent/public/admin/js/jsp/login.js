var Login = function() {

	return {
		init : function(uri) {
			
			$('#btn-login').on('click',function(){
				$('#form-login').submit();
			});
			
			$('#inpt-password').keypress(function (e) {
				  if (e.which == 13) {
						var tipodoc = $('#slct-tipodoc').val();
						var nrodocumento = $('#inpt-nrodocMascara').val();	
						$('#inpt-nrodoc').val(nrodocumento+'|'+tipodoc);
						$('#form-login').submit();
				    return false;
				  }
			});
			
			if($('#class-error').text()!=''){
				$('.box-form input').parent().addClass('obl');
			}
			
			

		}
	};

}();