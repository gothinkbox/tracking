var ReporteCliente = function() {

	return {
		init : function(uri,gifloading) {
			
			ComponentsPickers.init();
			configurarGrilla();
			obtenerEstadisticas(obtenerDatosForm());

		    $('html, body').animate({
				scrollTop : $("#div-estadistica").offset().top
			}, 700);
			
		    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    	obtenerEstadisticas(obtenerDatosForm());   
		     });
		    
		    
		    $('#btn-descargarClientes').on('click',function(){
		    	var fechaInicio=$('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');	            
			    var fechaFin=$('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
		    	window.location.href = uri+"/reportes-clientes/descargarParticipantesapp/"+fechaInicio+"/"+fechaFin;
		    });
		    
		    
		    
			
			/****************TIPO DE RANGO********************/
			 $(".btnTipoRango").on("click",function(){
			      	$(".btnTipoRango").removeClass("blue");
			      	$(this).addClass("blue");
			      	$(".applyBtn").trigger("click");
			 });
			
			function obtenerDatosForm(){
			    var fechaInicio=$('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');	            
		        var fechaFin=$('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
		        var btn1 = $("#btn1").hasClass("blue");
		        var btn7 = $("#btn7").hasClass("blue");
		        var btn30 = $("#btn30").hasClass("blue");
		        var btn90 = $("#btn90").hasClass("blue");
		        var periodo = 0;
		        if(btn1){periodo = $("#btn1").attr("rel");}
		        else if(btn7){periodo = $("#btn7").attr("rel");}
		        else if(btn30){periodo = $("#btn30").attr("rel");}
		        else if(btn90){periodo = $("#btn90").attr("rel");}
		        var  obj = {
						fechaInicio : fechaInicio,
						fechaFin : fechaFin,
						periodo : periodo
				   };
				return JSON.stringify(obj);
			}
			
			
			function obtenerEstadisticas (objReporte){
				var datos = [];
				$('.errorChart').remove();
				$.ajax({
					url : uri+'reportes-clientes/obtenerParticipantesRegistrados',
					data : objReporte,
					async : true,
					type : "POST",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					beforeSend : function() {
						$('#chart_estadistica').html('<div style="width:100%;position:relative;top:30%;text-align:center"><img src="'+gifloading+'"></div>');
					},
					error : function(a, b) {
					   $('#chart_estadistica').after('<span class="errorChart help-block help-block-error col-lg-12">Ocurri&oacute; un error</span>');
					},
					success : function(data) {
						$('#chart_estadistica').html('');
						$('#msj-noregistros').css("display","none");
						$.each(data, function(i, e) {
							var objTransaccion = {
									fecha : e.fecha,
									transacciones : e.transacciones
									}
							datos.push(objTransaccion);
						});
						if(datos.length > 0){
							  Morris.Area({
								  gridIntegers: true,
								  parseTime: false,
								  element: 'chart_estadistica',
								  data:datos,
								  xkey: 'fecha',
								  ykeys: ['transacciones'],
								  labels: ['Registros' ],
								  yLabelFormat: function(y){return y != Math.round(y)?'':y;},
								  ymin: 0,
								  pointSize: 4,
								  fillOpacity: 0.6,
							      hideHover: 'auto',
							      behaveLikeLine: true,
							      resize: true,
								  lineColors: ['#e9c221'],
								 });
						}else{
							$('#modal-noregistro').modal();
						}
					}
					
				});
			}
			
			
	 		
			
			function configurarGrilla() {
			    $('#grilla-clientes').jqGrid({
			        url : uri + 'reportes-clientes/listarClientes',
			        datatype : "json",
			        mtype : 'GET',
			       colNames : [ 'Id','Nombres','Email' ,'Documento', 'Telefono', 'Dispositivo', 'Fecha registro'],
			                   
		        colModel : [
		          {
					name : 'idParticipanteapp',
					index : 'idParticipanteapp',
					width : 30,
					hidden : true
				},{
					name : 'nombre',
					index : 'nombre',
					width : 210, 
					sortable : true,
				},{
					name : 'correo',
					index : 'correo',
					width : 152,
					sortable : true,
				},{
					name : 'dni',
					index : 'dni',
					width : 190,
					sortable : false,
				},{
					name : 'telefono',
					index : 'telefono',
					width : 120,
					sortable : false,
				},{
					name : 'tipoDispositivo',
					index : 'tipoDispositivo',
					width : 120,
					sortable : false,
					formatter: tipoDispositivo
				},
				{
					name : 'auditoria.fechaCreacion',
					index : 'fechaCreacion',
					width : 150,
					sortable : true,
					formatter: formatoFecha
				}
		        ],
		        pager : '#pager-clientes',
				rowNum : 10,
				rowList : [ 10, 20, 30, 40 ],
				sortname : 'fechaCreacion',
				sortorder : 'asc',
				viewrecords : true,
				gridview : true,
				height : '100%',
				recordtext: 'Mostrando {0} - {1} de {2}',
		        emptyrecords:  'No hay registros para mostrar',
			    loadtext: 'Cargando...',
			    pgtext : 'P&aacute;gina {0} de {1}',
				hoverrows:false, 
				shrinkToFit: false
		});
		   $('.ui-jqgrid-titlebar').hide();
		}
			
			
		function formatoFecha(cellvalue, options, rowdata)
		{
		    if (typeof cellvalue == "undefined" || cellvalue == null)
		    { return "";}
		    else 
		    {return $.datepicker.formatDate('dd/mm/yy', new Date(cellvalue));}
		}
			
		function tipoDispositivo(cellvalue, options, rowdata)
		{
		    if (typeof cellvalue == "undefined" || cellvalue == null)
		    { return "";}
		    else {
		    	
		    	switch (cellvalue) {
				case 1:
					return  "Web";
					break;
				case 2:
					return  "Android";
					break;
				case 3:
					return  "IOS";
					break;
				}
		    	
		    	
		    }
		}
		
		
		
		
			
			
		}
	};

}();






