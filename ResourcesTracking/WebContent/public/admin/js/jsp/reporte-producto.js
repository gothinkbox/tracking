var ReporteProducto = function() {

	return {
		init : function(uri,gifloading) {
			
			ComponentsPickers.init();
			obtenerEstadisticas(obtenerDatosForm());
			
		    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			    obtenerEstadisticas(obtenerDatosForm());
		     });
		    
		    
		    $('#btn-descargarProductos').on('click',function(){
		    	var fechaInicio=$('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');	            
			    var fechaFin=$('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
		    	window.location.href = uri+"/reporteProducto/productosVisitadosDescargar/"+fechaInicio+"/"+fechaFin;
		    });
		    
		    
		    
			
			/****************TIPO DE RANGO********************/
//			 $(".btnTipoRango").on("click",function(){
//			      	$(".btnTipoRango").removeClass("blue");
//			      	$(this).addClass("blue");
//			      	$(".applyBtn").trigger("click");
//			 });
			
			function obtenerDatosForm(){
			    var fechaInicio=$('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');	            
		        var fechaFin=$('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
		        var  obj = {
						fechaInicio : fechaInicio,
						fechaFin : fechaFin
				   };
				return JSON.stringify(obj);
			}
			
			
			function obtenerEstadisticas (objReporte){
				var datos = [];
				$('.errorChart').remove();
				$.ajax({
					url : uri+'reporteProducto/productosVisitados',
					data : objReporte,
					async : true,
					type : "POST",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					beforeSend : function() {
						$('#chart_estadistica').html('<div style="width:100%;position:relative;top:30%;text-align:center"><img src="'+gifloading+'"></div>');
					},
					error : function(a, b) {
					   $('#chart_estadistica').after('<span class="errorChart help-block help-block-error col-lg-12">Ocurri&oacute; un error</span>');
					},
					success : function(data) {
						$('#chart_estadistica').html('');
						$('#msj-noregistros').css("display","none");
						$.each(data, function(i, e) {
							var objTransaccion = {
									descripcion : e.descripcion,
									transacciones : e.transacciones,
									entidad : e.entidad
									}
							datos.push(objTransaccion);
						});
						if(datos.length > 0){
							  Morris.Bar({
								  gridIntegers: true,
								  parseTime: false,
								  element: 'chart_estadistica',
								  data:datos,
								  xkey: ['entidad'],
								  ykeys: ['transacciones'],
								  labels: ['Visitas'],
								  stacked: true,
								  yLabelFormat: function(y){return y != Math.round(y)?'':y;},
								  ymin: 0,
								  pointSize: 4,
								  fillOpacity: 0.6,
							      hideHover: 'auto',
							      behaveLikeLine: true,
							      resize: true,
							      barColors: ['#2196f3'],
								 });
						}else{
							$('#modal-noregistro').modal();
						}
					}
					
				});
			}
			
			
		function tipoDispositivo(cellvalue, options, rowdata)
		{
		    if (typeof cellvalue == "undefined" || cellvalue == null)
		    { return "";}
		    else {
		    	switch (cellvalue) {
				case 1:
					return  "Web";
					break;
				case 2:
					return  "Android";
					break;
				case 3:
					return  "IOS";
					break;
				}
		    }
		}			
			
		
		
		}
	};

}();
