var ReporteCategoria = function() {

	return {
		init : function(uri,gifloading) {
			
			ComponentsPickers.init();
			obtenerEstadisticas(obtenerDatos());
			
		    $('#btn-descargarCategorias').on('click',function(){
		    	var fechaInicio=$('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');	            
			    var fechaFin=$('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
		    	window.location.href = uri+"/reporteCategoria/categoriasVisitadasDescargar/"+fechaInicio+"/"+fechaFin;
		    });
		    
			
			$("#btnAddCategoria").on("click",function() {
				var valor = $("#sltCategorias option:selected").val();
				var texto = $("#sltCategorias option:selected").text();
				
				if(valor=="0"){$('.close').click();}
				
				var iguales = 0;
				$(".elemento").each(function() {
					var rel = $(this).attr("rel");
					if (valor == rel) {
						iguales = 1;
					}
				});
				
				if (valor != "0") {$("#listaLocales-error").remove();}
				if (valor == "0") {return false;}
				else if (iguales == 1) {return false;} 
				else {
					$("#sltCategorias option[value=" + valor + "]").attr("disabled", "disabled");
					$("#divCategorias").append(
					"<p class='elemento' rel='"+valor+"'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"
				    + texto + "</p>");
				}
			});
			
	 		/********************* Agregar Locales al Select **************/
	 		$('.close').live('click', function(){
	 			$(this).parent("p").detach();
	 			var valorOption =  $(this).closest('.elemento').attr('rel');
	 			$("#sltCategorias option[value=" + valorOption + "]").removeAttr('disabled');   
	 		});
	 		
	 		$('.btnResultado , .applyBtn').on('click',function(){
	 			obtenerEstadisticas(obtenerDatos());
	 		});
	 		
		    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
	 			obtenerEstadisticas(obtenerDatos());
		     });
		    
	 		
			function obtenerDatos(){
			    var fechaInicio=$('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');	            
		        var fechaFin=$('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
		        var  obj = {
						fechaInicio : fechaInicio,
						fechaFin : fechaFin
				   };
				return JSON.stringify(obj);
			}
	 		
	 		
	 		function obtenerEstadisticas (objReporte){
	 			var datos = [];
	 			$('.errorChart').remove();
	 			$.ajax({
	 				url : uri+'/reporteCategoria/categoriasVisitadas',
	 				data : objReporte,
	 				async : true,
	 				type : "POST",
	 				contentType : "application/json; charset=utf-8",
	 				dataType : "json",
	 				beforeSend : function() {
	 					$('#chart_estadistica').html('<div style="width:100%;position:relative;top:30%;text-align:center"><img src="'+gifloading+'"></div>');
	 				},
	 				error : function(a, b) {
	 				   $('#chart_estadistica').after('<span class="errorChart help-block help-block-error col-lg-12">Ocurri&oacute; un error</span>');
	 				},
	 				success : function(data) {
	 					$('#chart_estadistica').html('');
	 					$.each(data, function(i, e) {
	 						var objTransaccion = {
	 								descripcion : e.descripcion,
	 								transacciones : e.transacciones	
	 								}
	 						datos.push(objTransaccion);
	 					});
	 					if(datos.length > 0){
	 					    Morris.Bar({
	 					    	gridIntegers: true,
								  parseTime: false,
								  element: 'chart_estadistica',
								  data:datos,
								  xkey: ['descripcion'],
								  ykeys: ['transacciones'],
								  labels: ['Visitas'],
								  stacked: true,
								  yLabelFormat: function(y){return y != Math.round(y)?'':y;},
								  ymin: 0,
								  pointSize: 4,
								  fillOpacity: 0.6,
							      hideHover: 'auto',
							      behaveLikeLine: true,
							      resize: true,
							      barColors: ['#2196f3'],
	 					    });
	 					}else{
	 						$('#modalMensajeNoData').modal();
	 					}
	 				}
	 				
	 			});
	 		}
			
	 		$('.btnResultado').click();
			
	 		
			
		}
	};

}();






