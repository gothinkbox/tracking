var reporteVisitaParticipante = function() {

	return {
		init : function(uri, gifloading) {

			ComponentsPickers.init();
			llenarTabla();
			$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
				llenarTabla();
			});

			$('#btn-descargarVisitas').on('click',function() {
				var fechaInicio = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var fechaFin = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
				window.location.href = uri+ "/reporteVisitaParticipante/descargarVisitas/"+ fechaInicio + "/" + fechaFin;
			});

			/** **************TIPO DE RANGO******************* */
			$(".btnTipoRango").on("click", function() {
				$(".btnTipoRango").removeClass("blue");
				$(this).addClass("blue");
				$(".applyBtn").trigger("click");
			});

			function obtenerDatosForm() {
				var fechaInicio = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var fechaFin = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
				var obj = {
					fechaInicio : fechaInicio,
					fechaFin : fechaFin
				};
				return JSON.stringify(obj);
			}

			function llenarTabla(){
				var fechaInicio = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var fechaFin = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
				console.log('fechaInicio: ' + fechaInicio); 
				console.log('fechaFin: ' + fechaFin);
				var dataSet = [];
				$.ajax({
					url:  uri + 'reporteVisitaParticipante/descargarVisitasConsolidado/'+ fechaInicio + '/' + fechaFin,
					type: 'GET',
					typeData : 'JSON',
					beforeSend:function(){
					},
					success:function(data){
						$.each(data, function(k, v) {							
							dataSet.push([v.catalogo,v.dni,v.nombre,v.cantidad]);
						});		
						$('#listaReportes').dataTable({
 							  //"scrollX": true,	
								"bPaginate": true,
								"data": dataSet,	
								"dom" : 'Bfrtip',			
							    "buttons" : [
							            'excel'
							        ],
								"columns": [
								    { "title": "CATALOGO"},
									{ "title": "DNI"},
									{ "title": "NOMBRE"},
									{ "title": "CANTIDAD"}
								]
							} );	
						},
				error:function(){
					console.log('error');
				}
				
			   });	
				
			}

			function tipoDispositivo(cellvalue, options, rowdata) {
				if (typeof cellvalue == "undefined" || cellvalue == null) {
					return "";
				} else {
					switch (cellvalue) {
					case 1:
						return "Web";
						break;
					case 2:
						return "Android";
						break;
					case 3:
						return "IOS";
						break;
					}
				}
			}


		}
	};

}();
