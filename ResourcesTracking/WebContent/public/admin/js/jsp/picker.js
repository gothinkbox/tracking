var ComponentsPickers = function () {

    var handleDateRangePickers = function () {
        if (!jQuery().daterangepicker) {
            return;
        } 
        
        function today(){
        	var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 
            return today = dd+'/'+mm+'/'+yyyy;
        }

        $('#reportrange').daterangepicker({
        		opens: 'left',
                startDate: moment().subtract('months', 3),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: today(),
                dateLimit: {
                    days: 180
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: { 
                    'Hoy' : [moment(), moment()],
                    'Ayer': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    '\xdaltimos 7 d\xedas': [moment().subtract('days', 6), moment()],
                    '\xdaltimos 30 d\xedas': [moment().subtract('days', 29), moment()],
                    'Este mes': [moment().startOf('month'), moment().endOf('month')],
                    'Mes anterior': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                buttonClasses: ['btn'],
                applyClass: 'green',
                cancelClass: 'default',
                format: 'DD/MM/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Aplicar',
                    fromLabel: 'Desde',
                    toLabel: 'Hasta',
                    customRangeLabel: 'Rango Personalizado',
                    daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    firstDay: 1
                }
            },
            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
        );
        
        /* Establecer el estado inicial de rangos 
         * fecha inicial : hoy
         * fecha fin : - 3 meses
         * */
        $('#reportrange span').html(moment().subtract('months', 3).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    }


   

    return {
        //main function to initiate the module
        init: function () {
            handleDateRangePickers();
        }
    };

}();