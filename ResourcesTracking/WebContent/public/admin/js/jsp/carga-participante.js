var CargaParticipante = function(uri) {
	this.btncargar=$('#btnimportar');
	this.btnIncorrectos=$('#btnincorrectos');
	this.frmcargar=$('#frmcargar');
	this.containerinfo=$('#containerinfo');
	this.labelcorrectos=$('#lblcorrectos');
	this.lblregistrados=$('#lblregistrados');
	this.lblactualizados=$('#lblactualizados');	
	this.labelincorrectos=$('#lblincorrectos');	
};

CargaParticipante.prototype.init=function(uri){	
	this.containerinfo.hide();
	this.btnIncorrectos.hide();
	this.handler();
	var uri = uri;
}

CargaParticipante.prototype.handler=function(uri){
	var obj = this;

	obj.frmcargar.on('submit',function(e){
		e.preventDefault();
		return obj.cargarparticipante();
	});
	
	obj.btnIncorrectos.on('click',function(e){
		e.preventDefault();
		window.location.href='exportar/participante/incorrectos';
		obj.labelcorrectos.html('');
		obj.labelcorrectos.html('');
		obj.btnIncorrectos.fadeIn('slow');
		obj.containerinfo.hide('slow');
	});
}

CargaParticipante.prototype.cargarparticipante=function(){
	var obj=this;
	
	obj.btncargar.attr('disabled',true);
	obj.btncargar.addClass('disabled');
	obj.containerinfo.hide();

    var f = $(this);
    var formData = new FormData();
    formData.append("file",$('input[type=file]')[0].files[0]);
    $.ajax({
        url:obj.frmcargar.attr('action'),
        type: "post",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
    	beforeSend : function() {
    		$('.div-cuerpo').block({    			
                message: '<h2>Se estan Cargando los Datos...</h2>', 
                css: { backgroundColor: '#fff', color: '#000'} 
            });
    	},
        success:function(data){
        	console.log('response: '+data);
        	obj.btnIncorrectos.hide();
        	obj.containerinfo.find('#msg').html(data.mensaje);
        	console.log(' data.codigo: ' + data.codigo )
            if(data.codigo>0){ //Exito
        	 obj.frmcargar.find('.dropify-clear').click();
        	 obj.labelcorrectos.html(data.result.correctos);
        	 obj.lblregistrados.html(data.result.registrados);
        	 obj.lblactualizados.html(data.result.actualizados);        	 
        	 obj.labelincorrectos.html(data.result.incorrectos);
        	 console.log('incorrectos: '+data.result.incorrectos);
        	 if (data.result.incorrectos>0) {
        		 obj.containerinfo.find('#msg').html("Descargar registros incorrectos");
        		 obj.btnIncorrectos.show();
			  }
        	 obj.containerinfo.show();
        	}else{
        		obj.containerinfo.hide();
        		alert(data.mensaje);
        	}
        },complete:function(){
        	$('.div-cuerpo').unblock();
        	obj.btncargar.attr('disabled',false);
        	obj.btncargar.removeClass('disabled');
        }
    });

    return false;
}