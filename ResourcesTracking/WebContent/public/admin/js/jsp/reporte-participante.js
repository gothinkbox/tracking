var ReporteParticipante = function() {

	return {
		init : function(uri) {
			
			var registrosMostrar = 5;
			var orden = "apellido";
			var cantidad = 0;
			var total_paginas = 0;
			totalParticipantes(function(data) {
				cantidad  = data;
				total_paginas = Math.ceil(cantidad/registrosMostrar);
			});
			
			obtenerParticipantes(1,registrosMostrar,orden);
			
			$('.paginado').bootpag({
				   total: total_paginas,
				   page: 1,
				   maxVisible: 5,
				   leaps: false,
				   next: '>',
				   prev: '<'
				}).on('page', function(event, num){
					$('#tabla-participantes tbody').html('');
				    obtenerParticipantes(num,registrosMostrar,orden);
			});
			
			$('#btnDescargarClientes').click(function(){
				window.location.href = uri+ "/reporteParticipante/descargaListaClientes";				
			});
			
			
/************************* BUSCAR PARTICIPANTE  *********************************/
		
		$('.tipo-busqueda').click(function(){
			if($(this).attr('rel')==1){
				$('#inp-texto-busqueda').attr('placeholder','Nº de DNI');
				$('#inp-texto-busqueda').val("");
			}
			if($(this).attr('rel')==2){
				$('#inp-texto-busqueda').attr('placeholder','Email');
				$('#inp-texto-busqueda').val("");
			}
			if($(this).attr('rel')==3){
				$('#inp-texto-busqueda').attr('placeholder','Apellidos');
				$('#inp-texto-busqueda').val("");
			}
		});
		
		
		$('#btn-busqueda').click(function(){
			var keyBusqueda =  $('.tipo-busqueda.active').attr('rel');
			var textoBusqueda = $('#inp-texto-busqueda').val();
			
			if(textoBusqueda == ''){
				var sinResultados = "<tr><td colspan='7' align='center'>No se encontraron resultados de la busqueda</td></tr>";
				$('#tabla-participantes tbody').html('');
				$('#tabla-participantes tbody').html(sinResultados);
				$('.paginado').css('display','none');
			}
			else{
				buscarParticipante(keyBusqueda, textoBusqueda);	
			}
		});
		
/******************************************************************************/	

/************************* ACTIVAR/DESACTIVAR *********************************/
		var idParticipante = 0;
		var emailParticipante = "";
			
		$('.btn-idParticipante').live('click',function(){
			idParticipante = $(this).attr('rel');
			emailParticipante = $(this).attr('rev');
		});
		
		$('#desactivar-participante .boton-estado').click(function(){
			var estado = $('.inp-estado-inactivo').val();
			activacionParticipante(idParticipante, emailParticipante, estado);
		});
		 
		$('#activar-participante .boton-estado').click(function(){
			var estado = $('.inp-estado-activo').val();
			activacionParticipante(idParticipante, emailParticipante, estado);
		});
		
/*****************************************************************************/

/************************* ORDENAR *********************************/
		
		$(".select-ordenar").on("change",function(){

 			var nombreOrdenar = $('.select-ordenar').find('option:selected').val();
 			var cantidadOrdenar = 0;
			var totalPaginasOrdenar = 0;
			totalParticipantes(function(data) {
				cantidadOrdenar  = data;
				totalPaginasOrdenar = Math.ceil(cantidadOrdenar/registrosMostrar);
			});
			$('#tabla-participantes tbody').html('');
			obtenerParticipantes(1, registrosMostrar, nombreOrdenar);
			
			$('.paginado').bootpag({
				   total: totalPaginasOrdenar,
				   page: 1,
				   maxVisible: 5,
				   leaps: false,
				   next: '>',
				   prev: '<'
				}).on('page', function(event, num){
					$('#tabla-participantes tbody').html('');
					obtenerParticipantes(num, registrosMostrar, nombreOrdenar);
				});
			
		});
		
/*****************************************************************************/
		
			function obtenerParticipantes (pagina,tamanio,orden){
				$.ajax({
				url : uri + 'reporteParticipante/listarParticipantes/'+pagina+'/'+tamanio+'/'+orden,
				async : false,
				type : "GET",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				beforeSend : function() {},
				error : function(a, b) {},
				success : function(data) {
					var html = '';
					$(data).each(function(i,e) {
						 var row = '';
						 var estadoNombre = '';
						 var documentoNombre = '';
						 var boton = '';

						 if(e.estadoParticipante == 1){
							 estadoNombre = 'ACTIVO';
							 boton = '<button type="button" style="width: 80% !important;" class="btn-idParticipante btn btn-danger waves-effect waves-light waves-effect waves-light" rev="'+e.emailParticipante+'" rel="'+e.idParticipante+'" data-toggle="modal" data-target="#desactivar-participante">DESACTIVAR CUENTA</button>';
						 }else{
							 estadoNombre = 'INACTIVO';
							 boton = '<button type="button" style="width: 80% !important;" class="btn-idParticipante btn btn-success waves-effect waves-light waves-effect waves-light" rev="'+e.emailParticipante+'" rel="'+e.idParticipante+'" data-toggle="modal" data-target="#activar-participante">ACTIVAR CUENTA</button>';
						 }
						 
						 row =  '<tr>';
						 row += 	'<td class="alineacion-texto">'+ e.nombreParticipante+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.appaterno+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.apmaterno+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.emailParticipante+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.nombreTipoDocumento+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.nroDocumento+'</td>';						 	
						 row +=		'<td class="alineacion-texto">'+ e.telefonoParticipante+'</td>';
						 row +=		'<td class="alineacion-texto">'+ e.movilParticipante+'</td>';						 
						 row += 	'<td class="alineacion-texto">'+ estadoNombre+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.catalogo.nombreCatalogo+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.establecimiento+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.zona+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.tipocadena+'</td>';
						 row +=		'<td class="alineacion-texto">';
						 row +=		'<input id="inp-idParticipante" type="hidden" value="'+e.idParticipante+'"/>';
						 row +=			boton;
						 row +=		'</td>';
						 row += '</tr>';
				         $('#tabla-participantes tbody').append(row);
					
					});
				}
			   });
			}
			
			
			function buscarParticipante (keyBusqueda, textoBusqueda){
				$.ajax({
					url : uri + 'reporteParticipante/buscarParticipante/'+keyBusqueda+'/'+textoBusqueda,
					async : false,
					type : 'GET',
					contentType : 'application/json; charset=utf-8',
					dataType : 'json',
					beforeSend : function(){},
					error : function(a, b){},
					success : function(data){
						
						$('.paginado').bootpag({
							   total: 1,
							   page: 1,
							   maxVisible: 1,
							   leaps: false,
							   next: '>',
							   prev: '<'
							}).on('page', function(event, num){
								$('#tabla-participantes tbody').html('');

						});
						
						var html = '';
						$('#tabla-participantes tbody').html('');
						
						$(data).each(function(i,e) {
							
						var row = '';
						 var estadoNombre = '';
						 var documentoNombre = '';
						 var boton = '';
						 if(e.estadoParticipante == 1){
							 estadoNombre = 'ACTIVO';
							 boton = '<button type="button" style="width: 80% !important;" class="btn-idParticipante btn btn-danger waves-effect waves-light waves-effect waves-light" rev="'+e.emailParticipante+'" rel="'+e.idParticipante+'" data-toggle="modal" data-target="#desactivar-participante">DESACTIVAR CUENTA</button>';
						 }else{
							 estadoNombre = 'INACTIVO';
							 boton = '<button type="button" style="width: 80% !important;" class="btn-idParticipante btn btn-success waves-effect waves-light waves-effect waves-light" rev="'+e.emailParticipante+'" rel="'+e.idParticipante+'" data-toggle="modal" data-target="#activar-participante">ACTIVAR CUENTA</button>';
						 }	
						 
						 row =  '<tr>';
						 row += 	'<td class="alineacion-texto">'+ e.nombreParticipante+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.appaterno+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.apmaterno+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.emailParticipante+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.nombreTipoDocumento+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.nroDocumento+'</td>';						 	
						 row +=		'<td class="alineacion-texto">'+ e.telefonoParticipante+'</td>';
						 row +=		'<td class="alineacion-texto">'+ e.movilParticipante+'</td>';						 
						 row += 	'<td class="alineacion-texto">'+ estadoNombre+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.catalogo.nombreCatalogo+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.establecimiento+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.zona+'</td>';
						 row += 	'<td class="alineacion-texto">'+ e.tipocadena+'</td>';						 
						 row +=		'<td style="text-align: center">';
						 row +=		'<input id="inp-idParticipante" type="hidden" value="'+e.idParticipante+'"/>';
						 row +=			boton;
						 row +=		'</td>';
						 row += '</tr>';
				         $('#tabla-participantes tbody').append(row);
				         
						});
					}
					
				});
			}
			

			function totalParticipantes (retorno){
				$.ajax({
				url : uri + 'reporteParticipante/contarParticipantes',
				async : false,
				type : "GET",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				beforeSend : function() {},
				error : function(a, b) {},
				success : function(data) {
					retorno(data);
					}
			   });
			}
			
			
			function activacionParticipante(idParticipante, emailParticipante, estado){
				$.ajax({
					url : uri + 'reporteParticipante/activarParticipante/'+idParticipante+'/'+estado,
					async : false,
					type : 'GET',
					contentType : 'application/json; charset=utf-8',
					dataType : 'json',
					beforeSend : function(){},
					error : function(a, b){},
					success : function(data){
						
						if(data > 0){
							confirmacionCliente(emailParticipante, estado);							
							$('#tabla-participantes tbody').html('');
							obtenerParticipantes(1,registrosMostrar,orden);
							
						}	
						
					}
				});
			}
			
			
			function confirmacionCliente(email, estado){
				$.ajax({
					url : uri+'reporteParticipante/correoActivacion/'+email+'/'+estado,
					async : true,
					type : "GET",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					beforeSend : function() {},
					error : function(a, b) {},
					success : function(data) {
					
						$('#correo-participante').modal('show');
				
					}
				});
			}
			
			
			function convertDateSlash(inputFormat) {
				function pad(s) { return (s < 10) ? '0' + s : s; }
				var d = new Date(inputFormat);
				return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
			}
			
			
			function convertDateGuion(inputFormat) {
				function pad(s) { return (s < 10) ? '0' + s : s; }
				var d = new Date(inputFormat);
				return [ d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-');
			}
			
			
		}
	
	}
}();