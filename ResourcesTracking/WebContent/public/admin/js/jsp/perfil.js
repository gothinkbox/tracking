
var Perfil = function() {

	return {
		init : function(uri) {
			
			$('#btn-guardar-pefil').on('click',function(){
				if($('#form-perfil').valid()){
					guardarPerfil();
				}
			});
			
			 function obtenerDatos(){
				 var obj = {
					nombres : $('#inpt-nombres').val(),
					apellidoPa : $('#inpt-apellidopaterno').val(),
					apellidoMa : $('#inpt-apellidomaterno').val(),
					claveAnterior : $('#inpt-claveanteior').val(),
					clave : $('#clavenueva').val()
				 }
				 return JSON.stringify(obj);
			 }
			 
			function guardarPerfil(){
				$.ajax({
				url : uri + 'perfil/guardar',
				data : obtenerDatos(),
				async : true,
				type : "POST",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				beforeSend : function() {
					 $('#btn-guardar-pefil').text('Guardando ...');
					 $('#btn-guardar-pefil').attr('disabled',true);
					mostrarAlerta(2,'Registro','Guardando . . .')
				},
				error : function(a, b) {
					mostrarAlerta(3,'Ocurrio error')
					$('#btn-guardar-pefil').text('Guardando ...');
					$('#btn-guardar-pefil').attr('disabled',false);
					$('#btn-guardar-pefil').text('Guardar');
				},
				success : function(data) {
					  if(data.codigo > 0){
						  mostrarAlerta(1, data.mensaje)
						  $('#btn-guardar-pefil').text('Guardar');
					  
					  }else{
							$('#btn-guardar-pefil').attr('disabled',false);
							$('#btn-guardar-pefil').text('Guardar');
						  mostrarAlerta(3, data.mensaje)
					  }
				}
				});
			}
			

			
	        $('#form-perfil').validate({
	     		rules: {     
	     			nombres: {
	                    required : true,
	                },
	                apellidopaterno: {
	                	required : true,
	                },
	                apellidomaterno : {
	                	required : true
	                },
	                claveanteior : {
	                	required : true
	                },
	                clavenueva : {
	                	required : true
	                },
	                repitaclave : {
	                	required : true,
	                	equalTo  : "#clavenueva"
	                }
	     		},
	             messages: {
	            	 nombres: {
	                     required : "Ingrese nombres",
	                 },
	                 apellidopaterno : {
	                	 required : "Ingrese apellido paterno" 
	                 },
	                 apellidomaterno : {
	                	 required : "Ingrese apellido materno"
	                 },
	                 claveanteior : {
	                	 required : "Ingrese clave anterior"
	                 },
	                 clavenueva : {
	                	 required : "Ingrese clave nueva"
	                 },
	                 repitaclave : {
	                	 required : "Repita clave",
	                	 equalTo : "Claves no coinciden"
	                 }
	             },
	             highlight: function(element) {
	                 $(element).closest('.form-group').addClass('has-error');
	             },
	             unhighlight: function(element) {
	                 $(element).closest('.form-group').removeClass('has-error');
	             },
	             errorElement: 'span',
	             errorClass: 'help-block',
	             focusInvalid: false,
	             invalidHandler: function(form, validator) {
	                 if (!validator.numberOfInvalids())
	                     return;
	                 $('html, body').animate({
	                     scrollTop: $(validator.errorList[0].element).offset().top-120}, 700);
	             }
	     	});
			
			
	        function mostrarAlerta(estado,mensaje) { 
	    		var glyphicon ='';
	    		var alert = '';
	    		
	    		switch (parseInt(estado)) {
	    		case 1: 
	    			alert = 'success';
	    			$('body').css('cursor', 'default');
	    			break;
	    		
	    		case 2: 
	    			alert = 'info';
	    			$('body').css('cursor', 'wait');
	    			break;
	    			
	    		case 3: 
	    			alert = 'danger';
	    			glyphicon= 'remove-circle';
	    			$('body').css('cursor', 'default');
	    			break;
	    		}
	    		
	    		var html  = '<div class="alert alert-'+alert+' alert-dismissable">';
	    		    html +=     '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>';
	    		    html +=     '<span>'+mensaje+'</span>';
	    		    html += '</div>';
	    		 $('#mensajeMostrarAlerta').html(html);
	    }
			
			
			

		}
	};

}();