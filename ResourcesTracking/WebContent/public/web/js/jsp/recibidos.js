
var Recibidos = function() {

	return {
		init : function(uri) {
			
			tipoPago();
			segmento();
			ubigeo(filtroUbigeo(1), 1);
			
			var sourceListado = $('#row-recibidos').html();
			var templateListado = Handlebars.compile(sourceListado);
			
			
			function filtroUbigeo(tipoBusqueda){
				var codDep = $('#departamento').val();
				var codProv = $('#provincia').val();
				var ubigeo = {
						codDep : codDep,
						codProv : codProv
				}
		        var  filtro = {
		        		tipoBusqueda : tipoBusqueda,
		        		ubigeo : ubigeo
				   };
				return JSON.stringify(filtro);
			}
			
			$('#departamento').on('change', function(){
				ubigeo(filtroUbigeo(2), 2)
			});
			
			function ubigeo(datos, tipoBusqueda){
				$.ajax({
					url : uri + '/common/listarUbigeos',
					data : datos,
			    	type : 'POST',
			    	contentType : "application/json; charset=utf-8",
					dataType : "json",
					error:function(xhr){console.log(1);},
					beforeSend:function(){
						if(tipoBusqueda == 1){
							$("#departamento").html("<option value='00' selected>Cargando...</option>");
						}
						$("#provincia").html("<option value='00' selected>Cargando...</option>");
					},
					success:function(data){
						if(tipoBusqueda == 1){
							$("#departamento").html("<option value='00' selected>Todos</option>");
							$("#provincia").html("<option value='00' selected>Todos</option>");
							$.each(data,function(i,e){
								$("#departamento").append("<option value='"+e.codDep+"'>"+e.nombreUbigeo+"</option>");
							});				
						}
						if(tipoBusqueda == 2){
							$("#provincia").html("<option value='00' selected>Todos</option>");
							$.each(data,function(i,e){
								$("#provincia").append("<option value='"+e.codProv+"'>"+e.nombreUbigeo+"</option>");
							});				
						}
					}
				});
			}
			
			function segmento(){
				$.ajax({
					url : uri + '/common/listarSegmento',
			    	type : 'GET',
			    	contentType : "application/json; charset=utf-8",
					dataType : "json",
					error:function(xhr){console.log(1);},
					beforeSend:function(){
						$("#segmento").html("<option value='0' selected>Cargando...</option>");
					},
					success:function(data){
						$("#segmento").html("<option value='0' selected>Todos</option>");
						$.each(data,function(i,e){
							$("#segmento").append("<option value='"+e.idSegmento+"'>"+e.nombreSegmento+"</option>");
						});				
					}
				});
			}
			
			function tipoPago(){
				$.ajax({
					url : uri + '/common/listarTipoPago',
			    	type : 'GET',
			    	contentType : "application/json; charset=utf-8",
					dataType : "json",
					error:function(xhr){console.log(1);},
					beforeSend:function(){
						$("#tipoPago").html("<option value='0' selected>Cargando...</option>");
					},
					success:function(data){
						$("#tipoPago").html("<option value='0' selected>Todos</option>");
						$.each(data,function(i,e){
							$("#tipoPago").append("<option value='"+e.tipoPago+"'>"+e.nombreTipoPago+"</option>");
						});				
					}
				});
			}
			
			
			$('#form-filtro').validate({
	     		rules: {
	                estado: {
	                    required : true,
	                },
	                fecini: {
	                    required : true,
	                },	                
	                fecfin: {
	                	required : true,
	                },
	     			departamento: {
	                    required : true,
	                },
	                provincia: {
	                	required : true,
	                },
	                tipoPago: {
	                	required : true,
	                },
	                segmento: {
	                	required : true,
	                }
	     		},
	             messages: {
	            	 estado: {
		                    required : "Seleccione un estado",
		                },
		                fecini: {
		                    required : "Seleccione fecha inicio",
		                },	                
		                fecfin: {
		                	required : "Seleccione fecha fin",
		                },
		     			departamento: {
		                    required : "Seleccione departamento",
		                },
		                provincia: {
		                	required : "Seleccione provincia",
		                },
		                tipoPago: {
		                	required : "Seleccione tipo pago", 	
		                },
		                segmento: {
		                	required : "Seleccione segmento",
		                }
	                 
	             },
	             highlight: function(element) {
	                 $(element).closest('.form-group').addClass('has-error');
	             },
	             unhighlight: function(element) {
	                 $(element).closest('.form-group').removeClass('has-error');
	             },
	             errorElement: 'span',
	             errorClass: 'error-busqueda',
	             focusInvalid: false,
	             invalidHandler: function(form, validator) {
	                 if (!validator.numberOfInvalids())
	                     return;
	                 $('html, body').animate({
	                     scrollTop: $(validator.errorList[0].element).offset().top-120}, 700);
	             },
	             errorPlacement: function(error, element) {
	            	    error.insertAfter(element.parent());
	            	}
	     	});
			
			
			
			
			function obtenerDatos(){
			    var estado=$('#estado').val();	            
			    var fecini=$('#fecini').val();	            
			    var fecfin=$('#fecfin').val();	            
			    var departamento=$('#departamento').val();	            
			    var provincia=$('#provincia').val();	            
			    var tipoPago=$('#tipoPago').val();	            
			    var rubro=$('#rubro').val();	            
			    var tipoBusqueda=$('#tipoBusqueda').val();	            
			    var valor=$('#valor').val();	            
		        var  obj = {
		        		estado : estado,
		        		fecini : fecini,
		        		fecfin : fecfin,
		        		departamento : departamento,
		        		provincia : provincia,
		        		tipoPago : tipoPago,
		        		rubro : rubro,
		        		tipoBusqueda : tipoBusqueda,
		        		valor : valor,
				   };
				return JSON.stringify(obj);
			}
			
			
			function obtenerListado(){
				console.log(obtenerDatos());
				$.ajax({
					url : uri+'pedidos/listarPedidos',
					data : obtenerDatos(),
					type : "POST",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					beforeSend : function() {
						$('#tbody-recibidos').html('');
						$(".no-records-found").show();
					},
					error : function(ts) {
						console.log(ts.responseText)
					},
					success : function(data) {
						$.each(data, function(i, e) {
							$(".no-records-found").hide();
							console.log(e);
							$('#tbody-recibidos').append(templateListado(e));
						});
					}
					
				});
				
			}
			
			$('#btnBuscar').on('click',function(e){
				e.preventDefault();
					console.log('OK');
					obtenerListado();
			});
			
			$('#btnProcesar').on('click',function(e){
				e.preventDefault();
				var checked = []
				var $boxes = $('input[name=btSelectItem]:checked');
				$boxes.each(function(){
					checked.push(parseInt($(this).val()));
				});
				
				$.ajax({
					url : uri+'pedidos/prep',
					data : checked.toString(),
					type : "POST",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					beforeSend : function() {
						$('#btnProcesar').html('Procesando');
					},
					error : function(ts) {
						$('#btnProcesar').html('Preparar seleccionados');
						console.log(ts.responseText)
					},
					success : function(data) {
						window.location.href = uri+'pedidos/preparar';
					}
					
				});
				
			});
			
			
			//FIN LOGIC
		}
	};

}();