
var Preparar = function() {

	return {
		init : function(uri) {
			cargarPedido();
			
			function cargarPedido(){
				$.ajax({
					url : uri+'pedidos/get',
					type : "GET",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					beforeSend : function() {
//						$('#btnProcesar').html('Procesando');
					},
					error : function(ts) {
//						$('#btnProcesar').html('Preparar seleccionados');
						console.log(ts.responseText);
						window.location.href = uri+'pedidos';
					},
					success : function(data) {
						console.log('data>>>'+data);
						$('#span-id').html(data.idPedido);
						$('#fechaPedido').val(data.auditoria.fechaCreacion);
						$('#cantidad').val(data.cantidadItems);
						$('#pago').val(data.tipoPagoBean.nombreTipoPago);
						$('#documento').val(data.numDocIdentidad);
						$('#nombre').val(data.nombreUsuario);
						$('#destinatario').val(data.nombreContacto);
						$('#departamento').val(data.departamento);
						$('#provincia').val(data.provincia);
						$('#distrito').val(data.distrito);
						$('#direccion').val(data.direccion);
						$('#referencia').val(data.referencia);
						$('#telefono').val(data.telefonoContacto);
						$('#rubro').val(data.segmentoBean.nombreSegmento);
					}
					
				});
			}
			
			
			function obtenerDatos(){
			    var idPedido=$('#span-id').text();	            
			    var serie=$('#serie').val();	            
			    var guia=$('#guia').val();	            
			    var factura=$('#factura').val();
		        var  obj = {
		        		idPedido : idPedido,
		        		numGuiaRemision : guia,
		        		numDocPago : factura,
		        		seriales : serie
				   };
				return JSON.stringify(obj);
			}
			
			$('#btnGuardar').on('click',function(e){
				e.preventDefault();
				
				var obj = obtenerDatos();
				
				$.ajax({
					url : uri+'pedidos/guardarPrep',
					data : obj,
					type : "POST",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					beforeSend : function() {
						$('#btnGuardar').html('Procesando');
					},
					error : function(ts) {
						$('#btnGuardar').html('Guardar y Continuar');
						console.log(ts.responseText)
					},
					success : function(data) {
						console.log('DATA>'+data)
						window.location.href = uri+'pedidos/preparar';
					}
					
				});
				
			});
			
			//FIN LOGIC
		}
	};

}();